const path = require('path');
const fs = require('fs');

const lessToJs = require('less-vars-to-js');
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, './src/ant-theme-vars.less'), 'utf8'));

const babelOptions = {
  presets: [

    ['env', { modules: false, targets: { browsers: ['last 2 versions'] } }],
    'react'
  ],
  cacheDirectory: true,
  plugins: [
    ['import', { libraryName: "antd", style: true }],
    'transform-strict-mode',
    'transform-object-rest-spread'
  ]
};

module.exports = [
  {
    include: [
      /node_modules\/instascan\/src\/camera.js/,
      /node_modules\/instascan\/src\/scanner.js/,
      /node_modules\/frappe-charts\/dist\/frappe-charts.esm.js/
    ],
    test: /\.js$/,
    loader: 'babel-loader',
    options: babelOptions
  },
  {
    exclude: /node_modules/,
    test: /\.ts(x?)$/,
    use: [
      {
        loader: 'babel-loader',
        options: babelOptions
      },
      {
        loader: 'ts-loader'
      }
    ]
  },
  {
    test: /\.css$/,
    loader: "style-loader!css-loader"
  },
  {
    test: /\.less$/,
    use: [
      { loader: "style-loader" },
      { loader: "css-loader" },
      {
        loader: "less-loader",
        options: {
          javascriptEnabled: true,
          modifyVars: themeVariables,
          root: path.resolve(__dirname, './')
        }
      }
    ]
  }
];
