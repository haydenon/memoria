import React from "react";

declare interface IProps {
  text: string;
  onCopy?: (text: string, result: boolean) => void;
}

declare class CopyToClipboard extends React.Component<IProps> { }

export {
  CopyToClipboard
}
