import React from "react";

declare type Level = 'L' | 'M' | 'Q' | 'H';

declare interface IQRProps {
  value: string;
  size?: number;
  bgColor?: string;
  fgColor?: string;
  level?: Level
}

declare class QRCode extends React.Component<IQRProps> { }

export default QRCode;
