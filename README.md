# Memoria

A web application for writing journals. The application is almost completely client side E2E encrypted, and uses PouchDB to sync data between devices.

This is a personal project that I'm using to write journals and track how well I'm doing in various areas (e.g. exercise) over time.

## To do

- [ ] Better onboarding.
    - [ ] Simplify text at the start to help people understand how accounts work.
    - [ ] Tutorial for creating journal entries, and what they look like.
- [ ] Add charts to see how well different areas are over time.