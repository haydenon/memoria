declare class LZString {
  static compress: (str: string) => string;
  static compressToBase64: (str: string) => string;
  static decompress: (str: string) => string;
  static decompressFromBase64: (str: string) => string;
}

export default LZString;