export declare interface ICamera {
  id: string;
  name: string;
}

declare interface ICameraSettings {
  getCameras(): Promise<ICamera[]>;
}

export declare interface IScanner {
  addListener(event: 'scan', callback: (content: string, image: string | null) => void): void;
  addListener(event: 'active' | 'inactive', callback: () => void): void;
  start(camera: ICamera): Promise<void>;
  stop(): Promise<void>;
}

declare interface IScannerOptions {
  video: HTMLElement;
  continuous?: boolean;
  mirror?: boolean;
  captureImage?: boolean;
  backgroundScan?: boolean;
  refractoryPeriod?: number;
  scanPeriod?: number;
}


declare class Instascan {
  static Scanner: new (opts: IScannerOptions) => IScanner;
  static Camera: ICameraSettings;
}

export default Instascan;
