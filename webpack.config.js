const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const loaders = require('./webpack.loaders');

const HOST = process.env.HOST || "127.0.0.1";
const PORT = process.env.PORT || "8000";

module.exports = {
  node: {
    fs: "empty"
  },
  context: __dirname,
  entry: [
    'whatwg-fetch',
    './src/index.tsx'
  ],
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'dist/public'),
    filename: '[hash].js'
  },
  resolve: {
    modules: ['src', 'node_modules'],
    extensions: ['.tsx', '.ts', '.js', '.less']
  },
  module: {
    loaders
  },
  devtool: process.env.WEBPACK_DEVTOOL || 'cheap-module-source-map',
  plugins: [
    new WebpackCleanupPlugin(),
    new CopyWebpackPlugin([{ from: './src/assets', to: 'assets' }]),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      files: {
        css: ['style.css'],
        js: ['bundle.js'],
      }
    })
  ]
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        drop_console: true,
        drop_debugger: true
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new ExtractTextPlugin({
      filename: 'style.[contenthash:20].css',
      allChunks: true
    }),
  ])
} else {
  module.exports.entry.splice(module.exports.entry.length - 1, 0, 'react-hot-loader/patch')
  module.exports.devServer = {
    proxy: { '/api/**': { target: 'http://localhost:3000', secure: false } },
    contentBase: "./dist/public",
    // do not print bundle build stats
    noInfo: true,
    // enable HMR
    hot: true,
    // embed the webpack-dev-server runtime into the bundle
    inline: true,
    // serve index.html in place of 404 responses to allow HTML5 history
    historyApiFallback: true,
    port: PORT,
    host: HOST
  }
  module.exports.plugins = (module.exports.plugins || []).concat([
    new ExtractTextPlugin({
      filename: 'style.css',
      allChunks: true
    })
  ])
}

