import React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { push } from 'react-router-redux';

import { Badge, Button, Card, Divider, Icon, Popconfirm } from 'antd';
import { Moment } from 'moment';
import { AutoSizer, List } from 'react-virtualized';

import { groupBy, reverse, sort } from '../prelude';

import { setBreadcrumbs } from '../actions/breadcrumbs.actions';
import { deleteJournal } from '../actions/entry.actions';
import PercentageChart from '../components/PercentageChart';
import PopupDatepicker from '../components/PopupDatepicker';
import { readableTimeSince } from '../date-helpers';
import { IEntry, ISection } from '../journal';
import { IState } from '../reducer';
import { IEntryState } from '../reducer/entry.reducer';
import { ISettingsState } from '../reducer/settings.reducer';
import { entries, IEntryHelper, ISectionHelper, ISettingsHelper, sections, settings } from '../state-helpers';
import { friendlyDate, getDate, getMoment } from '../util';

import './journalList.less';

interface IProps {
  dispatch: Dispatch<any>;
  entries: IEntryHelper;
  sections: ISectionHelper;
  settings: ISettingsHelper;
}

interface IListState {
  findOpen: boolean;
}

const TOOLBAR_HEIGHT = 52;
const CARD_HEIGHT = 206 + 15;

class JournalListComponent extends React.Component<IProps, IListState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      findOpen: false
    };
  }

  public componentDidMount() {
    document.title = 'Entries | Memoria';
    this.props.dispatch(setBreadcrumbs([{ label: 'Journals' }]));
  }

  public render() {
    return (
      <div className="journal-list">
        {!this.props.entries.items.length ? this.noJournals() : this.journalList()}
      </div>
    );
  }

  private newJournal = () => {
    this.props.dispatch(push('/journal/new'));
  }

  private noJournals = () => (
    <div className="exception-page">
      <h1>No journals <Icon type="exception" /></h1>
      <p>You currently have no journal entries</p>
      <Button size="large" icon="plus" onClick={this.newJournal}>Create new entry</Button>
    </div>
  )

  private journalList = () => {
    return (
      <AutoSizer>
        {({ height, width }) =>
          <div>
            {this.actionToolbar(width)}
            <List
              height={height - TOOLBAR_HEIGHT}
              width={width}
              overscanRowCount={12}
              rowCount={this.props.entries.nonDuplicates.length}
              rowHeight={CARD_HEIGHT}
              rowRenderer={this.journalCard}
            />
          </div>}
      </AutoSizer>
    );
  }

  private entryTitle = (entry: IEntry) => {
    const readable = readableTimeSince(getMoment(entry.date, this.props.settings));
    return `${friendlyDate(entry.date, this.props.settings)} | ${readable}`;
  }

  private journalCard = (values: { index: number, key: string, style: React.CSSProperties }) => {
    const entry = this.props.entries.nonDuplicates[values.index];
    const getScore = (s: ISection) => {
      if (!s.quality) {
        return NaN;
      } else if (s.quality.score > 0) {
        return 1;
      } else if (s.quality.score < 0) {
        return -1;
      } else {
        return 0;
      }
    };

    const entrySections = this.props.sections.byDate(entry.date)
      .filter((s) => s.quality);
    const sectionValues = sort(
      groupBy(entrySections, getScore)
        .filter((g) => !isNaN(g.property)),
      (g1, g2) => g2.property - g1.property
    );
    const colors = sectionValues.map((grp) => this.props.settings.colorForScore(grp.property, 1));
    const data = sectionValues.map((grp) => grp.count);
    const labels = sectionValues.map((grp) => {
      switch (grp.property) {
        case 1: return 'Good';
        case 0: return 'Average';
        case -1: return 'Bad';
        default: return '';
      }
    });
    return (
      <div key={values.key} style={{ ...values.style, height: CARD_HEIGHT }}>
        <Card
          title={this.entryTitle(entry)}
          className="journal-card"
          actions={[
            <Link key="1" className="journal-action" to={this.journalLink(entry)}><Icon type="book" /></Link>,
            <Popconfirm
              title="Are you sure you want to delete this journal entry?"
              key="2"
              okText="Delete"
              okType="danger"
              onConfirm={this.deleteJournal(entry)}
            >
              <a className="journal-action link-danger" href="#">
                <Icon type="delete" />
              </a>
            </Popconfirm>,
          ]}
          bordered={false}
        >
          <PercentageChart key={entry.date} data={data} labels={labels} colors={colors} />
        </Card>
      </div>
    );
  }

  private actionToolbar = (width: number) => {
    const TOOLBAR_PADDING = 10;
    return (
      <div
        className="action-toolbar"
        style={{ height: TOOLBAR_HEIGHT, width, paddingTop: TOOLBAR_PADDING }}
      >
        <Button icon="plus" onClick={this.newJournal}>New entry</Button>
        <PopupDatepicker
          open={this.state.findOpen}
          onOpenChange={this.showFindChange}
          disabledDate={this.nonExistentDate}
          onChange={this.goToDate}
          showInput={true}
          format={this.props.settings.date.dateFormat}
        />
        <Button icon="search" onClick={this.toggleFind}>
          Find date
        </Button>
        {this.props.entries.duplicates.length
          ? (
            <Badge count={this.props.entries.duplicates.length}>
              <Button onClick={this.goToDuplicates}>Duplicates</Button>
            </Badge>
          )
          : null}
      </div>
    );
  }

  private goToDuplicates = () => this.props.dispatch(push('/journal/duplicates'));

  private goToDate = (date: Moment) => {
    if (this.props.entries.existsOnDate(date))
      this.props.dispatch(push(`/journal/${getDate(date, this.props.settings)}`));
  }

  private nonExistentDate = (date: Moment) => {
    return !this.props.entries.existsOnDate(date);
  }

  private toggleFind = () => {
    this.setState({ findOpen: !this.state.findOpen });
  }

  private showFindChange = (status: boolean) => {
    this.setState({ findOpen: status });
  }

  private deleteJournal = (entry: IEntry) => () => {
    if (this.props.entries.deleting)
      return;

    this.props.dispatch(deleteJournal(entry));
  }

  private journalLink = (entry: IEntry) => `/journal/${entry.date}`;
}

const mapStateToProps = (state: IState) => ({
  entries: entries(state),
  sections: sections(state),
  settings: settings(state),
});

export default connect(mapStateToProps)(JournalListComponent);
