import React from 'react';
import { connect, Dispatch } from 'react-redux';
import { push } from 'react-router-redux';

import { Button, Card, Icon, List, Modal, Popconfirm } from 'antd';
import { Moment } from 'moment';
import ReactQuill from 'react-quill';

import { rest } from '../prelude';
import { friendlyDate } from '../util';

import { setBreadcrumbs } from '../actions/breadcrumbs.actions';
import { deleteJournal } from '../actions/entry.actions';
import { IEntry } from '../journal';
import { IState } from '../reducer';
import { IScreenState } from '../screen';
import { entries, IEntryHelper, ISettingsHelper, settings } from '../state-helpers';
import { IEntryDuplicate } from '../state-helpers/entries';

import './journalDuplicate.less';

interface IProps {
  dispatch: Dispatch<IState>;
  entries: IEntryHelper;
  settings: ISettingsHelper;
  screen: IScreenState;
}

interface IDuplicateState {
  openEntry?: IEntry;
}

const dateStr = (date: Moment) => date.format('YYYY-MM-DD');

class JournalDuplicates extends React.Component<IProps, IDuplicateState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  public componentDidMount() {
    this.props.dispatch(setBreadcrumbs([{
      label: 'Journals',
      link: '/journal',
    }, {
      label: 'Duplicates',
    }]));
  }

  public render() {
    const HEADER_FOOTER_PADDING = 55 + 53;
    const BODY_PADDING = 24;
    const MODAL_OFFSET = this.props.screen.height * 0.05;
    const MODAL_WIDTH = Math.min(this.props.screen.width * 0.95, 1200);
    const BODY_HEIGHT = (this.props.screen.height * 0.8) - HEADER_FOOTER_PADDING;

    const duplicates = this.props.entries.duplicates;

    if (!duplicates.length)
      return this.noDuplicates();

    return (
      <div className="journal-duplicates">
        <h1>Resolve duplicates</h1>
        {duplicates.map(this.duplicate)}
        <Modal
          style={{ top: MODAL_OFFSET }}
          title={this.state.openEntry ? this.title(this.state.openEntry.date) : ''}
          visible={this.state.openEntry !== undefined}
          onCancel={this.closeEntry}
          width={MODAL_WIDTH}
          footer={[
            <Button key="close" onClick={this.closeEntry}>Close</Button>
          ]}
          wrapClassName="duplicate-modal"
        >
          <div style={{ height: BODY_HEIGHT }}>
            {this.state.openEntry
              ? <ReactQuill value={this.state.openEntry.journal} readOnly={true} modules={{ toolbar: [] }} />
              : null}
          </div>
        </Modal>
      </div>
    );
  }

  private noDuplicates = () => (
    <div className="exception-page">
      <h1>No duplicates <Icon type="check-circle-o" /></h1>
      <p>You have no duplicates to resolve</p>
      <Button size="large" onClick={this.backToJournals}>Back to all journals</Button>
    </div>
  )

  private backToJournals = () => this.props.dispatch(push('/journal'));

  private duplicate = (duplicate: IEntryDuplicate) => {
    return (
      <Card
        key={duplicate.date}
        title={this.title(duplicate.date)}
        className="journal-card"
        bordered={false}
      >
        <List
          grid={{ gutter: 16, xs: 1, sm: 2, lg: 3, xl: 4 }}
          dataSource={duplicate.items}
          renderItem={this.duplicateEntry}
        />
      </Card>
    );
  }

  private duplicateEntry = (entry: IEntry, index: number) => (
    <List.Item>
      <Card title={`Entry ${index + 1}`}>
        <Button
          icon="book"
          onClick={this.openEntry(entry)}
          disabled={this.props.entries.delete.inProgress}
        >
          View
        </Button>
        <Popconfirm
          title="Are you sure delete the other entries?"
          onConfirm={this.pickEntry(entry)}
          okText="Yes"
        >
          <Button
            type="primary"
            icon="check"
            disabled={this.props.entries.delete.inProgress}
          >
            Pick
          </Button>
        </Popconfirm>
      </Card>
    </List.Item>
  )

  private pickEntry = (entry: IEntry) => () => {
    if (this.props.entries.delete.inProgress)
      return;

    const others = this.props.entries.items.filter((e) =>
      e.id !== entry.id
      && e.date === entry.date);
    if (!others.length) {
      return;
    }

    const deleteEntry = (other: IEntry): Promise<void> => {
      return new Promise((resolve, reject) => {
        this.props.dispatch(deleteJournal(other, {
          success: () => resolve(),
          error: () => reject()
        }));
      });
    };

    others.reduce(
      (acc, other) => acc.then(() => deleteEntry(other)),
      Promise.resolve<void>(undefined)
    );
  }

  private openEntry = (openEntry: IEntry) => () => this.setState({ openEntry });

  private closeEntry = () => this.setState({ openEntry: undefined });

  private title = (date: string) =>
    friendlyDate(date, this.props.settings)
}

const mapStateToProps = (state: IState) => ({
  entries: entries(state),
  settings: settings(state),
  screen: state.screen,
});

export default connect(mapStateToProps)(JournalDuplicates);
