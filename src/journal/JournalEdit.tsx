import * as React from 'react';

import { Button, Divider, Icon, Input, message, Spin } from 'antd';
import moment, { Moment } from 'moment';
import { DeltaStatic } from 'quill';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { match, Redirect } from 'react-router-dom';
import { push } from 'react-router-redux';
import { AutoSizer } from 'react-virtualized';
import { Dispatch } from 'redux';

import { Option, present, Result, update } from '../prelude';

import { setBreadcrumbs } from '../actions/breadcrumbs.actions';
import { updateJournal } from '../actions/entry.actions';
import Editor from '../editor/Editor';
import LoadingEditor from '../editor/LoadingEditor';
import { parseJournal } from '../editor/parser';
import { IEntry, ISection, Journal } from '../journal';
import { eql } from '../prelude/equal';
import { IState } from '../reducer';
import { IEntryState } from '../reducer/entry.reducer';
import { ISectionState } from '../reducer/section.reducer';
import { ISettingsState } from '../reducer/settings.reducer';
import { shortDateFromId } from '../settings/date-formats';
import { ISettings } from '../settings/settings';
import { entries, IEntryHelper, sections, settings } from '../state-helpers';

import './journalContainer.less';

interface IProps {
  dispatch: Dispatch<IState>;
  entries: IEntryHelper;
  section: ISectionState;
  settings: ISettingsState;
  match: match<{ date: string }>;
}

interface IEditState {
  parsedJournal?: Journal;
  parseError?: string;
}

const ACTIONS_HEIGHT = 41;

class JournalEdit extends React.Component<IProps, IEditState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      parsedJournal: undefined
    };
  }

  public componentDidMount() {
    if (!settings(this.props).loading)
      this.setDate(this.date());
  }

  public componentDidUpdate(prevProps: IProps) {
    Option.execute(
      (entry) => {
        if (this.props.entries.isDuplicate(this.date(), entry.id)) {
          message.error('Journal entry is a duplicate, please resolve which to keep');
          this.props.dispatch(push('/journal'));
        }
      },
      this.entry()
    );

    if (settings(prevProps).loading && !settings(this.props).loading)
      this.setDate(this.date());
  }

  public render() {
    if (this.props.entries.loading)
      return (
        <div>
          <div className="journal-editor-action" style={{ height: ACTIONS_HEIGHT }} />
          <LoadingEditor />
        </div>
      );

    return Option.match(
      (entry) => this.editor(entry),
      () => this.notFound(),
      this.entry()
    );
  }

  private notFound = () => (
    <div className="exception-page">
      <h1>Journal not found <Icon type="warning" /></h1>
      <p>We're sorry, but we couldn't find that journal!</p>
      <Button size="large" icon="left" onClick={this.discard}>Back to all journals</Button>
    </div>
  )

  private editor = (entry: IEntry) => {
    const updating = false;
    const modified = this.state.parsedJournal === undefined ||
      !eql(this.state.parsedJournal[0].journal, entry.journal);
    const dateFormat = shortDateFromId(entry.dateFormat).valueOrUndefined;
    return (
      <AutoSizer disableWidth={true}>
        {({ height }) => <div>
          <div className="journal-editor-action" style={{ height: ACTIONS_HEIGHT }}>
            {modified
              ? [
                <Button key={1} type="primary" icon="check" onClick={this.onSubmit} disabled={updating}>Update</Button>,
                <Button key={3} type="danger" icon="close" onClick={this.discard}>Discard</Button>
              ] : [<Button key={1} icon="left" onClick={this.discard}>Back to journals</Button>]}

          </div>
          <div style={{ height: height - ACTIONS_HEIGHT, backgroundColor: '#ffffff' }}>
            <Editor
              value={entry.journal}
              dateFormat={dateFormat}
              onDateChange={this.onDateChange}
              onChange={this.parse}
              disabledDate={this.disabledDate}
              defaultDate={this.date()}
              recommendedSections={sections(this.props).recommendations(5, this.sectionNames())}
            />
          </div>
        </div>}
      </AutoSizer >
    );
  }

  private sectionNames = () =>
    this.state.parsedJournal !== undefined
      ? this.state.parsedJournal[1].map((s) => s.name.toLowerCase())
      : []

  private disabledDate = (date: Moment) =>
    !date
    || (date.format(settings(this.props).date.dataDateFormat) !== this.dateParam()
      && this.props.entries.existsOnDate(date))

  private entry = () => this.props.entries.byDate(this.date());

  private date = () => moment(this.dateParam(), settings(this.props).date.dataDateFormat);

  private dateParam = () => this.props.match.params.date;

  private onSubmit = () => {
    const current = Option.except(new Error('Error occurred on journal entry update'), this.entry());

    if (this.state.parsedJournal) {
      const currentSections = sections(this.props).byDate(current.date);
      const journal: Journal = [
        update(this.state.parsedJournal[0], { id: current.id }),
        this.applySectionIds(currentSections, this.state.parsedJournal[1])
      ];

      this.props.dispatch(updateJournal(journal, {
        success: () => this.props.dispatch(push('/journal')),
        error: (err) => message.error(err)
      }));
    } else {
      const error = this.state.parseError || 'Cannot update invalid journal';
      message.error(error);
    }
  }

  private applySectionIds = (current: ISection[], parsed: ISection[]): ISection[] => {
    current.forEach((c) => {
      const index = parsed.findIndex((p) => p.name.toLowerCase() === c.name.toLowerCase());
      if (index >= 0) {
        parsed[index] = update(parsed[index], { id: c.id });
      }
    });

    return parsed;
  }

  private parse = (journal: DeltaStatic) => Result.match(
    (parsedJournal) => this.setState({ parsedJournal, parseError: undefined }),
    (parseError) => this.setState({ parsedJournal: undefined, parseError }),
    parseJournal(present(this.props.settings.settings))(journal)
  )

  private discard = () => this.props.dispatch(push('/journal'));

  private onDateChange = (date: Moment) => this.setDate(date);

  private setDate = (date: Moment) => {
    const dateString = date.format(settings(this.props).date.friendlyDateFormat);
    const shortDate = date.format(settings(this.props).date.dateFormat);
    document.title = `${shortDate} | Memoria`;

    this.props.dispatch(setBreadcrumbs([
      { label: 'Journals', link: '/journal', },
      { label: dateString },
    ]));
  }
}

const mapStateToProps = (state: IState) => ({
  entries: entries(state),
  section: state.section,
  settings: state.settings,
});

export default connect(mapStateToProps)(JournalEdit);
