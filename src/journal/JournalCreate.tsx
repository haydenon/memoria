import * as React from 'react';

import { Button, Divider, Input, message } from 'antd';
import moment, { Moment } from 'moment';
import Delta from 'quill-delta';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';
import { Redirect } from 'react-router-dom';
import { push } from 'react-router-redux';
import { AutoSizer } from 'react-virtualized';
import { Dispatch } from 'redux';

import { identity, Option, present, rest, Result } from '../prelude';

import { setBreadcrumbs } from '../actions/breadcrumbs.actions';
import { createJournal } from '../actions/entry.actions';
import EditorComponent from '../editor/Editor';
import LoadingEditor from '../editor/LoadingEditor';
import { parseJournal } from '../editor/parser';
import { IEntry, Journal } from '../journal';
import { IState } from '../reducer';
import { IEntryState } from '../reducer/entry.reducer';
import { ISectionState } from '../reducer/section.reducer';
import { ISettingsState } from '../reducer/settings.reducer';
import { IScreenState } from '../screen';
import { ISettings } from '../settings/settings';
import { entries, IEntryHelper, sections, settings } from '../state-helpers';

import './journalContainer.less';

interface IProps {
  dispatch: Dispatch<IState>;
  entries: IEntryHelper;
  section: ISectionState;
  settings: ISettingsState;
  screenWidth: IScreenState;
}

interface ICreateState {
  journal?: Delta;
  parsedJournal?: Journal;
  parseError?: string;
  initialDate?: Moment;
  previousEntry?: IEntry;
  editorValue?: Delta;
}

type ButtonSize = 'small' | 'default' | 'large';

class JournalCreate extends React.Component<IProps, ICreateState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  public componentDidMount() {
    if (!this.state.initialDate && !this.props.entries.loading) {
      const date = this.props.entries.nextValidDate(moment());
      const previousEntry = this.props.entries.previousEntry(date).valueOrUndefined;
      this.setState({ initialDate: date, previousEntry });
      this.setDate(date);
    }
  }

  public componentDidUpdate(prevProps: IProps) {
    if (this.state.initialDate && this.props.entries.isDuplicate(this.state.initialDate)) {
      message.error('Journal entry is a duplicate.');
      this.props.dispatch(push('/journal'));
    }

    if (!this.state.initialDate && !this.props.entries.loading) {
      const date = this.props.entries.nextValidDate(moment());
      const previousEntry = this.props.entries.previousEntry(date).valueOrUndefined;
      this.setState({ initialDate: date, previousEntry });
      this.setDate(date);
    }
  }

  public render() {
    return this.props.entries.loading
      ? <LoadingEditor />
      : this.editor();
  }

  private editor = () => {
    return (
      <AutoSizer disableWidth={true}>
        {({ height }) => <div>
          {this.actionToolbar()}
          <div style={{ height: height - this.toolbarHeight() }}>
            <EditorComponent
              defaultDate={this.props.entries.nextValidDate(moment())}
              value={this.state.editorValue}
              onDateChange={this.onDateChange}
              onChange={this.updateJournal}
              disabledDate={this.props.entries.existsOnDate}
              recommendedSections={sections(this.props).recommendations(5, this.sectionNames())}
            />
          </div>
        </div>}
      </AutoSizer >
    );
  }

  private toolbarHeight = () =>
    this.props.screenWidth.width >= 385 ? 41 : 35

  private actionToolbar = () => {
    const creating = this.props.entries.create.inProgress;
    return (
      <div className="journal-editor-action" style={{ height: this.toolbarHeight() }}>
        <Button type="primary" size={this.size()} icon={this.icon('plus')} onClick={this.onSubmit} disabled={creating}>
          Create
        </Button>
        <Button type="danger" size={this.size()} icon={this.icon('close')} onClick={this.discard}>Discard</Button>
        <Button
          size={this.size()}
          icon={this.icon('copy')}
          disabled={!this.state.previousEntry}
          onClick={this.copyPrevious}
        >
          {this.props.screenWidth.width < 420 ? 'Copy previous' : 'Copy previous entry'}
        </Button>
      </div>
    );
  }

  private size: () => ButtonSize = () =>
    this.props.screenWidth.width >= 385 ? 'default' : 'small'

  private icon: (name: string) => string | undefined = (name: string) =>
    this.props.screenWidth.width >= 335 ? name : undefined

  private sectionNames = () =>
    this.state.parsedJournal !== undefined
      ? this.state.parsedJournal[1].map((s) => s.name.toLowerCase())
      : []

  private onSubmit = () => {
    if (this.state.parsedJournal) {
      this.props.dispatch(createJournal(this.state.parsedJournal, {
        success: () => this.props.dispatch(push('/journal')),
        error: (err) => message.error(err)
      }));
    } else {
      const error = this.state.parseError || 'Cannot update invalid journal';
      message.error(error);
    }
  }

  private updateJournal = (journal: Delta) => {
    this.setState({ journal });
    Result.match(
      (parsedJournal) => this.setState({ parsedJournal, parseError: undefined }),
      (parseError) => this.setState({ parsedJournal: undefined, parseError }),
      parseJournal(present(this.props.settings.settings))(journal)
    );
  }

  private copyPrevious = () => {
    if (!this.state.previousEntry
      || !this.state.journal
      || !this.state.journal.ops
      || !this.state.previousEntry.journal.ops)
      return;

    const ops = [this.state.journal.ops[0], ...rest(this.state.previousEntry.journal.ops)];

    this.setState({ editorValue: new Delta(ops) });
  }

  private discard = () => this.props.dispatch(push('/journal'));

  private onDateChange = (date: Moment) => {
    this.setState({ initialDate: date });
    this.setDate(date);
  }

  private setDate = (date: Moment) => {
    const short = date.format(settings(this.props).date.dateFormat);
    document.title = `${short} | Memoria`;

    this.props.dispatch(setBreadcrumbs([
      { label: 'Journals', link: '/journal', },
      { label: date.format(settings(this.props).date.friendlyDateFormat) },
    ]));
  }
}

const mapStateToProps = (state: IState) => ({
  entries: entries(state),
  settings: state.settings,
  section: state.section,
  screenWidth: state.screen,
});

export default connect(mapStateToProps)(JournalCreate);
