import PouchDB from 'pouchdb';
import { error, Option, Result, throwIf, update } from '../prelude';
import {
  decrypt,
  encrypt,
  getRandomBytes,
  IEncryptedData,
  IEncryptedKey,
  isEncryptedData,
  toByteArray
} from './common';

const ENCRYPTION_KEY_TOKEN = 'encryptionkey';

const defaultIterations = 500000;

export interface IKeyStorageStrategy {
  getEncryptedKey(): Promise<IEncryptedKey | undefined>;
  putEncryptedKey(key: IEncryptedKey, isUpdate: boolean): Promise<IEncryptedKey>;
}

export class PouchDbStorageStrategy implements IKeyStorageStrategy {
  constructor(
    private configDb: PouchDB.Database<{}>
  ) { }

  public getEncryptedKey = async (): Promise<IEncryptedKey | undefined> => {
    const key = await this.configDb.get(ENCRYPTION_KEY_TOKEN).catch((err) => {
      if (err.name === 'not_found') {
        return undefined;
      } else {
        throw err;
      }
    }) as (IEncryptedKey | undefined);

    return key;
  }

  public putEncryptedKey = async (key: IEncryptedKey, isUpdate: boolean): Promise<IEncryptedKey> => {
    if (!isUpdate) {
      const resp = await this.configDb.putIfNotExists(key);
      return resp.updated ? key : this.configDb.get<IEncryptedKey>(ENCRYPTION_KEY_TOKEN);
    }

    const isModel = (m: any): m is IEncryptedKey =>
      m && m._id && m.data && m.iv;

    await this.configDb.upsert(key._id, (oldKey) => {
      if (!isModel(oldKey)) {
        return key;
      }

      return update(key, { _rev: oldKey._rev });
    });

    return key;
  }
}

export const importPassphrase = (key: any) => crypto.subtle.importKey(
  'raw',
  toByteArray(key),
  {
    name: 'PBKDF2',
  },
  false,
  ['deriveKey']
);

export const importKey = (key: JsonWebKey) => crypto.subtle.importKey(
  'jwk',
  key,
  {
    name: 'AES-CBC',
  },
  false,
  ['encrypt', 'decrypt']);

export const deriveKey = (key: CryptoKey, salt: Uint8Array, iterations = defaultIterations) => crypto.subtle.deriveKey(
  {
    name: 'PBKDF2',
    salt,
    iterations,
    hash: { name: 'SHA-256' },
  },
  key,
  {
    name: 'AES-CBC',
    length: 256,
  },
  false,
  ['encrypt', 'decrypt']
);

export const encryptKey = async (data: JsonWebKey, password: string, salt: Uint8Array): Promise<IEncryptedData> => {
  const key = await deriveKey(await importPassphrase(password), salt);
  const iv = getRandomBytes(16);
  const encrypted = await encrypt(data, key, iv);
  return { _id: '', data: encrypted, iv };
};

export const decryptKey =
  async (encrypted: IEncryptedData, password: string, salt: Uint8Array): Promise<JsonWebKey> => {
    const key = await deriveKey(await importPassphrase(password), salt);
    return await decrypt(encrypted.data, key, encrypted.iv) as JsonWebKey;
  };

const createKey = async () => crypto.subtle.generateKey({
  name: 'AES-CBC',
  length: 256
}, true, ['encrypt', 'decrypt']);

export class KeyStorageManager {
  private key?: CryptoKey;

  constructor(
    private strategy: IKeyStorageStrategy,
  ) { }

  public unlockKey = async (passphrase: string): Promise<boolean> =>
    Option.match(
      (ekey) => importKey(ekey)
        .then((key) => {
          this.key = key;
          return true;
        }),
      () => Promise.resolve(false),
      await this.getWebKey(passphrase)
    )

  public changePassphrase = async (oldPassphrase: string, newPassphrase: string): Promise<void> => {
    const key = Option.except(
      new Error('Old passphrase is invalid'),
      await this.getWebKey(oldPassphrase),
    );

    const err = 'Failed to change passphrase';

    this.key = undefined;
    await this.importJsonWebKey(key, newPassphrase, true)
      .catch(error(err));

    const valid = await this.testPassphrase(newPassphrase);
    throwIf(err, () => !valid);
  }

  public getKey = async (): Promise<Option<CryptoKey>> =>
    Option.fromUndefined(this.key)

  public testPassphrase = (passphrase: string) =>
    this.unlockKey(passphrase)

  public generateKey = async (passphrase: string): Promise<CryptoKey> => {
    const newKey = await createKey();
    const exported = await crypto.subtle.exportKey('jwk', newKey);

    return this.importJsonWebKey(exported, passphrase);
  }

  public importKey = async (key: JsonWebKey, passphrase: string): Promise<void> => {
    await this.importJsonWebKey(key, passphrase);
  }

  public getExportKey = async (passphrase: string): Promise<Result<JsonWebKey, string>> =>
    Result.fromOption(await this.getWebKey(passphrase), 'Failed to export key')

  private importJsonWebKey = async (webKey: JsonWebKey, passphrase: string, isUpdate = false): Promise<CryptoKey> => {
    const salt = getRandomBytes(16);
    const encryptedKey = await encryptKey(webKey, passphrase, salt) as IEncryptedKey;
    encryptedKey._id = ENCRYPTION_KEY_TOKEN;
    encryptedKey.salt = salt;

    const savedKey = await this.strategy.putEncryptedKey(encryptedKey, isUpdate);
    const savedCryptoKey = await importKey(await decryptKey(savedKey, passphrase, savedKey.salt));

    this.key = savedCryptoKey;
    return savedCryptoKey;
  }

  private getWebKey = async (passphrase: string): Promise<Option<JsonWebKey>> => {
    const storedKey = await this.strategy.getEncryptedKey();
    if (storedKey) {
      return decryptKey(storedKey, passphrase, storedKey.salt)
        .then((jwk) => Option.some<JsonWebKey>(jwk))
        .catch(() => Option.none<JsonWebKey>());
    }
    return Option.none();
  }
}
