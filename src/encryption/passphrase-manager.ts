import { DEFAULT_PASSPHRASE, getPassphrase } from '../account';
import { IPassphraseInformation, PASSPHRASE_INFORMATION_DEF } from '../data/account.model';
import { identity, not, throwIf } from '../prelude';
import MemoriaCrypto from './crypto';
import { KeyStorageManager } from './key-storage';

const PASSPHRASE_ID = PASSPHRASE_INFORMATION_DEF.getIdPrefix(undefined);

export default class PassphraseManager {
  private passphraseInfo?: IPassphraseInformation;

  constructor(
    private keyStorageManager: KeyStorageManager,
    private localDb: PouchDB.Database<{}>,
  ) { }

  public getPassphraseInformation = async (): Promise<boolean> => {
    if (this.passphraseInfo !== undefined) {
      return this.passphraseInfo.passphraseEnabled;
    }

    const passphraseInfo = await this.localDb.get<IPassphraseInformation>(PASSPHRASE_ID).catch(() => undefined);
    if (passphraseInfo === undefined) {
      return false;
    }

    this.passphraseInfo = passphraseInfo;

    return passphraseInfo.passphraseEnabled;
  }

  public testPassphrase = async (passphrase: string) => {
    if (this.passphraseInfo !== undefined && !this.passphraseInfo.passphraseEnabled) {
      return true;
    }

    const valid = await this.keyStorageManager.testPassphrase(passphrase);

    const promise = new Promise<void>((resolve) => {
      setTimeout(() => resolve(), 1000);
    });

    await promise;
    return valid;
  }

  public setPassphrase = async (passphrase: string | undefined) => {
    if (passphrase === undefined) {
      await this.keyStorageManager.unlockKey(DEFAULT_PASSPHRASE);
      return true;
    }

    return this.testPassphrase(passphrase);
  }

  public addPassphraseInformation = async (passphrase: string | undefined): Promise<void> => {
    const phrase = getPassphrase(passphrase);
    const valid = await this.keyStorageManager.unlockKey(phrase)
      .then(throwIf('Invalid passphrase', not));

    const info = {
      _id: PASSPHRASE_ID,
      passphraseEnabled: passphrase !== undefined
    };
    await this.localDb.putIfNotExists<IPassphraseInformation>(info);

    this.passphraseInfo = info;
  }

  public changePassphrase = async (oldPassphrase: string, newPassphrase: string | undefined) => {
    const newPhrase = getPassphrase(newPassphrase);
    await this.keyStorageManager.changePassphrase(oldPassphrase, newPhrase);

    await this.localDb.upsert<IPassphraseInformation>(PASSPHRASE_ID, (old: any) => ({
      _id: PASSPHRASE_ID,
      _rev: old && old._rev || undefined,
      passphraseEnabled: newPassphrase !== undefined
    }));
  }
}
