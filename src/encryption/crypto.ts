import { error, Option, Result } from '../prelude';
import {
  decrypt,
  encrypt,
  fromByteArray,
  getRandomBytes,
  IEncryptedData,
  IEncryptedKey,
  isEncryptedData,
  toByteArray
} from './common';
import { KeyStorageManager } from './key-storage';

// tslint:disable-next-line:interface-name
interface ArrayConstructor {
  from<T>(params: { length: number }, fill: () => T): T[];
}

export default class MemoriaCrypto {
  constructor(
    private readonly keyStorageManager: KeyStorageManager,
  ) { }

  public encryptData = async (data: any): Promise<IEncryptedData> => {
    const iv = getRandomBytes(16);
    const key = Option.except(
      new Error('Cannot encrypt data until encryption key created'),
      await this.keyStorageManager.getKey()
    );
    const enc = async () => encrypt(data, key, iv);

    return enc().then((encrypted) => ({ _id: '', data: encrypted, iv }))
      .catch(() => {
        throw new Error('Failed to encrypt data');
      });
  }

  public decryptData = async <T>(encrypted: IEncryptedData): Promise<T> => {
    const key = Option.except(
      new Error('Cannot decrypt data until encryption key created'),
      await this.keyStorageManager.getKey()
    );
    const dec = async () => decrypt(encrypted.data, key, encrypted.iv);
    return dec().then((item) => item as T).catch(() => {
      throw new Error('Failed to decrypt data');
    });
  }
}
