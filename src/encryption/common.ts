import { IModel } from '../data/model';

declare class TextEncoder {
  public encode(str: string): Uint8Array;
}

declare class TextDecoder {
  public decode(data: Uint8Array): string;
}

export interface IEncryptedData extends IModel {
  data: Uint8Array;
  iv: Uint8Array;
}

export const isEncryptedData = (item: any): item is IEncryptedData => item._id && item.data && item.iv;

export interface IEncryptedKey extends IEncryptedData {
  salt: Uint8Array;
}

export const toByteArray = (data: any) => new TextEncoder().encode(JSON.stringify(data));

export const fromByteArray = (data: Uint8Array) => JSON.parse(new TextDecoder().decode(data));

export const getRandomBytes = (length: number) => window.crypto.getRandomValues(new Uint8Array(length));

export const encrypt = (data: any, key: CryptoKey, iv: Uint8Array) => crypto.subtle.encrypt(
  {
    name: 'AES-CBC',
    iv,
  },
  key,
  toByteArray(data)
).then((encrypted) => new Uint8Array(encrypted));

export const decrypt = (encrypted: Uint8Array, key: CryptoKey, iv: Uint8Array) => crypto.subtle.decrypt(
  {
    name: 'AES-CBC',
    iv,
  },
  key,
  encrypted
).then((decrypted) => new Uint8Array(decrypted)).then(fromByteArray);

export const generateRandomPassword = (length: number) =>
  btoa(String.fromCharCode.apply(null, getRandomBytes(length)));
