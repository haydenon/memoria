import {
  getRandomBytes,
  IEncryptedKey,
} from './common';
import MemoriaCrypto from './crypto';
import {
  decryptKey,
  encryptKey,
  IKeyStorageStrategy,
  KeyStorageManager,
} from './key-storage';

// tslint:disable:no-unused-expression

const DEFAULT_PASS = 'test_passphrase';
const OTHER_PASS = 'other_passphrase';
const DEFAULT_SALT = getRandomBytes(16);

class TestStorageStrategy implements IKeyStorageStrategy {
  constructor(public key?: IEncryptedKey | undefined) { }

  public getEncryptedKey = (): Promise<IEncryptedKey | undefined> => {
    return Promise.resolve(this.key);
  }
  public putEncryptedKey = (key: IEncryptedKey): Promise<IEncryptedKey> => {
    this.key = key;
    return Promise.resolve(key);
  }
}

const encryptExportKey = async (
  exported: JsonWebKey,
  passphrase = DEFAULT_PASS,
  salt = DEFAULT_SALT) => encryptKey(exported, passphrase, salt)
    .then(async (data) => {
      const key = await data as IEncryptedKey;
      key.salt = salt;
      return key;
    });

const decryptExportKey = async (
  encrypted: IEncryptedKey,
  passphrase = DEFAULT_PASS) => decryptKey(encrypted, passphrase, encrypted.salt);

const createExportedKey = (passphrase = DEFAULT_PASS, salt = DEFAULT_SALT) => crypto.subtle.generateKey({
  name: 'AES-CBC',
  length: 256
}, true, ['encrypt', 'decrypt'])
  .then((key) => crypto.subtle.exportKey('jwk', key));

describe('crypto', () => {
  it('can decrypt encrypted key', async () => {
    const key = await createExportedKey();
    const encrypted = await encryptExportKey(key);

    const decrypted = await decryptExportKey(encrypted);
    chai.expect(decrypted).to.deep.equal(key);
  }).timeout(15000);

  it('can decrypt encrypted data', async () => {
    const storageStrategy = new TestStorageStrategy();
    const storageManager = new KeyStorageManager(storageStrategy);
    await storageManager.generateKey(DEFAULT_PASS);
    await storageManager.unlockKey(DEFAULT_PASS);
    const crypt = new MemoriaCrypto(storageManager);

    const data = { hello: 'こんにちは' };

    const encrypted = await crypt.encryptData(data);

    chai.expect(encrypted).to.not.deep.equal(data);
    chai.expect(encrypted.data).to.not.deep.equal(data);

    const decrypted = await crypt.decryptData(encrypted);
    chai.expect(decrypted).to.deep.equal(data);
  }).timeout(15000);

  it('can\'t decrypt data with different key', async () => {
    const storageStrategy = new TestStorageStrategy();
    const storageManager = new KeyStorageManager(storageStrategy);
    await storageManager.generateKey(DEFAULT_PASS);
    await storageManager.unlockKey(DEFAULT_PASS);
    const crypt = new MemoriaCrypto(storageManager);

    const data = { hello: 'こんにちは' };

    const encrypted = await crypt.encryptData(data);

    const otherKey = await encryptExportKey(await createExportedKey(OTHER_PASS));
    const newManager = new KeyStorageManager(new TestStorageStrategy(otherKey));
    await newManager.generateKey(OTHER_PASS);
    await newManager.unlockKey(OTHER_PASS);
    const newCrypt = new MemoriaCrypto(newManager);

    return newCrypt.decryptData(encrypted)
      .then(() => { throw new Error('Should throw error'); })
      .catch((err: Error) => {
        chai.expect(err.message).to.equal('Failed to decrypt data');
      });
  }).timeout(15000);
});
