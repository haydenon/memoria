import * as e6p from 'es6-promise';
e6p.polyfill();

import { createBrowserHistory } from 'history';
import moment from 'moment';
import PouchDB from 'pouchdb';
import { DeltaStatic } from 'quill';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { routerMiddleware } from 'react-router-redux';
import { Action, applyMiddleware, compose, createStore, Dispatch, Middleware, MiddlewareAPI, Store } from 'redux';
import thunk from 'redux-thunk';

import { checkAccount } from './actions/account.actions';
import { AccountService } from './data/account-service';
import { DataProvider } from './data/data-provider';
import { DataService } from './data/data-service';
import { ENTRY_DEF, mapEntryView, mapSectionViews, SECTION_DEF } from './data/journal.model';
import setupDataUpdates from './data/setupDataUpdates';
import MemoriaCrypto from './encryption/crypto';
import { KeyStorageManager, PouchDbStorageStrategy } from './encryption/key-storage';
import PassphraseManager from './encryption/passphrase-manager';
import { Entry, Section } from './journal';
import { IState, reducer } from './reducer';
import Routes from './Routes';
import { updateScreenWidth } from './screen';
import { IServices } from './services';

import 'react-quill/dist/quill.snow.css';
import './styles/index.less';

const localDb = new PouchDB('localMemoria', { auto_compaction: true });
const remoteDb = new PouchDB('remoteMemoria', { auto_compaction: true });
const keyStorageManager = new KeyStorageManager(new PouchDbStorageStrategy(localDb));
const passphraseManager = new PassphraseManager(keyStorageManager, localDb);
const crypt = new MemoriaCrypto(keyStorageManager);
const accountService = new AccountService(crypt, localDb, keyStorageManager.importKey);
const dataProvider = new DataProvider(crypt, accountService, localDb, remoteDb);
const dataService = new DataService(dataProvider);

// Turns class actions into plain js objects
const toPlainObject = ((_store: Store<IState>) => (next: Dispatch<IState>) => (action: Action) => {
  if (action.constructor.name !== 'Object') {
    return next(Object.keys(action)
      .reduce((obj, key) => {
        obj[key] = (action as any)[key];
        return obj;
      }, {} as any));
  }

  return next(action);
}) as any as Middleware;

const history = createBrowserHistory();

function getStore(): Store<IState> {
  const middleware = [applyMiddleware(...[
    thunk.withExtraArgument({
      dataService,
      keyStorageManager,
      accountService,
      passphraseManager,
    }),
    routerMiddleware(history),
    toPlainObject
  ])];
  if (process.env.NODE_ENV === 'production') {
    return createStore(
      reducer,
      compose(...middleware)
    ) as any;
  } else {
    const win = window as any;
    if (win.__REDUX_DEVTOOLS_EXTENSION__) {
      middleware.push(win.__REDUX_DEVTOOLS_EXTENSION__());
    }
    return (module.hot && module.hot.data && module.hot.data.store)
      ? module.hot.data.store
      : createStore(
        reducer,
        compose(...middleware)
      );
  }
}

const store = getStore();

setupDataUpdates(store.dispatch, dataProvider);

store.dispatch(checkAccount());

window.addEventListener('resize', () => {
  store.dispatch(updateScreenWidth());
});

const services: IServices = {
  accountService,
  keyStorageManager,
  passphraseManager,
};

ReactDOM.render(
  <Provider store={store} >
    <AppContainer>
      <Routes history={history} services={services} />
    </AppContainer>
  </Provider>,
  document.getElementById('app')
);

// Hot Module Replacement API
if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept();

  module.hot.dispose((data) => {
    data.store = store;
  });
}
