import React from 'react';

import { Spin } from 'antd';
import { AutoSizer } from 'react-virtualized';

import './loadingScreen.less';

export default class LoadingScreen extends React.Component<{}> {
  public render() {
    const spinSize = 50;
    return (
      <div className="loading-screen">
        <AutoSizer>
          {({ height, width }) => (
            <div
              className="spinner-container"
              style={{
                marginTop: (height / 2) - (spinSize / 2),
                marginLeft: (width / 2) - (spinSize / 2)
              }}
            >
              <Spin className="spinner" spinning={true} />
            </div>
          )}
        </AutoSizer>
      </div>
    );
  }
}
