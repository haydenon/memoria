import React from 'react';

import { DatePicker } from 'antd';
import { DatePickerProps } from 'antd/lib/date-picker/interface';
import moment, { Moment } from 'moment';

import './popupDatepicker.less';

interface IDatepickerState {
  openedAt: number;
}

const DEBOUNCE_TIME = 400;

type Props = DatePickerProps & {
  showInput?: boolean,
};

export default class PopupDatepicker extends React.Component<Props, IDatepickerState> {
  constructor(props: DatePickerProps) {
    super(props);
    this.state = {
      openedAt: 0
    };
  }

  public render() {
    return (
      <span className="popup-datepicker-input">
        <DatePicker
          {...this.props}
          placeholder={this.props.showInput ? this.props.format || 'YYYY-MM-DD' : this.props.placeholder}
          onOpenChange={this.datePickerOpenChange}
          prefixCls={`${!this.props.showInput ? 'no-input ' : ''} popup-datepicker-calendar ant-calendar`}
        />
      </span>
    );
  }

  public componentDidUpdate(prevProps: DatePickerProps) {
    if (this.props.open && !prevProps.open) {
      this.setState({ openedAt: moment.now() });
    }
  }

  private datePickerOpenChange = (status: boolean) => {
    if (this.props.onOpenChange && !status && moment.now() > this.state.openedAt + DEBOUNCE_TIME) {
      this.props.onOpenChange(status);
    }
  }
}
