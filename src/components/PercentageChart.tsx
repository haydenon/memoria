import React from 'react';

import Chart from 'frappe-charts';
import 'frappe-charts-styles';
import './percentageChart.less';

let chartId = 1;

interface IProps {
    data: number[];
    labels: string[];
    colors?: string[];
}

class PercentageChart extends React.Component<IProps> {
    private _id: number;
    private chart?: Chart;

    constructor(props: IProps) {
        super(props);
        this._id = chartId++;
    }

    public componentDidMount() {
        const data = {
            labels: this.props.labels,

            datasets: [
                {
                    values: this.props.data,
                },
            ]
        };
        const options = {
            parent: `#${this.id()}`,
            data,
            type: 'percentage',
            height: 80,
            colors: this.props.colors,
        };
        this.chart = new Chart(options);
    }

    public componentWillUnmount() {
        if (this.chart)
            this.chart.unbind_window_events();
    }

    public render() {
        return <div id={this.id()} />;
    }

    private id = () => `percentage_chart_${this._id}`;
}

export default PercentageChart;
