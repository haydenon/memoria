import { truthy } from '.';

const eqlObject = <T, K extends keyof T>(item1: T, item2: T) => {
  const keys = new Set<K>(Object.keys(item1).concat(Object.keys(item2)) as K[]);
  return Array.from(keys).every((key) => eql(item1[key], item2[key]));
};

const eqlArray = <T>(arr1: T[], arr2: T[]) => {
  return arr1.length === arr2.length
    && arr1.every((item1, index) => eql(item1, arr2[index]));
};

export const eql = <T>(item1: T, item2: T): boolean => {
  if (!truthy(item1) || !truthy(item2))
    return item1 === item2;

  const type = typeof item1;

  if (type !== typeof item2 || item1.constructor.name !== item2.constructor.name)
    return false;

  if (type !== 'object' && type !== 'function')
    return item1 === item2;

  if (item1 instanceof Array && item2 instanceof Array) {
    return eqlArray(item1, item2);
  }

  return eqlObject(item1, item2);
};
