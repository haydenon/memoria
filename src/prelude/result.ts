import { Option } from './option';

type Errorable = Error | string;

const toError = (error: Errorable) => {
  return typeof error === 'string'
    ? new Error(error)
    : error;
};

export class Result<T, E> {
  public static error = <T, E>(error: E) => {
    return new Result<T, E>(undefined, error);
  }

  public static success = <T, E>(value: T): Result<T, E> => {
    return new Result<T, E>(value);
  }

  public static bind = <T, E, R>(action: (t: T) => Result<R, E>, result: Result<T, E>): Result<R, E> => {
    return Result.match(
      (t) => action(t),
      (e) => Result.error(e),
      result
    );
  }

  public static match = <T, E, R>(success: (t: T) => R, error: (e: E) => R, result: Result<T, E>): R => {
    if (result.value !== undefined) {
      return success(result.value);
    } else {
      return error(result.error as E);
    }
  }

  public static map = <T, E, R>(transform: (item: T) => R, result: Result<T, E>): Result<R, E> => {
    return Result.match(
      (t) => Result.success(transform(t)),
      (e) => Result.error(e),
      result
    );
  }

  public static except = <T, E>(getError: (err: E) => Error, result: Result<T, E>): T => {
    return Result.match(
      (t) => t,
      (e) => { throw getError(e); },
      result
    );
  }

  public static value = <T, E extends Errorable>(result: Result<T, E>): T => {
    return Result.except<T, E>(toError, result);
  }

  public static fromOption = <T, E>(option: Option<T>, ifError: E): Result<T, E> => {
    return Option.match(
      (t) => Result.success<T, E>(t),
      () => Result.error<T, E>(ifError),
      option
    );
  }

  private constructor(
    private value?: T,
    private error?: E,
  ) { }

  public bind = <R>(action: (t: T) => Result<R, E>): Result<R, E> => Result.bind(action, this);
  public match = <R>(ifSome: (t: T) => R, ifNone: () => R): R => Result.match(ifSome, ifNone, this);
  public map = <R>(transform: (item: T) => R): Result<R, E> => Result.map(transform, this);
  public except = (getError: (err: E) => Error): T => Result.except(getError, this);
}
