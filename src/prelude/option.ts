import { noop } from '.';
import { Result } from './result';

export class Option<T> {
  public static none = <T>() => {
    return new Option<T>();
  }

  public static some = <T>(value: T): Option<T> => {
    return new Option<T>(value);
  }

  public static fromUndefined = <T>(value: T | undefined): Option<T> => {
    return new Option<T>(value);
  }

  public static bind = <T, R>(action: (t: T) => Option<R>, option: Option<T>): Option<R> => {
    return Option.match(
      (t) => action(t),
      () => Option.none(),
      option
    );
  }

  public static match = <T, R>(ifSome: (t: T) => R, ifNone: () => R, option: Option<T>): R => {
    if (option.hasValue) {
      return ifSome(option.value);
    } else {
      return ifNone();
    }
  }

  public static map = <T, R>(transform: (item: T) => R, option: Option<T>): Option<R> => {
    return Option.match(
      (t) => Option.some(transform(t)),
      () => Option.none(),
      option
    );
  }

  public static execute = <T>(action: (item: T) => void, option: Option<T>) => {
    Option.match(
      action,
      noop,
      option
    );
  }

  public static except = <T>(err: Error | string, option: Option<T>): T => {
    const error = typeof err === 'string'
      ? new Error(err)
      : err;

    return Option.match(
      (t) => t,
      (): T => { throw error; },
      option
    );
  }

  public static toResult = <T, E>(option: Option<T>, ifError: E): Result<T, E> => {
    return Option.match(
      (t) => Result.success<T, E>(t),
      () => Result.error<T, E>(ifError),
      option
    );
  }

  constructor(
    private val?: T
  ) { }

  public get value() {
    if (this.val === undefined) {
      throw new Error('Attempting to use undefined option value');
    }

    return this.val;
  }

  public get valueOrUndefined(): T | undefined {
    return this.val;
  }

  public get hasValue() {
    return this.val !== undefined;
  }

  public bind = <R>(action: (t: T) => Option<R>): Option<R> => Option.bind(action, this);
  public match = <R>(ifSome: (t: T) => R, ifNone: () => R): R => Option.match(ifSome, ifNone, this);
  public map = <R>(transform: (item: T) => R): Option<R> => Option.map(transform, this);
  public except = (error: Error): T => Option.except(error, this);
}
