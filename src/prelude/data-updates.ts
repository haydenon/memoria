export const update = <T, K extends keyof T>(item: T, updates: Partial<T>): T => {
  const setProperty = (obj: T, key: K): T => {
    const value = updateKeys.indexOf(key) >= 0
      ? updates[key] as T[K]
      : item[key];
    obj[key] = value;
    return obj;
  };
  const updateKeys = Object.keys(updates);
  const keys = new Set<K>(
    Object.keys(item)
      .concat(updateKeys) as K[]);
  return Array.from(keys).reduce(setProperty, {} as any as T);
};

export const updateArray = <T>(arr: T[], pred: (t: T, index: number) => boolean, updates: Partial<T>) => {
  const index = arr.findIndex(pred);
  if (index < 0) {
    return arr;
  }

  return [...arr.slice(0, index), update(arr[index], updates), ...arr.slice(index + 1, arr.length)];
};

export const deleteFromArray = <T>(arr: T[], pred: (t: T, index: number) => boolean) => {
  const index = arr.findIndex(pred);
  if (index < 0) {
    return arr;
  }

  return [...arr.slice(0, index), ...arr.slice(index + 1, arr.length)];
};
