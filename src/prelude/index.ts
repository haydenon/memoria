import { deleteFromArray, update, updateArray } from './data-updates';
import { eql } from './equal';
import { Option } from './option';
import { Result } from './result';

const identity = <T>(item: T): T => item;

const not = (bool: boolean) => !bool;

// tslint:disable-next-line:no-empty
const noop = () => { };

const rest = <T>(arr: T[]) => arr.slice(1, arr.length);

const truthy = <T>(item: T | undefined | null): item is T => item !== undefined && item !== null;

const filterType = <R>(items: any[], isType: (item: any) => item is R) =>
  items.filter(isType).map((item) => (item as any) as R);

const filterMap = <T, R>(map: (item: T) => R | undefined | null, items: T[]): R[] => {
  const mapped = items.map(map);
  return mapped.filter((i) => truthy(i)) as R[];
};

function* range(...args: number[]): IterableIterator<number> {
  let stop = -1;
  let start = 0;
  if (args.length === 1) {
    stop = args[0];
  } else if (args.length === 2) {
    start = args[0];
    stop = args[1];
  } else {
    throw new Error('Range usage: range([start], stop)');
  }

  let i = start;
  while (i < stop) {
    yield i++;
  }
}

const present = <T>(item: T | undefined): T => {
  if (truthy(item))
    return item as T;
  throw new Error('Value is undefined when expected to be valid value');
};

type Partial<T> = {
  [K in keyof T]?: T[K]
};

const error = <T>(err: Error | string) => (...args: any[]): T => {
  if (typeof err === 'string')
    throw new Error(err);
  throw err;
};

const throwIf = <T>(err: Error | string, pred: (t: T) => boolean) => (t: T) => {
  if (!pred(t)) {
    return t;
  }

  if (typeof err === 'string')
    throw new Error(err);
  throw err;
};

const throwReturn = <T, R>(err: Error | string, pred: (t: T) => boolean, r: R) => (t: T) => {
  if (!pred(t)) {
    return r;
  }

  if (typeof err === 'string')
    throw new Error(err);
  throw err;
};

const reverse = <T>(arr: T[]): T[] => new Array<T>().concat(arr).reverse();

const sort = <T>(arr: T[], comparison?: (t1: T, t2: T) => number): T[] =>
  new Array<T>().concat(arr).sort(comparison);

const groupBy = <T, TProp>(arr: T[], property: (t: T, index: number) => TProp) => {
  const distinct = Array.from(new Set<TProp>(arr.map(property)));
  return distinct.map((prop) => {
    const equalValues = arr.filter((item, ind) => property(item, ind) === prop);
    return {
      items: equalValues,
      property: prop,
      count: equalValues.length
    };
  });
};

const capitalise = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);

const toReadableString = (str: string) =>
  (!str || !str.length) ? str : str.split(/[A-Z][a-z]+/g).join(' ').toLowerCase();

const flatMap = <T, R>(map: (t: T) => R[], arr: T[]): R[] =>
  flatten(arr.map(map));

const flatten = <T>(arr: T[][]): T[] =>
  [].concat.apply([], arr);

const tee = (action: () => any): void => {
  const value = action();
};

export {
  toReadableString,
  sort,
  identity,
  not,
  noop,
  rest,
  Option,
  Result,
  truthy,
  filterType,
  filterMap,
  range,
  present,
  eql,
  error,
  throwIf,
  throwReturn,
  reverse,
  groupBy,
  capitalise,
  flatten,
  flatMap,
  tee,
  update,
  updateArray,
  deleteFromArray
};
