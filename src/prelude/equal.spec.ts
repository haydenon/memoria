import { eql } from './equal';

// tslint:disable:no-unused-expression

describe('eql', () => {
  describe('when checking equality', () => {
    it('returns true for basic objects', () => {
      const first = { a: 'hello', b: 'world', };
      const second = { a: 'hello', b: 'world', };
      chai.expect(eql(first, second)).to.be.true;
    });
    it('returns true for basic objects', () => {
      const first = ['hello', 'world'];
      const second = ['hello', 'world'];
      chai.expect(eql(first, second)).to.be.true;
    });
    it('returns true for nested objects and array', () => {
      const first = {
        a: 'hello', b: [
          'world',
          { key: 123, array: ['1', { [2]: '2' }, 3] }
        ]
      };
      const second = {
        a: 'hello', b: [
          'world',
          { key: 123, array: ['1', { [2]: '2' }, 3] }
        ]
      };
      chai.expect(eql(first, second)).to.be.true;
    });
  });

  describe('when checking inequality', () => {
    it('returns false for basic objects', () => {
      const first = { a: 'hello', b: 'world', };
      const second = { a: 'hello', b: 'bob', };
      chai.expect(eql(first, second)).to.be.false;
    });
    it('returns false for basic arrays', () => {
      const first = ['hello', 'world'];
      const second = ['hello', 'bob'];
      chai.expect(eql(first, second)).to.be.false;
    });
    it('returns true for nested objects and array', () => {
      const first = {
        a: 'hello', b: [
          'world',
          { key: 123, array: ['1', { [2]: 'a' }, 3] }
        ]
      };
      const second = {
        a: 'hello', b: [
          'world',
          { key: 123, array: ['1', { [2]: '2' }, 3] }
        ]
      };
      chai.expect(eql(first, second)).to.be.false;
    });
  });
});
