import React from 'react';
import { connect, Dispatch } from 'react-redux';
import { push, RouterState } from 'react-router-redux';

import { Option, Result, tee } from '../prelude';

import { Button, Card, Col, Icon, Input, message, Row, Spin } from 'antd';

import { CopyToClipboard } from 'react-copy-to-clipboard';
import { enableSync } from '../actions/account.actions';
import LoadingScreen from '../components/LoadingScreen';
import { ISyncModel } from '../data/account.model';
import { IState } from '../reducer';
import { IScreenState } from '../screen';
import { getFriendlyCode, getSyncCode } from '../settings/sync-code';
import { account, IAccountHelper } from '../state-helpers';

interface IBackupState {
  key?: JsonWebKey;
  syncDetails?: ISyncModel;
}

interface IProps {
  dispatch: Dispatch<IState>;
  routing: RouterState;
  account: IAccountHelper;
  screen: IScreenState;
  getKey: (passphrase: string) => Promise<Result<JsonWebKey, string>>;
  getSyncDetails: () => Promise<Option<ISyncModel>>;
}

class BackupScreen extends React.Component<IProps, IBackupState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  public componentDidMount() {
    document.title = 'Backup | Memoria';

    this.checkInitialised();
    this.loadCode();
  }

  public componentDidUpdate() {
    this.checkInitialised();
    this.loadCode();
  }

  public render() {
    if (this.props.account.checking)
      return <LoadingScreen />;

    return (
      <div className="passphrase-screen">
        <div className="passphrase-content">
          {this.props.account.syncEnabled
            ? this.backupCode()
            : this.enableSync()}
        </div>
      </div>
    );
  }

  private checkInitialised() {
    if (!this.props.account.checking && this.props.account.waitingForPassphrase) {
      this.props.dispatch(push('/start/passphrase'));
    } else if (!this.props.account.checking && !this.props.account.initialised) {
      this.props.dispatch(push('/start'));
    } else if (!this.props.account.checking && !this.passphrase()) {
      this.props.dispatch(push('/'));
    }
  }

  private backupCode = () => {
    const code = this.getBackupCode();
    const SPIN_WIDTH = 32;
    const BUTTON_WIDTH = 90;
    return (
      <div>
        <h1>Backup</h1>
        <Card>
          {code === undefined
            ? (
              <div style={{ width: SPIN_WIDTH, marginLeft: 'auto', marginRight: 'auto' }}>
                <Spin size="large" />
              </div>
            )
            : (
              <div style={{ width: '100%' }}>
                <p>
                  To ensure you can recover from losing data on one device you should keep
                  this private code in a secret and secure place like a password manager, or connect multiple
                  devices to your account.
                <br />
                  <br />
                  If you have your private code, you will always be able to recover even if you
                  forget all your device passphrases.
                </p>
                <div style={{ marginBottom: 10 }}>
                  <Input
                    value={code}
                    readOnly={true}
                    style={{ width: `calc(100% - ${BUTTON_WIDTH + 10}px)`, marginRight: 10 }}
                  />
                  <CopyToClipboard text={code} onCopy={this.copyCode}>
                    <Button icon="copy">Copy</Button>
                  </CopyToClipboard>
                </div>
                <Button type="primary" onClick={this.skip}>Done</Button>
              </div>
            )}
        </Card>
      </div>
    );
  }

  private enableSync = () => {
    const deviceIcon = this.props.screen.width >= 1200
      ? 'desktop'
      : this.props.screen.width >= 500
        ? 'tablet'
        : 'mobile';
    return (
      <div className="exception-page">
        <h1>Backup <Icon type="sync" style={{ fontSize: '1.1em' }} /></h1>
        <p className="with-small-text">
          Your data is stored on your device, but you can sync your data between devices.
          Only your devices can ever read and write your data.
          <br />
          <br />
          If you enable sync and keep a secure backed up copy of your private code, then you will be able to retrieve
          your data even if you clear this browser's data, lose access to it or forget your passphrase.
      </p>
        <div>
          <Button size="large" icon="cloud" onClick={this.enable} disabled={this.props.account.enablingSync}>
            Sync
          </Button>
          <Button size="large" icon={deviceIcon} onClick={this.skip} disabled={this.props.account.enablingSync}>
            Keep on device
          </Button>
        </div>
      </div>
    );
  }

  private copyCode = (_text: string, result: boolean) => {
    if (!result)
      message.warning('Unable to copy text');
  }

  private enable = () => this.props.dispatch(enableSync());

  private skip = () => this.props.dispatch(push('/'));

  private getBackupCode = () => {
    const key = this.state.key;
    const sync = this.state.syncDetails;

    if (key === undefined || sync === undefined)
      return undefined;

    return getFriendlyCode(getSyncCode(key, sync));
  }

  private passphrase = () => this.props.routing.location !== null
    ? this.props.routing.location.state
    && this.props.routing.location.state.passphrase as string
    : undefined

  private loadCode = () => {
    const passphrase = this.passphrase();
    if (!passphrase)
      return;

    if (this.props.account.syncEnabled
      && passphrase !== undefined
      && !this.state.key) {
      this.props.getKey(passphrase)
        .then((res) => Result.match(
          (key) => this.setState({ key }),
          () => tee(message.error('Failed to retrieve key', 3000)),
          res
        ));
    }
    if (this.props.account.syncEnabled && !this.state.syncDetails) {
      this.props.getSyncDetails()
        .then((opt) => Option.match(
          (syncDetails) => this.setState({ syncDetails }),
          () => tee(message.error('Failed to retrive sync details')),
          opt
        ));
    }
  }
}

const mapStateToProps = (state: IState) => ({
  routing: state.routing,
  account: account(state),
  screen: state.screen,
});

export default connect(mapStateToProps)(BackupScreen);
