import React, { ChangeEvent } from 'react';

import { Button, Card, Form, Input } from 'antd';

import './passphrase.less';

interface IProps {
  onCreate: (passphrase: string | undefined) => void;
}

interface IPassphraseState {
  create: boolean;
  passphrase: string;
  validation?: string;
  confirmPassphrase: string;
  confirmValidation?: string;
}

type PassphraseKey = 'passphrase' | 'confirmPassphrase';

class ChoosePassphrase extends React.Component<IProps, IPassphraseState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      create: false,
      passphrase: '',
      confirmPassphrase: '',
    };
  }

  public render() {
    return (
      <div className="passphrase-screen">
        <div className="passphrase-content">
          <h1>Choose a passphrase</h1>
          <Card>
            {this.state.create
              ? this.createForm()
              : this.explanation()}
          </Card>
        </div>
      </div>);
  }

  private createForm = () => (
    <div>
      <Form.Item className={this.getHelp(false) ? 'has-error' : ''} help={this.getHelp(false)}>
        <Input
          type="password"
          placeholder="Passphrase"
          onChange={this.setValue(false)}
          onPressEnter={this.submitPassphrase}
        />
      </Form.Item>
      <Form.Item className={this.getHelp(true) ? 'has-error' : ''} help={this.getHelp(true)}>
        <Input
          type="password"
          placeholder="Confirm passphrase"
          onChange={this.setValue(true)}
          onPressEnter={this.submitPassphrase}
        />
      </Form.Item>
      <Button icon="key" onClick={this.submitPassphrase}>Choose passphrase</Button>
    </div>
  )

  private explanation = () => (
    <div>
      <p>
        You can choose a passphrase to protect your data.
        This will stop anyone from being able to access your journals on this device.
        This passphrase is device specific and you can have a different, or even no, passphrase on another device.
        <br />
        <br />
        Your data is encrypted on this device and we cannot access your data even without a passphrase and sync enabled.
        Because of this, please be aware that if you forget it,
        <i><strong> it is not possible to reset your passphrase or retrive data from this device.</strong></i>
        <br />
        <br />
        If you do forget, there is no limit to the number of unlock attempts you may make.
        It may not be a good idea unless you have a passphrase you are absolutely sure you can remember or you
        have enabled sync and have either another unlocked device or a backed up copy of your private code.
      </p>
      <div>
        <Button onClick={this.createPhrase}>I still want a passphrase</Button>
        <Button type="primary" onClick={this.onSkip}>I'm happy without a passphrase</Button>
      </div>
    </div>
  )

  private submitPassphrase = () => {
    const passed = this.validate('both');
    const match = this.state.passphrase === this.state.confirmPassphrase;
    if (passed && match) {
      this.props.onCreate(this.state.passphrase);
    }
  }

  private getHelp = (confirm: boolean) => {
    return confirm ? this.state.confirmValidation : this.state.validation;
  }

  private setValue = (confirm: boolean) => (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    if (confirm) {
      this.setState({ confirmPassphrase: value });
    } else {
      this.setState({ passphrase: value });
    }

    this.validate(confirm, value);
  }

  private validate = (specifier: boolean | 'both', providedValue?: string) => {
    const validateOne = (confirm: boolean, value?: string) => {
      const pass = this.state.passphrase;
      value = value !== undefined
        ? value
        : (!confirm ? pass : this.state.confirmPassphrase);

      let validation: string | undefined;

      if (!value.trim()) {
        validation = 'Must provide a value';
      } else if (confirm && value !== pass) {
        validation = 'Must match password';
      }

      if (confirm) {
        this.setState({ confirmValidation: validation });
      } else {
        this.setState({ validation });
      }

      return validation === undefined;
    };

    switch (specifier) {
      case true:
      case false:
        return validateOne(specifier, providedValue);
      case 'both':
        const conf = validateOne(true);
        const norm = validateOne(false);
        return conf && norm;
    }
  }

  private createPhrase = () => this.setState({ create: true });

  private onSkip = () => this.props.onCreate(undefined);
}

export default ChoosePassphrase;
