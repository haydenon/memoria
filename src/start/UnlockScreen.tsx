import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';
import { push } from 'react-router-redux';

import { Button, Card, Form, Input, message } from 'antd';
import { setPassphrase } from '../actions/account.actions';
import { IState } from '../reducer';
import { IAccountState } from '../reducer/account.reducer';

interface IProps {
  dispatch: Dispatch<IState>;
  account: IAccountState;
}

interface IUnlockState {
  passphrase: string;
  attempting: boolean;
}

class UnlockScreen extends React.Component<IProps, IUnlockState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      passphrase: '',
      attempting: false,
    };
  }

  public componentDidUpdate() {
    this.checkInitialised();
  }

  public render() {
    return (
      <div className="passphrase-screen">
        <div className="passphrase-content unlock">
          <h1>Unlock</h1>
          <Card>
            <Form.Item>
              <Input placeholder="Passphrase" type="password" onChange={this.setPhrase} onPressEnter={this.unlock} />
            </Form.Item>
            <Button icon="key" type="primary" onClick={this.unlock} disabled={this.state.attempting}>
              Unlock
            </Button>
          </Card>
        </div>
      </div>
    );
  }

  private setPhrase = (event: ChangeEvent<HTMLInputElement>) => {
    this.setState({ passphrase: event.target.value });
  }

  private unlock = () => {
    if (!this.state.passphrase) {
      message.warning('Please insert your passphrase');
      return;
    }

    this.setState({ attempting: true });
    this.props.dispatch(setPassphrase(this.state.passphrase, () => {
      message.error('Incorrect passphrase');
      this.setState({ attempting: false });
    }));
  }

  private checkInitialised = () => {
    if (!this.props.account.checking && this.props.account.initialised) {
      this.props.dispatch(push('/'));
    } else if (!this.props.account.checking && !this.props.account.waitingForPassphrase) {
      this.props.dispatch(push('/start'));
    }
  }
}

const mapStateToProps = (state: IState) => ({
  account: state.account,
});

export default connect(mapStateToProps)(UnlockScreen);
