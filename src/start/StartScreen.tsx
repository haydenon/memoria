import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { push } from 'react-router-redux';
import { ThunkAction } from 'redux-thunk';

import { Button, Card, Col, Divider, Icon, message, Row } from 'antd';
import { AutoSizer } from 'react-virtualized';

import { DEFAULT_PASSPHRASE, getPassphrase } from '../account';
import { createAccount, PassphraseRequired } from '../actions/account.actions';
import LoadingScreen from '../components/LoadingScreen';
import { IState } from '../reducer';
import { IAccountState } from '../reducer/account.reducer';
import { IScreenState } from '../screen';
import { account } from '../state-helpers';
import ChoosePassphrase from './ChoosePassphrase';

import './startScreen.less';

interface IProps {
  dispatch: Dispatch<IState>;
  screenWidth: IScreenState;
  account: IAccountState;
}

interface IStartState {
  enterPassphrase: boolean;
  createdAccount: boolean;
  setPassphrase?: string;
}

class StartScreen extends React.Component<IProps, IStartState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      createdAccount: false,
      enterPassphrase: false,
    };
  }

  public componentDidMount() {
    this.checkInitialised();
  }

  public componentDidUpdate() {
    this.checkInitialised();
  }

  public render() {
    if (this.props.account.checking)
      return <LoadingScreen />;

    if (this.state.enterPassphrase)
      return <ChoosePassphrase onCreate={this.createAccount} />;

    const smallScreen = this.props.screenWidth.width < 1200;
    const HEADER_HEIGHT = 63 + 21;
    const dividerHeight = (height: number) => smallScreen
      ? 1
      : Math.min(height - HEADER_HEIGHT - 20, 280);
    const disabled = account(this.props).initialising;
    return (
      <div className="start-screen">
        <div className="start-options">
          <AutoSizer>
            {({ width, height }) => <div style={{ width, height, overflowY: 'auto' }}>
              <h1>Welcome!</h1>
              <Row>
                <Col xl={11} lg={24}>
                  <Card className="start-card">
                    <h2>Create a new account</h2>
                    <div className="start-card-icon"><Icon type="user-add" /></div>
                    <div className="start-card-button">
                      <Button size="large" type="primary" icon="plus" disabled={disabled} onClick={this.getPassphrase}>
                        Create account
                      </Button>
                    </div>
                  </Card>
                </Col>
                <Col xl={2} lg={24}>
                  <div className="divider">
                    <Divider type={smallScreen ? 'horizontal' : 'vertical'} style={{ height: dividerHeight(height) }} />
                  </div>
                </Col>
                <Col xl={11} lg={24} className="start-card">
                  <Card>
                    <h2>Connect to existing account</h2>
                    <div className="start-card-icon"><Icon type="sync" /></div>
                    <div className="start-card-button">
                      <Button size="large" type="primary" icon="link" disabled={disabled} onClick={this.connectAccount}>
                        Connect to account
                      </Button>
                    </div>
                  </Card>
                </Col>
              </Row>
            </div>}
          </AutoSizer>
        </div>
      </div>
    );
  }

  private checkInitialised = () => {
    if (!this.props.account.checking && this.props.account.waitingForPassphrase) {
      this.props.dispatch(push('/start/passphrase'));
    } else if (!this.props.account.checking
      && this.props.account.initialised
      && this.state.createdAccount) {
      this.props.dispatch(push('/start/backup', { passphrase: getPassphrase(this.state.setPassphrase) }));
    } else if (!this.props.account.checking && this.props.account.initialised) {
      this.props.dispatch(push('/'));
    }
  }

  private getPassphrase = () => {
    document.title = 'Create passphrase';
    this.setState({ enterPassphrase: true });
  }

  private createAccount = (passphrase: string | undefined) => {
    this.setState({ createdAccount: true, setPassphrase: passphrase });
    this.props.dispatch(createAccount(
      passphrase,
      (prom) => prom.catch((err) => message.error(err.message)),
    ));
  }

  private connectAccount = () => {
    this.props.dispatch(push('/start/import'));
  }
}

const mapStateToProps = (state: IState) => ({
  screenWidth: state.screen,
  account: state.account,
});

export default connect(mapStateToProps)(StartScreen);
