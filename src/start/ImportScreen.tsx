import React from 'react';
import { connect, Dispatch } from 'react-redux';
import { push } from 'react-router-redux';

import { Button, Card, Form, Input, message, Progress, Select } from 'antd';
import { SelectValue } from 'antd/lib/select';
import Instascan, { ICamera, IScanner } from 'instascan';
import { AutoSizer } from 'react-virtualized';

import { Option, tee } from '../prelude';

import { DEFAULT_PASSPHRASE, getPassphrase, IAccountCode } from '../account';
import { importAccount, importAccountData } from '../actions/account.actions';
import LoadingScreen from '../components/LoadingScreen';
import { IState } from '../reducer';
import { IAccountState } from '../reducer/account.reducer';
import { IScreenState } from '../screen';
import ChoosePassphrase from './ChoosePassphrase';

import './importScreen.less';

const SOption = Select.Option;

const toCode = (str: string): Option<IAccountCode> => {
  try {
    const code = JSON.parse(str);
    if (code && code.key && code.password && code.user) {
      return Option.some(code as IAccountCode);
    }

    return Option.none();
  } catch {
    return Option.none();
  }
};

interface IImportState {
  enterPassphrase: boolean;
  decodedAccount?: IAccountCode;
  scanCode: boolean;
  selectedCamera?: ICamera;
  cameras: ICamera[];
  scanner?: IScanner;
  progress?: { done: number, total: number };
}

interface IProps {
  dispatch: Dispatch<IState>;
  account: IAccountState;
  screen: IScreenState;
}

class ImportScreen extends React.Component<IProps, IImportState> {
  private codeInp: Input | null = null;

  constructor(props: IProps) {
    super(props);
    this.state = {
      enterPassphrase: false,
      scanCode: false,
      cameras: [],
    };
  }

  public componentDidMount() {
    document.title = 'Connect account | Memoria';
    this.checkInitialised();
  }

  public componentDidUpdate(prevProps: IProps, prevState: IImportState) {
    this.checkInitialised();

    if (!prevState.scanCode && this.state.scanCode) {
      this.startScanner();
    }
  }

  public componentWillUnmount() {
    if (this.state.scanner)
      this.state.scanner.stop();
  }

  public render() {
    if (this.props.account.checking)
      return <LoadingScreen />;

    if (this.state.enterPassphrase)
      return <ChoosePassphrase onCreate={this.onPassphraseCreate} />;

    const HEADER_HEIGHT = 73 + 25;
    return (
      <div className="start-screen">
        <div className="start-import">
          <AutoSizer>
            {({ height, width }) =>
              <div style={{ height, width }}>
                {this.props.account.dataImport.inProgress
                  ? this.dataImport()
                  : <div>
                    <h1>Connect to existing account</h1>
                    {this.state.scanCode
                      ? this.scanCode(width, height - HEADER_HEIGHT)
                      : this.enterCode()}
                  </div>
                }
              </div>}
          </AutoSizer>
        </div>
      </div>
    );
  }

  private dataImport = () => {
    const progress = (done: number, total: number) => {
      const percent = Math.round((done / total) * 100);
      return (
        <div style={{ width: 120, height: 120, marginRight: 'auto', marginLeft: 'auto' }}>
          <Progress percent={percent} type="circle" />
        </div>
      );
    };
    return (
      <Card>
        <h2>Importing and encrypting your data!</h2>
        {this.state.progress !== undefined
          ? progress(this.state.progress.done, this.state.progress.total)
          : null
        }
      </Card>
    );
  }

  private enterCode = () => (
    <Card>
      <Form.Item label="Insert key">
        <Input size="large" ref={(inp) => this.codeInp = inp} />
      </Form.Item>
      <div>
        <Button type="primary" icon="link" size={this.buttonSize()} onClick={this.decodeInsertedCode}>
          Connect
        </Button>
        <span>or</span>
        <Button icon="qrcode" size={this.buttonSize()} onClick={this.toggleMode}>Scan QR Code</Button>
      </div>
    </Card>
  )

  private buttonSize = () => {
    if (this.props.screen.width >= 385) {
      return 'large';
    } else if (this.props.screen.width >= 360) {
      return 'default';
    } else {
      return 'small';
    }
  }

  private scanCode = (width: number, height: number) => {
    const EXTRAS_MARGIN = 10;
    const SELECT_CAMERA = 40;
    const CODE_BUTTON_HEIGHT = 40;
    const CARD_PADDING = 24;

    const videoHeight = height - CODE_BUTTON_HEIGHT - (EXTRAS_MARGIN * 2) - (CARD_PADDING * 2) - SELECT_CAMERA;
    const videoWidth = width - (CARD_PADDING * 2);

    return (
      <Card style={{ width, height }}>
        <div style={{ height: SELECT_CAMERA, marginBottom: EXTRAS_MARGIN }}>
          <Button icon="swap" type="primary" disabled={this.state.cameras.length < 2} onClick={this.nextCamera}>
            Switch camera
          </Button>
        </div>
        <div style={{ height: videoHeight, width: videoWidth }}>
          <video id="qr-preview" height={videoHeight} width={videoWidth} />
        </div>
        <div style={{ height: CODE_BUTTON_HEIGHT, marginTop: EXTRAS_MARGIN }}>
          <Button size="large" icon="key" onClick={this.toggleMode}>Enter code</Button>
        </div>
      </Card>
    );
  }

  private nextCamera = () => {
    const current = this.state.cameras.findIndex((cam) => cam.id === this.selectedCameraId());
    let camera: ICamera | undefined;
    if (current < 0 && this.state.cameras.length) {
      camera = this.state.cameras[0];
    } else if (current >= 0) {
      const nextInd = (current + 1) % this.state.cameras.length;
      camera = this.state.cameras[nextInd];
    }

    if (camera) {
      this.setState({ selectedCamera: camera });
      if (this.state.scanner) {
        this.state.scanner.stop();
        this.state.scanner.start(camera);
      }
    }
  }

  private selectedCameraId = () =>
    this.state.selectedCamera
      ? this.state.selectedCamera.id
      : ''

  private startScanner = () => {
    const video = document.getElementById('qr-preview');
    if (!video) {
      return;
    }

    const options = {
      video,
      mirror: false,
    };
    const scanner = new Instascan.Scanner(options);
    this.setState({ scanner });
    scanner.addListener('scan', this.decodeScannedCode);

    Instascan.Camera.getCameras()
      .then((cameras) => {
        if (cameras.length < 1) {
          message.error('No cameras');
          this.setState({ cameras: [] });
        } else {
          const camera = cameras[0];
          scanner.start(camera);
          this.setState({ cameras, selectedCamera: camera });
        }
      });
  }

  private stopScanner = () => {
    if (!this.state.scanner) {
      this.setState({ cameras: [] });
      return;
    }

    this.state.scanner.stop();
    this.setState({ cameras: [], scanner: undefined });
  }

  private decodeInsertedCode = () => {
    if (!this.codeInp) {
      return;
    }
    let value = '';

    try {
      value = atob(this.codeInp.input.value.trim());
    } catch {
      message.error('Code provided is invalid');
      return;
    }

    Option.match(
      this.importAccount,
      () => tee(message.error('Code provided is invalid')),
      toCode(value)
    );
  }

  private decodeScannedCode = (content: string) => {
    Option.match(
      this.importAccount,
      () => tee(message.warning('Detected invalid code')),
      toCode(content)
    );
  }

  private importAccount = (code: IAccountCode) => {
    if (this.state.scanner)
      this.state.scanner.stop();
    this.setState({
      enterPassphrase: true,
      decodedAccount: code
    });
  }
  private onPassphraseCreate = (passphrase: string | undefined) => {
    if (!this.state.decodedAccount) {
      return;
    }

    const code = { ...this.state.decodedAccount, passphrase };
    this.props.dispatch(importAccount(code));
  }

  private toggleMode = () => {
    if (this.state.scanCode) {
      this.stopScanner();
    } else {
      this.codeInp = null;
    }

    this.setState({
      scanCode: !this.state.scanCode
    });
  }

  private checkInitialised = () => {
    if (!this.props.account.checking && this.props.account.waitingForPassphrase) {
      this.props.dispatch(push('/start/passphrase'));
    } else if (!this.props.account.checking && this.props.account.initialised && !this.state.progress) {
      this.setState({ enterPassphrase: false });
      const progressListener = (done: number, total: number) => {
        if (done === total) {
          this.props.dispatch(push('/'));
        } else {
          this.setState({ progress: { done, total } });
        }
      };

      this.props.dispatch(importAccountData({ progress: progressListener }));
    }
  }
}

const mapStateToProps = (state: IState) => ({
  account: state.account,
  screen: state.screen,
});

export default connect(mapStateToProps)(ImportScreen);
