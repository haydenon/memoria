export const DEFAULT_PASSPHRASE = 'MEMORIA_DEFAULT_PASSPHRASE';

export const getPassphrase = (passphrase: string | undefined) =>
  passphrase !== undefined
    ? passphrase
    : DEFAULT_PASSPHRASE;

export interface ISync {
  enabled: boolean;
}

export interface IAccount {
  sync: ISync;
}

export interface IAccountCode {
  passphrase: string | undefined;
  key: JsonWebKey;
  user: string;
  password: string;
}
