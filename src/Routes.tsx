import * as PropTypes from 'prop-types';
import * as React from 'react';
import { Route, Switch } from 'react-router';
import { Redirect } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import LayoutRoute from './LayoutRoute';
import BackupScreen from './start/BackupScreen';
import ImportScreen from './start/ImportScreen';
import StartScreen from './start/StartScreen';
import UnlockScreen from './start/UnlockScreen';

import JournalCreate from './journal/JournalCreate';
import JournalDuplicates from './journal/JournalDuplicates';
import JournalEdit from './journal/JournalEdit';
import JournalList from './journal/JournalList';
import routes from './route-list';
import { IServices } from './services';
import GeneralSettings from './settings/general/GeneralSettings';
import SyncSettings from './settings/SyncSettings';

interface IProps {
  history: any;
  services: IServices;
}

class Routes extends React.Component<IProps> {
  private getKey = this.props.services.keyStorageManager.getExportKey;
  private getSyncDetails = this.props.services.accountService.getSyncDetails;
  private testPassphrase = this.props.services.passphraseManager.testPassphrase;

  public render() {
    return (
      <ConnectedRouter history={this.props.history}>
        <Switch>
          <Route path={routes.get('ImportScreen')} component={ImportScreen} />
          <Route path={routes.get('UnlockScreen')} component={UnlockScreen} />
          <Route path={routes.get('BackupScreen')} component={this.WrappedBackupScreen} />
          <Route path={routes.get('StartScreen')} component={StartScreen} />
          <LayoutRoute path={routes.get('JournalCreate')} component={JournalCreate} />
          <LayoutRoute path={routes.get('JournalDuplicates')} component={JournalDuplicates} />
          <LayoutRoute path={routes.get('JournalEdit')} component={JournalEdit} />
          <LayoutRoute path={routes.get('JournalList')} component={JournalList} />
          <LayoutRoute path={routes.get('GeneralSettings')} component={this.WrappedSettings} />
          <LayoutRoute path={routes.get('SyncSettings')} component={this.WrappedSyncSettings} />
          <LayoutRoute path={routes.get('Home')} component={JournalList} />
        </Switch>
      </ConnectedRouter>
    );
  }

  private WrappedSyncSettings = () => (
    <SyncSettings getKey={this.getKey} getSyncDetails={this.getSyncDetails} testPassphrase={this.testPassphrase} />
  )

  private WrappedBackupScreen = () => (
    <BackupScreen getKey={this.getKey} getSyncDetails={this.getSyncDetails} />
  )

  private WrappedSettings = () => (
    <GeneralSettings testPassphrase={this.testPassphrase} />
  )
}

export default Routes;
