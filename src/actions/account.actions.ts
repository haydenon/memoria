import { Action, Dispatch } from 'redux';

import { Option, throwIf, throwReturn } from '../prelude';
import { post } from '../util';

import { DEFAULT_PASSPHRASE, getPassphrase, IAccount, IAccountCode, ISync } from '../account';
import { DataService } from '../data/data-service';
import {
  createAction,
  createAsyncActionOrchestration,
  createAsyncActions,
  createAsyncNoArgActionOrchestration,
  createDatalessAction,
  IThunkServices
} from './actions';

interface IAccountDetails {
  account: IAccount | undefined;
  passphraseEnabled: boolean;
}

interface IAccountCreateDetails {
  account: IAccount;
  passphraseEnabled: boolean;
}

// tslint:disable:no-shadowed-variable

const CHECK_ACCOUNT = '[account] check';
const CHECKED_ACCOUNT = '[account] checked';
const PASSPHRASE_REQUIRED = '[account] passphrase required';

export class AccountCheck extends createDatalessAction(CHECK_ACCOUNT) { }

export class AccountChecked extends createAction<IAccountDetails | undefined>(CHECKED_ACCOUNT) { }

export class PassphraseRequired extends createDatalessAction(PASSPHRASE_REQUIRED) { }

export const checkAccount = () => (dispatch: Dispatch<Action>, _getState: any, services: IThunkServices) => {
  dispatch(new AccountCheck());
  services.passphraseManager.getPassphraseInformation().then((required) => {
    if (required) {
      dispatch(new PassphraseRequired());
    } else {
      services.passphraseManager.setPassphrase(undefined).then(() => {
        return services.accountService.getAccount()
          .then((acc) => Option.match(
            (account) => {
              services.dataService.initialise();
              return dispatch(new AccountChecked({ account, passphraseEnabled: false }));
            },
            () => dispatch(new AccountChecked(undefined)),
            acc
          ))
          .catch(() => dispatch(new AccountChecked(undefined)));
      });
    }
  });
};

export const setPassphrase = (passphrase: string, errorCallback: () => void) =>
  (dispatch: Dispatch<Action>, _getState: any, services: IThunkServices) => {
    services.passphraseManager.setPassphrase(passphrase).then((success) => {
      if (success) {
        services.accountService.getAccount()
          .then((acc) => Option.match(
            (account) => {
              services.dataService.initialise();
              return dispatch(new AccountChecked({ account, passphraseEnabled: true }));
            },
            () => dispatch(new AccountChecked(undefined)),
            acc
          ));
      } else {
        errorCallback();
      }
    }).catch((err) => {
      errorCallback();
    });
  };

export const PassphraseChangeActions = createAsyncActions<undefined, boolean>('account', 'change password');

export const changePassphrase = (
  oldPhrase: string,
  newPhrase: string | undefined,
  promiseCallback?: (promise: Promise<boolean>) => void) =>
  createAsyncNoArgActionOrchestration(
    ({ passphraseManager }) =>
      passphraseManager.changePassphrase(oldPhrase, newPhrase)
        .then(() => newPhrase !== undefined),
    PassphraseChangeActions
  )(promiseCallback);

export const AccountCreateActions = createAsyncActions<string | undefined, IAccountCreateDetails>('account', 'create');

export const createAccount = createAsyncActionOrchestration(
  (passphrase, services) =>
    services.keyStorageManager.generateKey(getPassphrase(passphrase)).then(() =>
      services.passphraseManager.addPassphraseInformation(passphrase).then(() =>
        services.dataService.createSettings().then(() =>
          services.accountService.createAccount()
            .then((account) => ({ account, passphraseEnabled: passphrase !== undefined }))))),
  AccountCreateActions
);

export const SyncEnableActions = createAsyncActions<undefined, ISync>('account', 'enable sync');

export const enableSync = createAsyncNoArgActionOrchestration(
  ({ accountService }) => accountService.enableSync(),
  SyncEnableActions
);

export const AccountImportActions = createAsyncActions<IAccountCode, IAccountCreateDetails>('account', 'import');

export const importAccount = createAsyncActionOrchestration(
  (code, services) => services.accountService.importAccount(code)
    .then((account) => services.passphraseManager.addPassphraseInformation(code.passphrase)
      .then(() => ({ account, passphraseEnabled: code.passphrase !== undefined }))),
  AccountImportActions
);

interface IImportData {
  progress?: (item: number, total: number) => void;
}

export const AccountImportDataActions = createAsyncActions<IImportData, void>('account', 'import data');

export const importAccountData = createAsyncActionOrchestration(
  (importValues, { dataService }) => {
    return dataService.importFromRemote(importValues.progress)
      .then(dataService.initialise);
  },
  AccountImportDataActions
);

export const AccountDeleteActions = createAsyncActions<undefined, undefined>('account', 'delete');

export const deleteAccount = createAsyncNoArgActionOrchestration(
  ({ dataService }) => dataService.deleteDatabases().then(() => undefined),
  AccountDeleteActions
);
