import { Action, Dispatch } from 'redux';
import { createAction } from './actions';

import { IBreadcrumb } from '../LayoutRoute';

// tslint:disable:no-shadowed-variable

const BREADCRUMBS_SET = '[breadcrumbs] set';

export class BreadcrumbsSet extends createAction<IBreadcrumb[]>(BREADCRUMBS_SET) { }

const home: IBreadcrumb = {
  label: 'Home',
  link: '/'
};

export const setBreadcrumbs = (breadcrumbs: IBreadcrumb[]) => (dispatch: Dispatch<Action>) => {
  dispatch(new BreadcrumbsSet([home].concat(breadcrumbs)));
};
