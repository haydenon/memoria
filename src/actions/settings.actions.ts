import { createAction, createAsyncActionOrchestration, createAsyncActions, IActionCallback } from './actions';

import { Action, Dispatch } from 'redux';
import { IUpdate } from '../data/data-provider';
import { IDateSettings, IEvaluationSettings, ISettings } from '../settings/settings';

// tslint:disable:no-shadowed-variable

const DATE_LOAD = '[settings] date load';

const EVALUATION_LOAD = '[settings] evaluation load';

export class DateLoad extends createAction<IDateSettings>(DATE_LOAD) { }

export const DateUpdateActions =
  createAsyncActions<IUpdate<IDateSettings>, void>('settings', 'date update');

export const updateDate = createAsyncActionOrchestration(
  (data, { dataService }) => dataService.updateSettings('date', data),
  DateUpdateActions
);

export class EvaluationLoad extends createAction<IEvaluationSettings>(EVALUATION_LOAD) { }

export const EvaluationUpdateActions =
  createAsyncActions<IUpdate<IEvaluationSettings>, void>('account', 'evaluation update');

export const updateEvaluation = createAsyncActionOrchestration(
  (data, { dataService }) => dataService.updateSettings('evaluation', data),
  EvaluationUpdateActions
);
