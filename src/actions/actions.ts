import { Action, Dispatch } from 'redux';
import { AccountService } from '../data/account-service';
import { DataService } from '../data/data-service';
import { KeyStorageManager } from '../encryption/key-storage';
import PassphraseManager from '../encryption/passphrase-manager';
import { IState } from '../reducer';

// tslint:disable:interface-name

export interface DatalessActionConstructor {
  new(): IAction<undefined>;
  actionType(): string;
}

export interface ActionConstructor<T> {
  new(data: T): IAction<T>;
  actionType(): string;
}

export interface IActionCallback<TData> {
  success?: (data: TData) => void;
  error?: (error: string) => void;
}

interface IAction<T> extends Action {
  data: T;
}

export interface IThunkServices {
  dataService: DataService;
  keyStorageManager: KeyStorageManager;
  passphraseManager: PassphraseManager;
  accountService: AccountService;
}

export const isOfType = <T extends IAction<TData>, TData>(
  action: Action,
  type: new (data: TData) => T
): action is T => {
  return (type as any as ActionConstructor<TData>).actionType() === action.type;
};

export const isOfDatalessType = <T extends IAction<undefined>>(
  action: Action,
  type: new () => T
): action is T => {
  return (type as any as ActionConstructor<undefined>).actionType() === action.type;
};

export const createAction = <T>(type: string): ActionConstructor<T> => {
  return class NewAction implements IAction<T> {
    public static actionType = () => type;

    public type = type;

    constructor(public data: T) { }
  };
};

export const createDatalessAction = (type: string): DatalessActionConstructor => {
  return class NewAction extends createAction<undefined>(type) {
    public static actionType = () => type;

    public type = type;

    constructor() {
      super(undefined);
    }
  };
};

export interface IAsyncActions<T, R> {
  Start: ActionConstructor<T>;
  Success: ActionConstructor<R>;
  Error: ActionConstructor<string>;
}

export const createAsyncActions = <T, R>(resource: string, action: string): IAsyncActions<T, R> => {
  const Start = createAction<T>(`[${resource}] ${action}`);
  const Success = createAction<R>(`[${resource}] ${action} succeeded`);
  const Error = createAction<string>(`[${resource}] ${action} failed`);
  return { Start, Success, Error };
};

export const createAsyncActionOrchestration = <T, R>(
  action: (data: T, services: IThunkServices, getState: () => IState) => Promise<R>,
  { Start, Success, Error }: IAsyncActions<T, R>) =>
  (data: T, promiseCallback?: (promise: Promise<R>) => void) => {
    return (dispatch: Dispatch<Action>, getState: () => IState, services: IThunkServices) => {
      dispatch(new Start(data));
      action(data, services, getState)
        .then((res) => {
          dispatch(new Success(res));
          if (promiseCallback)
            promiseCallback(Promise.resolve(res));
        })
        .catch((err) => {
          dispatch(new Error(err.message));
          if (promiseCallback)
            promiseCallback(Promise.reject(err));
        });
    };
  };

export const createAsyncNoArgActionOrchestration = <R>(
  action: (services: IThunkServices) => Promise<R>,
  actions: IAsyncActions<undefined, R>) =>
  (promiseCallback?: (promise: Promise<R>) => void) => {
    return createAsyncActionOrchestration(
      (_, services) => action(services),
      actions
    )(undefined, promiseCallback);
  };

export const makeCancelToken = () => {
  let cancelled = false;
  return {
    callback: () => {
      cancelled = true;
    },
    getState: () => {
      return cancelled;
    }
  };
};
