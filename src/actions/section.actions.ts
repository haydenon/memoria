import { createAction } from './actions';

import { ISection } from '../journal';

// tslint:disable:no-shadowed-variable

const SECTION_LOAD = '[section] load';

export class SectionLoad extends createAction<ISection[]>(SECTION_LOAD) { }
