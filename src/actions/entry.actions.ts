import { Action, Dispatch } from 'redux';
import {
  createAction,
  createAsyncActionOrchestration,
  createAsyncActions,
  IActionCallback,
  IThunkServices,
  makeCancelToken
} from './actions';

import { DataService } from '../data/data-service';

import { IEntry, Journal } from '../journal';
import { dateToString } from '../util';

// tslint:disable:no-shadowed-variable

const ENTRY_LOAD = '[entry] load';

const JOURNAL_CREATE = '[journal] create';
const JOURNAL_CREATED = '[journal] created';
const JOURNAL_CREATE_FAILED = '[journal] create failed';

const JOURNAL_UPDATE = '[journal] update';
const JOURNAL_UPDATED = '[journal] updated';
const JOURNAL_UPDATE_FAILED = '[journal] update failed';

const JOURNAL_DELETE = '[journal] delete';
const JOURNAL_DELETED = '[journal] deleted';
const JOURNAL_DELETE_FAILED = '[journal] delete failed';

export class EntryLoad extends createAction<IEntry[]>(ENTRY_LOAD) { }

export class JournalCreate extends createAction<Journal>(JOURNAL_CREATE) { }

export class JournalCreated extends createAction<Journal>(JOURNAL_CREATED) { }

export class JournalCreateFailed extends createAction<string>(JOURNAL_CREATE_FAILED) { }

export const createJournal = (journal: Journal, callbacks?: IActionCallback<Journal>) =>
  (dispatch: Dispatch<Action>, _getState: any, { dataService }: IThunkServices) => {
    dispatch(new JournalCreate(journal));

    dataService.createJournal(journal)
      .then(() => {
        dispatch(new JournalCreated(journal));
        if (callbacks && callbacks.success)
          callbacks.success(journal);
      })
      .catch((err) => {
        dispatch(new JournalCreateFailed(err.message));
        if (callbacks && callbacks.error)
          callbacks.error(err.message);
      });
  };

export class JournalUpdate extends createAction<Journal>(JOURNAL_UPDATE) { }

export class JournalUpdated extends createAction<Journal>(JOURNAL_UPDATED) { }

export class JournalUpdateFailed extends createAction<string>(JOURNAL_UPDATE_FAILED) { }

export const updateJournal = (journal: Journal, callbacks?: IActionCallback<Journal>) =>
  (dispatch: Dispatch<Action>, _getState: any, { dataService }: IThunkServices) => {
    dispatch(new JournalUpdate(journal));

    dataService.updateJournal(journal)
      .then(() => {
        dispatch(new JournalUpdated(journal));
        if (callbacks && callbacks.success)
          callbacks.success(journal);
      })
      .catch((err) => {
        dispatch(new JournalUpdateFailed(err.message));
        if (callbacks && callbacks.error)
          callbacks.error(err.message);
      });
  };

export class JournalDelete extends createAction<IEntry>(JOURNAL_DELETE) { }

export class JournalDeleted extends createAction<IEntry>(JOURNAL_DELETED) { }

export class JournalDeleteFailed extends createAction<string>(JOURNAL_DELETE_FAILED) { }

export const deleteJournal = (entry: IEntry, callbacks?: IActionCallback<IEntry>) =>
  (dispatch: Dispatch<Action>, _getState: any, { dataService }: IThunkServices) => {
    dispatch(new JournalDelete(entry));

    dataService.deleteJournal(entry)
      .then(() => {
        dispatch(new JournalDeleted(entry));
        if (callbacks && callbacks.success)
          callbacks.success(entry);
      })
      .catch((err) => {
        dispatch(new JournalDeleteFailed(err.message));
        if (callbacks && callbacks.error)
          callbacks.error(err.message);
      });
  };

interface IJournalImport {
  journals: Journal[];
  progressListener: (item: number, total: number) => void;
  getCancelCallback?: (callback: () => void) => void;
}

export const JournalImportActions = createAsyncActions<IJournalImport, void>('journal', 'import');

export const importJournals = createAsyncActionOrchestration(
  async (importValues, { dataService }, getState) => {
    const state = getState();
    const existingDates = state.entry.entries.map((e) => e.date);
    const journals = importValues.journals.filter((j) => existingDates.indexOf(j[0].date) === -1);
    const cancelToken = makeCancelToken();

    if (importValues.getCancelCallback)
      importValues.getCancelCallback(cancelToken.callback);

    return dataService.importJournals(journals, importValues.progressListener, cancelToken.getState);
  },
  JournalImportActions
);
