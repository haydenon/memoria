import React from 'react';

import { Divider } from 'antd';

export default class extends React.Component<{}> {
  public render() {
    return (
      <div className="journal-loading">
        <div className="toolbar">
          <div className="loading journal-loading-block" />
          <div className="loading journal-loading-block" />
          <Divider type="vertical" className="toolbar-divider" />
          <div className="loading journal-loading-block" />
          <div className="loading journal-loading-block" />
          <Divider type="vertical" className="toolbar-divider" />
          <div className="loading journal-loading-block" />
        </div>
        <div className="editor">
          <div className="date journal-loading-block" />
          <div className="break" />
          <div className="heading journal-loading-block" />
          <div className="line-1 journal-loading-block" />
          <div className="line-2 journal-loading-block" />
          <div className="line-1 journal-loading-block" />
          <div className="line-3 journal-loading-block" />
          <div className="break" />
          <div className="heading journal-loading-block" />
          <div className="attribute journal-loading-block" />
          <div className="line-1 journal-loading-block" />
          <div className="line-2 journal-loading-block" />
          <div className="line-4 journal-loading-block" />
          <div className="break" />
          <div className="line-1 journal-loading-block" />
          <div className="line-4 journal-loading-block" />
          <div className="line-3 journal-loading-block" />
          <div className="break" />
          <div className="heading journal-loading-block" />
          <div className="attribute journal-loading-block" />
          <div className="line-1 journal-loading-block" />
          <div className="line-4 journal-loading-block" />
          <div className="line-2 journal-loading-block" />
          <div className="line-3 journal-loading-block" />
        </div>
      </div>
    );
  }
}
