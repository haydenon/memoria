import moment from 'moment';
import { Delta, DeltaStatic } from 'quill';
import * as React from 'react';
import { Entry, Journal, Section } from '../journal';
import { error, Option, Result } from '../prelude';
import { ShortDateFormatIds } from '../settings/date-formats';
import { DATE_DEFAULT, EVALUATION_DEFAULT, IEvaluationSettings, ISettings } from '../settings/settings';
import { EditorJournal, IJournalRow, parseDeltas, parseJournal, RowType } from './parser';

// tslint:disable:no-unused-expression

const settings: ISettings = {
  evaluation: EVALUATION_DEFAULT,
  date: DATE_DEFAULT,
};

const validJournal = ([
  { insert: '2018/03/04' },
  { attributes: { header: 1 }, insert: '\n' },
  { insert: '\nFeeling' },
  { attributes: { header: 2 }, insert: '\n' },
  { attributes: { background: '#cce8cc', italic: true }, insert: 'Good' },
  { insert: '\nEnter text here...\n' },
] as any) as DeltaStatic;

describe('parser', () => {
  const date = moment('2018/03/04', settings.date.dateFormat);
  const journal: Journal = [
    Entry(
      '',
      date.format(settings.date.dataDateFormat),
      ShortDateFormatIds[settings.date.dateFormat],
      validJournal,
    ),
    [Section(
      '',
      date.format(settings.date.dataDateFormat),
      'Feeling', 'Enter text here...',
      { label: 'Good', score: 1, max: 1 }
    )]
  ];
  it('should parse correctly', () => {
    const parsed = parseJournal(settings)(validJournal);
    chai.expect(Result.value(parsed)).to.eql(journal);
  });
});

describe('parsing deltas', () => {
  const expected: EditorJournal = [
    { text: '2018/03/04', type: 'Header' },
    { text: '', type: 'Break' },
    { text: 'Feeling', type: 'Header' },
    { text: 'Good', type: 'Attribute' },
    { text: 'Enter text here...', type: 'Text' },
    { text: '', type: 'EOF' },
  ];

  const attributes = settings.evaluation.attributes.map((a) => a.phrase.toLowerCase());

  it('should output correct row items', () => {
    const parsed = parseDeltas(validJournal, attributes);
    chai.expect(parsed.hasValue).to.be.true;
    chai.expect(parsed.value).to.deep.equal(expected);
  });
});
