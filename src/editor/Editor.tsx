import * as React from 'react';
import { connect } from 'react-redux';

import { eql, noop, Option, updateArray } from '../prelude';

import {
  Affix,
  Button,
  Col,
  Divider,
  Dropdown,
  Form,
  Icon,
  Input,
  Menu,
  Row,
  Switch,
} from 'antd';
import { FormComponentProps } from 'antd/lib/form/Form';
import moment, { Moment } from 'moment';
import Quill, { DeltaOperation, RangeStatic, Sources } from 'quill';
import Delta from 'quill-delta';
import ReactQuill from 'react-quill';
import { formSubmit } from '../util';
import AttributeHighlighter from './attribute-highlighter';
import {
  Block,
  Blot,
  blotName,
  Container,
  hasNext,
  hasPrev,
  isParent,
  Text
} from './editor-util';

import './editor.less';

import { Dispatch } from 'redux';
import { BreadcrumbsSet, setBreadcrumbs } from '../actions/breadcrumbs.actions';
import PopupDatepicker from '../components/PopupDatepicker';
import { IState } from '../reducer';
import { IEntryState } from '../reducer/entry.reducer';
import { ISettingsState } from '../reducer/settings.reducer';
import { ShortDateFormat, ShortDateFormatIds, shortDateFromId } from '../settings/date-formats';
import { settings } from '../state-helpers';

const { Item: MenuItem, Divider: MenuDivider, ItemGroup: MenuGroup } = Menu;
const { Item: FormItem } = Form;

export interface ISectionDefinition {
  name: string;
  attribute: boolean;
}

const replaceDate = (delta: Delta, prev: ShortDateFormat, format: ShortDateFormat): Delta => {
  if (delta.ops === undefined || !delta.ops.length)
    return delta;

  const toPattern = (form: ShortDateFormat) =>
    new RegExp(form.split(/[\/\-\.]/).map((str) => `[0-9]{${str.length}}`).join('[\\/\\-\\.]'));

  const firstLine = delta.ops[0].insert as string;
  const prevPattern = toPattern(prev);
  const matchDate = prevPattern.exec(delta.ops[0].insert);
  if (matchDate === null)
    return delta;

  const date = moment(matchDate[0], prev);

  return new Delta(updateArray(
    delta.ops,
    (_, ind) => ind === 0,
    { insert: firstLine.replace(prevPattern, date.format(format)) }
  ));
};

const transformDeltaDate = (delta: Delta, prev: ShortDateFormat, dateFormat: ShortDateFormat): Delta => {
  if (prev === dateFormat)
    return delta;

  return replaceDate(delta, prev, dateFormat);
};

function getInsertIndex(quill: Quill, selection: RangeStatic)
  : { position: number, next: boolean, prev: boolean } {
  const ret = (num: number, next = false, prev = false) => ({ position: num, next, prev });
  const cursor = selection && selection.index || 0;

  const container: Container = quill.getLine(cursor)[0];
  if (!container.children) {
    return ret(quill.getLength());
  }

  let blot: Blot = container;
  while (isParent(blot)
    && blot.children
    && blotName(blot.children.head) !== 'break'
    && blot.next) {
    blot = blot.next;
  }

  if (isParent(blot)
    && blot.children
    && blotName(blot.children.head) === 'break') {
    return ret(quill.getIndex(blot), hasNext(blot), hasPrev(blot));
  }

  return ret(quill.getLength(), hasNext(blot), hasPrev(blot));
}

function trigger(action: (quill: Quill) => void) {
  return function (this: { quill: Quill }) {
    action(this.quill);
  };
}

interface IEditorState {
  quill?: Quill;
  editorValue?: Delta;
  datePickerOpen: boolean;
  datePickerOpenedAt: number;
  datePickerCallback: (moment: Moment) => void;
  sectionMenuVisible: boolean;
  sectionMenuCallback: (definition: ISectionDefinition) => void;
}

interface IProps {
  dispatch: Dispatch<IState>;
  value?: Delta;
  dateFormat?: ShortDateFormat;
  defaultDate?: Moment;
  onDateChange?: (date: Moment) => void;
  onChange: (deltas: Delta) => void;
  entry: IEntryState;
  settings: ISettingsState;
  disabledDate: (date: Moment) => boolean;
  recommendedSections: ISectionDefinition[];
}

interface ISectionForm extends FormComponentProps {
  onSubmit: (def: ISectionDefinition) => void;
}

class NewSectionForm extends React.Component<ISectionForm> {
  private handleSubmit = formSubmit(this.props.form, (def: ISectionDefinition) => {
    this.props.form.setFieldsValue({
      name: '',
    });
    this.props.onSubmit(def);
  });

  public render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} layout="horizontal" style={{ width: 315 }}>
        <FormItem
          label="Name"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
        >
          {getFieldDecorator('name', {
            rules: [{ required: true, message: 'Please enter a name' }]
          })(
            <Input placeholder="Name" />
          )}
        </FormItem>
        <FormItem
          label="Evaluation"
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
        >
          {getFieldDecorator('attribute')(
            <Switch />
          )}
        </FormItem>
        <FormItem>
          <Row>
            <Col offset={6} span={18}>
              <Button htmlType="submit" icon="plus" type="primary">Add</Button>
            </Col>
          </Row>
        </FormItem>
      </Form >
    );
  }
}

const WrappedSectionForm = Form.create()(NewSectionForm);

function isText(blot: Blot): blot is Text {
  return blot && 'value' in blot && (blot as any).value instanceof Function;
}

Quill.register('modules/attributehighlighter', AttributeHighlighter);

const CHANGE_DEBOUNCE = 150;

class EditorComponent extends React.Component<IProps, IEditorState> {
  private static initialState: IEditorState = {
    editorValue: undefined,
    datePickerCallback: noop,
    datePickerOpen: false,
    datePickerOpenedAt: moment.now(),
    sectionMenuCallback: noop,
    sectionMenuVisible: false,
  };

  private changeTimer: number | undefined;

  private sectionNameInput: Input | null = null;

  private modules = {
    ['attributehighlighter']: {
      ['attributes']: settings(this.props).evaluation.attributes,
      ['scoreToColour']: settings(this.props).colorForScore,
    },
    ['toolbar']: {
      ['container']: '#editor-toolbar',
      ['handlers']: {
        ['insertDate']: trigger(this.insertDate.bind(this)),
        ['insertSection']: trigger(this.insertSection.bind(this)),
      }
    }
  };

  constructor(props: any) {
    super(props);
    this.state = EditorComponent.initialState;

    this.insertDate = this.insertDate.bind(this);
    this.insertSection = this.insertSection.bind(this);
  }

  public componentDidMount() {
    this.props.onChange(this.initialValue());
  }

  public render() {
    const CustomToolbar = this.customToolbar;
    return (
      <div className="journal-container">
        <CustomToolbar />
        <ReactQuill
          ref={this.handleEditorRef}
          theme="snow"
          value={this.state.editorValue || this.initialValue()}
          onChange={this.onChange}
          modules={this.modules}
          className="journal-editor"
        />
      </div>
    );
  }

  public componentDidUpdate(prevProps: IProps) {
    if (prevProps.value !== this.props.value) {
      const value = this.props.value !== undefined
        ? this.transformValue(this.props.value)
        : undefined;
      this.setState({ editorValue: value });
    }
  }

  private onChange = (change: string, delta: Delta) => {
    if (this.changeTimer !== undefined) {
      window.clearTimeout(this.changeTimer);
    }

    this.changeTimer = window.setTimeout(() => {
      if (!this.state.quill)
        return;

      this.setState({ editorValue: this.state.quill.getContents() });
      this.props.onChange(this.state.quill.getContents());
    }, CHANGE_DEBOUNCE);
  }

  private handleEditorRef = (editor: ReactQuill | null) => {
    if (editor && !this.state.quill) {
      this.setState({ quill: editor.getEditor() });
    }
  }

  private transformValue = (value: Delta) =>
    this.props.dateFormat === undefined
      ? value
      : transformDeltaDate(
        value,
        this.props.dateFormat,
        settings(this.props).date.dateFormat
      )

  private initialValue = (): Delta => {
    if (this.props.value) {
      return this.transformValue(this.props.value);
    }

    const ops: DeltaOperation[] = [
      { insert: (this.props.defaultDate || moment()).format(settings(this.props).date.dateFormat) },
      {
        attributes: { header: 1 }, insert: '\n'
      },
      { insert: '\n' },
    ];

    // tslint:disable-next-line:no-object-literal-type-assertion
    return {
      ops,
    } as Delta;
  }

  private insertDate(quill: Quill) {
    const callback = (date: Moment) => {
      this.setState({ datePickerCallback: noop, datePickerOpen: false });
      const firstLine = quill.getLine(0);
      const firstLineText = firstLine[0].children.head.text;
      if (/^[0-9]{4}\/[0-9]{2}\/[0-9]{2}$/.test(firstLineText)) {
        quill.deleteText(0, 11);
      }
      quill.insertText(0, `${date.format(settings(this.props).date.dateFormat)}\n`);
      quill.formatLine(1, 10, 'header', 1);
      quill.setSelection(11, 0);

      if (this.props.onDateChange)
        this.props.onDateChange(date);
    };
    this.setState({ datePickerOpen: true, datePickerOpenedAt: moment.now(), datePickerCallback: callback });
  }

  private insertSection(quill: Quill) {
    const selection = quill.getSelection();

    const sectionCallback = (def: ISectionDefinition) => {
      const NEW_SECTION = def.name;
      const SECTION_TEXT = 'Enter text here...';

      const { position, next, prev } = getInsertIndex(quill, selection);
      let cursorPosition = position;

      if (prev) {
        quill.insertText(cursorPosition, '\n');
        cursorPosition += 1;
      }

      quill.insertText(cursorPosition, `${NEW_SECTION}\n`);
      quill.formatLine(cursorPosition + 1, 0, 'header', 2);
      cursorPosition += NEW_SECTION.length + 1;

      const attributes = settings(this.props).evaluation.attributes;
      if (def.attribute && attributes.length) {
        const attr = attributes[0];
        const ATTRIBUTE = attr.phrase;
        const background = settings(this.props).colorForScore(attr.value);

        quill.insertText(cursorPosition, `${ATTRIBUTE}\n`);
        quill.setSelection(cursorPosition, ATTRIBUTE.length);
        quill.format('italic', true);
        quill.format('background', background);
        cursorPosition += ATTRIBUTE.length + 1;
      }

      quill.insertText(cursorPosition, `${SECTION_TEXT}`);
      cursorPosition += SECTION_TEXT.length;
      if (next)
        quill.insertText(cursorPosition, '\n');
      quill.setSelection(cursorPosition, 0);
    };

    this.setState({
      sectionMenuCallback: sectionCallback
    });
  }

  private datePickerOpenChange = (status: boolean) => {
    this.setState({ datePickerOpen: status, datePickerCallback: noop });
  }

  private handleSectionVisibleChange = (open: boolean | undefined) => {
    const state: any = { sectionMenuVisible: !!open };
    if (!open) {
      state.sectionMenuCallback = noop;
    }

    this.setState(state);
  }

  private handleNewSection = (def: ISectionDefinition) => {
    if (this.state.sectionMenuCallback)
      this.state.sectionMenuCallback(def);
  }

  private handleSectionMenuClick = (e: any) => {
    if (e.key && e.key !== 'custom') {
      const key = +e.key;
      this.state.sectionMenuCallback(this.props.recommendedSections[key]);
      this.setState({ sectionMenuVisible: false });
    }
  }

  private sectionMenu = () => (
    <Menu onClick={this.handleSectionMenuClick}>
      <MenuItem className="custom-section-name" key="custom">
        <WrappedSectionForm onSubmit={this.handleNewSection} />
      </MenuItem>
      <MenuDivider />
      <MenuGroup className="previous-section-title" title="Previous">
        {this.props.recommendedSections.map((item, ind) => <MenuItem key={ind}>{item.name}</MenuItem>)}
        {!this.props.recommendedSections.length
          ? <MenuItem disabled={true}>No other previous sections</MenuItem>
          : null
        }
      </MenuGroup>
    </Menu>
  )

  private customToolbar = () => (
    <div id="editor-toolbar">
      <button className="ql-insertDate custom-icon">
        <PopupDatepicker
          open={this.state.datePickerOpen}
          onChange={this.state.datePickerCallback}
          onOpenChange={this.datePickerOpenChange}
          disabledDate={this.props.disabledDate}
          defaultValue={this.props.defaultDate}
        />
        <Icon type="calendar" />
      </button>
      <Dropdown
        trigger={['click']}
        overlay={this.sectionMenu()}
        visible={this.state.sectionMenuVisible}
        onVisibleChange={this.handleSectionVisibleChange}
      >
        <button className="custom-icon ql-insertSection"><Icon type="plus-circle" /></button>
      </Dropdown>
      <Divider type="vertical" className="toolbar-divider" />
      <button className="ql-bold" />
      <button className="ql-italic" />
      <Divider type="vertical" className="toolbar-divider" />
      <select className="ql-background" defaultValue="">
        <option value={''} />
        <option value="#66b966" />
        <option value="#cce8cc" />
        <option value="#ffffcc" />
        <option value="#facccc" />
        <option value="#f06666" />
      </select>
    </div>
  )
}

const mapStateToProps = (state: IState) => ({
  entry: state.entry,
  settings: state.settings,
});

export default connect(mapStateToProps)(EditorComponent);
