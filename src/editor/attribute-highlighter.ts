import Quill, { DeltaStatic, Sources } from 'quill';
import { present } from '../prelude';
import { IAttribute } from '../settings/settings';
import { Blot, blotName } from './editor-util';

export interface IHighlighterOptions {
  attributes: IAttribute[];
  scoreToColour: (score: number) => string;
}

const normalise = (str: string) => str.toLowerCase().trim();

export default class AttributeHighlighter {
  private attributes: IAttribute[];
  private scoreToColour: (score: number) => string;
  constructor(private quill: Quill, private options: IHighlighterOptions) {
    quill.on('text-change', this.highlightAttribute);
    this.attributes = options.attributes;
    this.scoreToColour = options.scoreToColour;
  }

  private highlightAttribute = (_del: DeltaStatic, _old: DeltaStatic, source: Sources) => {
    if (source !== 'user')
      return;

    let contents = this.quill.getLines();

    if (_del.ops && _del.ops[0].retain) {
      const retain = _del.ops[0].retain || 0;
      let currentLength = 0;
      let i = 0;
      for (; i < contents.length && currentLength <= retain; i++) {
        currentLength += contents[i].length();
      }
      // i is offset by 1: line 0 -> i = 1
      const lower = i > 1 ? i - 2 : i - 1;
      const higher = i >= contents.length ? contents.length : i + 1;
      contents = contents.slice(lower, higher);
    }

    contents.forEach((item: Blot, ind: number) => {
      if (blotName(item) === 'header'
        && ind + 1 < contents.length
        && blotName(contents[ind + 1]) === 'block') {
        const next = contents[ind + 1];
        const length = next.length();
        const index = this.quill.getIndex(next);
        const text = this.quill.getText(index, length);
        if (this.hasAttribute(text)) {
          const score = this.attributeScore(text);
          this.quill.removeFormat(index, length);
          this.quill.formatText(index, length, 'italic', true);
          this.quill.formatText(index, length, 'background', this.scoreToColour(score));
        } else {
          this.quill.formatText(index, length, 'background', '');
        }
      }
    });
  }

  private hasAttribute = (attr: string) =>
    !!this.attributes.filter((a) => normalise(a.phrase) === normalise(attr)).length

  private attributeScore = (attr: string) =>
    present(this.attributes.find((a) => normalise(a.phrase) === normalise(attr))).value
}
