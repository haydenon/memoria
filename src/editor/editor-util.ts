import { Blot } from 'parchment/dist/src/blot/abstract/blot';

// tslint:disable:interface-name

type BlotType = 'header' | 'break' | 'block';

const blotName = (blot: Blot): BlotType => {
  return (blot as any).statics.blotName;
};

const isParent = (blot: Blot): blot is Container => {
  return blot && 'children' in blot;
};

const hasPrev = (blot: Blot): boolean => {
  return !!(blot
    && blot.prev
    && blotName(blot.prev) !== 'break');
};

const hasNext = (blot: Blot): boolean => {
  return !!(blot
    && blot.next
    && blotName(blot.next) !== 'break');
};

interface Container extends Blot {
  children: {
    head: Blot
  };
}

interface Block extends Container {
  formats(): {
    [index: string]: any;
  };
}

interface Text extends Blot {
  value: () => string;
}

export {
  Blot,
  blotName,
  isParent,
  hasNext,
  hasPrev,
  BlotType,
  Block,
  Container,
  Text,
};
