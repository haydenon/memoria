import { capitalise, groupBy, identity, Option, rest, Result } from '../prelude';

import moment, { Moment } from 'moment';
import Quill, { DeltaOperation, DeltaStatic } from 'quill';
import { Entry, IEntry, ISection, ISectionQuality, Journal, Section } from '../journal';
import { ShortDateFormatId, ShortDateFormatIds } from '../settings/date-formats';
import { IEvaluationSettings, ISettings } from '../settings/settings';

export type RowType = 'Header' | 'Attribute' | 'Break' | 'Text' | 'EOF';
const EOF: RowType = 'EOF';

export interface IJournalRow {
  text: string;
  type: RowType;
}

export type EditorJournal = IJournalRow[];

const deltaText = (delta: DeltaOperation): Option<string> => {
  if (!delta.insert && typeof (delta.insert) === 'string')
    return Option.some(delta.insert.trim());
  else
    return Option.none();
};

const getMoment = (settings: ISettings) => (str: string): Result<[Moment, ShortDateFormatId], string> => {
  const format = settings.date.dateFormat;
  const formatId: ShortDateFormatId = ShortDateFormatIds[format];
  const mom = moment(str, format);
  return mom.isValid()
    ? Result.success([mom, formatId] as [Moment, ShortDateFormatId])
    : Result.error('The date specified isn\'t a valid date');
};

const getDate = (journal: EditorJournal, settings: ISettings): Result<[Moment, ShortDateFormatId], string> => {
  if (journal.length < 1)
    return Result.error('No date specified in journal. Please put the date in the first line');

  const text = journal[0].text;
  const prefixYear = /[0-9]{4}[\/\-\.][0-9]{2}[\/\-\.][0-9]{2}/;
  const suffixYear = /[0-9]{2}[\/\-\.][0-9]{2}[\/\-\.][0-9]{4}/;
  const prefixMatch = prefixYear.exec(text);
  if (prefixMatch) {
    return getMoment(settings)(prefixMatch[0]);
  }

  const suffixMatch = suffixYear.exec(text);
  if (suffixMatch) {
    return getMoment(settings)(suffixMatch[0]);
  }

  return Result.error('No date specified in journal. Please put the date in the first line');
};

const getEntry = (journal: EditorJournal, document: DeltaStatic, settings: ISettings) =>
  (dateAndFormat: [Moment, ShortDateFormatId]): Result<Journal, string> => {
    const [date, dateFormat] = dateAndFormat;
    const entries = rest(journal);
    let title = '';
    let attribute: ISectionQuality | undefined;
    let text = '';
    const sections: ISection[] = [];

    const pushSection = () => {
      if (title && (text || attribute)) {
        sections.push(Section('', date.format(settings.date.dataDateFormat), title, text.trim(), attribute));
        title = '';
        text = '';
        attribute = undefined;
      }
    };

    entries.forEach((line, ind) => {
      if (line.type === EOF) {
        pushSection();
      } else if (line.type === 'Header') {
        pushSection();
        title = line.text;
      } else if (line.type === 'Break' && text.length) {
        text += '\n';
      } else if (line.type === 'Attribute') {
        const attr = settings.evaluation.attributes
          .find((a) => line.text.trim().toLowerCase() === a.phrase.toLowerCase());
        if (!attr) throw new Error('Attribute must exist');
        const score = attr.value;
        const max = settings.evaluation.maxScore;
        attribute = {
          label: line.text.trim(),
          score,
          max,
        };
      } else if (line.type === 'Text') {
        text += line.text;
      }
    });

    const duplicates = groupBy(sections, (s) => s.name.toLowerCase())
      .filter((c) => c.count > 1)
      .map((c) => c.property);

    if (duplicates.length) {
      const sectionDesc = duplicates.length > 1 ? 'duplicate sections' : 'a duplicate section';
      const message = `Journal entry contains ${sectionDesc} for ${duplicates.map(capitalise).join(', ')}`;
      return Result.error(message);
    }

    const tuple: Journal = [Entry('', date.format(settings.date.dataDateFormat), dateFormat, document), sections];
    return Result.success(tuple);
  };

export const parseDeltas = (deltas: DeltaStatic, attributes: string[]): Option<EditorJournal> => {
  const operations = deltas.map ? deltas.map(identity) : deltas.ops;
  if (operations === undefined || !operations.length)
    return Option.some([]);

  const getType = (op: DeltaOperation, text: string, prevHeader: boolean): RowType => {
    if (op.attributes && op.attributes.header > 0)
      return 'Header';
    else if (prevHeader && attributes.indexOf(text.toLowerCase()) !== -1)
      return 'Attribute';
    else
      return 'Text';
  };

  let currentText = '';
  const journalRows: EditorJournal = [];
  operations.forEach((op, index) => {
    if (op.insert && typeof (op.insert) === 'string') {
      const rows = op.insert.split(/(\n)/).filter((t) => t);
      rows.forEach((text, ind) => {
        if (text === '\n') {
          if (currentText === '') {
            journalRows.push({ text: '', type: 'Break' });
          } else {
            const previousHeader = journalRows.length > 0
              && journalRows[journalRows.length - 1].type === 'Header';
            journalRows.push({ text: currentText, type: getType(op, currentText, previousHeader) });
          }

          currentText = '';
        } else {
          currentText += text;
        }
      });
    }
  });

  return Option.some([...journalRows, { text: '', type: EOF }]);
};

export const parseJournal = (settings: ISettings) => (document: DeltaStatic): Result<Journal, string> => {
  const attributes = settings.evaluation.attributes.map((a) => a.phrase.toLowerCase());
  const journal = parseDeltas(document, attributes).value;

  const entry = getDate(journal, settings)
    .bind(getEntry(journal, document, settings));

  return entry;
};
