import moment, { Moment } from 'moment';

import { ISettingsState } from '../reducer/settings.reducer';
import { settings } from './settings';

export const dateStr = (m: Moment) =>
  m !== undefined ? m.format('YYYY-MM-DD') : '0000-00-00';

const hasSettings = (props: any): props is { settings: ISettingsState } => props.settings;

export const loading = <T>(props: T, load: (t: T) => boolean) => {
  if (hasSettings(props)) {
    return load(props) || settings(props).loading;
  }

  return load(props);
};
