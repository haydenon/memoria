import moment, { Moment } from 'moment';

import { IEntry } from '../journal';
import { groupBy, Option } from '../prelude';
import { IAsyncAction } from '../reducer/async-action';
import { IEntryState } from '../reducer/entry.reducer';
import { dateStr, loading } from './common';

interface IEntryProps {
  entry: IEntryState;
}

const entryExistsOnDate = <T extends IEntryProps>(props: T) => (date: Moment) =>
  props.entry.entries.find((e) => e.date === dateStr(date)) !== undefined;

const nextValidDate = <T extends IEntryProps>(props: T) => (date: Moment) => {
  const currDate = moment(date).startOf('day');
  while (entryExistsOnDate(props)(currDate)) {
    currDate.add(1, 'day');
  }

  return currDate;
};

export interface IEntryDuplicate {
  date: string;
  items: IEntry[];
}

export interface IEntryHelper {
  items: IEntry[];
  nonDuplicates: IEntry[];
  create: IAsyncAction;
  delete: IAsyncAction;
  duplicates: IEntryDuplicate[];
  isDuplicate: (date: Moment, excludeId?: string) => boolean;
  byId: (id: string) => Option<IEntry>;
  byDate: (date: Moment) => Option<IEntry>;
  previousEntry: (date: Moment) => Option<IEntry>;
  loading: boolean;
  deleting: boolean;
  existsOnDate: (date: Moment) => boolean;
  nextValidDate: (from: Moment) => Moment;
}

const duplicates = (props: IEntryProps): IEntryDuplicate[] =>
  groupBy(props.entry.entries, (e) => e.date)
    .filter((group) => group.count > 1)
    .map((group) => ({ date: group.items[0].date, items: group.items }));

const nonDuplicates = (props: IEntryProps): IEntry[] =>
  groupBy(props.entry.entries, (e) => e.date)
    .filter((group) => group.count === 1)
    .map((group) => group.items[0]);

const isDuplicate = (props: IEntryProps) => (date: Moment, excludeId?: string): boolean =>
  props.entry.entries.filter((e) =>
    (excludeId === undefined || e.id !== excludeId)
    && e.date === dateStr(date)).length >= 1;

export const entries = <T extends IEntryProps>(props: T): IEntryHelper => ({
  get items() { return props.entry.entries; },
  get nonDuplicates() { return nonDuplicates(props); },
  get create() { return props.entry.create; },
  get delete() { return props.entry.delete; },
  get duplicates() { return duplicates(props); },
  isDuplicate: isDuplicate(props),
  byId: (id: string): Option<IEntry> => {
    const entry = props.entry.entries.find((e) => e.id === id);
    return entry !== undefined ? Option.some(entry) : Option.none();
  },
  byDate: (date: Moment): Option<IEntry> => {
    const entry = props.entry.entries.find((e) => e.date === dateStr(date));
    return entry !== undefined ? Option.some(entry) : Option.none();
  },
  previousEntry: (date: Moment): Option<IEntry> => {
    const prevDate = moment(date).startOf('day').subtract(1, 'day');
    const validEntries = props.entry.entries.filter((entry) => prevDate.diff(entry.date) >= 0);
    return validEntries.length
      ? Option.some(validEntries[0]) // Assumes descending order on entries
      : Option.none();
  },
  get loading() { return loading(props, (p) => p.entry.loading); },
  get deleting() { return props.entry.delete.inProgress; },
  existsOnDate: entryExistsOnDate(props),
  nextValidDate: nextValidDate(props),
});
