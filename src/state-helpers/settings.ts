import { present } from '../prelude';
import { ISettingsState } from '../reducer/settings.reducer';
import { IDateSettings, IEvaluationSettings, ISettings } from '../settings/settings';

const adjustedScore = <T extends { settings: ISettingsState }>(props: T) => (score: number, max?: number): number => {
  const evalSettings = present(props.settings.settings).evaluation;
  const maxScore = max || evalSettings.maxScore;
  const floored = score >= 0
    ? Math.floor(score / maxScore * 3)
    : Math.ceil(score / maxScore * 3);
  return Math.min(floored, maxScore);
};

export interface ISettingsHelper {
  loading: boolean;
  evaluation: IEvaluationSettings;
  date: IDateSettings;
  adjustedScore: (score: number, max?: number) => number;
  colorForScore: (score: number, max?: number) => string;
}

export const settings = <T extends { settings: ISettingsState }>(props: T): ISettingsHelper => ({
  get loading() {
    type Key = keyof ISettings;
    return (Object.keys(props.settings.state) as Key[]).some((key) =>
      props.settings.state[key].loading);
  },
  get evaluation(): IEvaluationSettings {
    return present(props.settings.settings).evaluation;
  },
  get date(): IDateSettings {
    return present(props.settings.settings).date;
  },
  adjustedScore: adjustedScore(props),
  colorForScore: (score: number, max?: number): string =>
    present(props.settings.settings).evaluation.scoreToColour[adjustedScore(props)(score, max)]
});
