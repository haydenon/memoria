import { Moment } from 'moment';

import { capitalise, groupBy, Option, present } from '../prelude';

import { ISection } from '../journal';
import { ISectionState } from '../reducer/section.reducer';
import { dateStr, loading } from './common';

interface IRecommendation {
  name: string;
  attribute: boolean;
}

const recommendations = <T extends { section: ISectionState }>(props: T) =>
  (numberToReturn = 5, exclusions?: string[]): IRecommendation[] => {
    const countByName = groupBy(
      props.section.sections.filter((s) => !exclusions || exclusions.indexOf(s.name.toLowerCase()) < 0),
      (s) => s.name.toLowerCase()
    );

    const highestCounts = countByName
      .sort((a, b) => b.count - a.count)
      .slice(0, numberToReturn);

    return highestCounts.map((count) => {
      const attrCount = count.items.filter((item) => item.quality).length;
      return {
        name: capitalise(count.property),
        attribute: attrCount >= (count.count / 2)
      };
    });
  };

export interface ISectionHelper {
  items: ISection[];
  byId: (id: string) => Option<ISection>;
  byDate: (date: string) => ISection[];
  loading: boolean;
  recommendations: (numberToReturn: 5, exclusions: string[]) => IRecommendation[];
}

export const sections = <T extends { section: ISectionState }>(props: T): ISectionHelper => ({
  get items() { return props.section.sections; },
  byId: (id: string): Option<ISection> => {
    const section = props.section.sections.find((s) => s.id === id);
    return section !== undefined ? Option.some(section) : Option.none();
  },
  byDate: (date: string): ISection[] => {
    return props.section.sections.filter((s) => s.date === date);
  },
  get loading() { return loading(props, (p) => p.section.loading); },
  recommendations: recommendations(props)
});
