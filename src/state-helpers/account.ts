import { IAccountState } from '../reducer/account.reducer';
import { IAsyncAction } from '../reducer/async-action';

export interface IAccountHelper {
  initialising: boolean;
  initialised: boolean;
  checking: boolean;
  syncEnabled: boolean;
  enablingSync: boolean;
  passphraseEnabled: boolean;
  waitingForPassphrase: boolean;
  passwordChange: IAsyncAction;
}

export const account = <T extends { account: IAccountState }>(props: T): IAccountHelper => ({
  get initialising() {
    return props.account.initialising;
  },
  get initialised() {
    return props.account.initialised;
  },
  get checking() {
    return props.account.checking;
  },
  get syncEnabled() {
    return props.account.account && props.account.account.sync.enabled || false;
  },
  get enablingSync() {
    return props.account.enableSync.inProgress;
  },
  get passphraseEnabled() {
    return props.account.passphraseEnabled;
  },
  get waitingForPassphrase() {
    return props.account.waitingForPassphrase;
  },
  get passwordChange() {
    return props.account.changePass;
  },
});
