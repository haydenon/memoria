import { Action, Dispatch } from 'redux';

const getScreenWidth = () => Math.max(
  document.documentElement.clientWidth,
  window.innerWidth || 0
);

const getScreenHeight = () => Math.max(
  document.documentElement.clientHeight,
  window.innerHeight || 0
);

type ScreenUpdate = Action & { width: number, height: number };

export interface IScreenState {
  width: number;
  height: number;
}

export const SCREEN_UPDATE = '[screen] Update';

export const screenUpdate = (width: number, height: number): ScreenUpdate => ({
  type: SCREEN_UPDATE,
  width,
  height,
});

// Over 200 to avoid errors being thrown by ant design's tab resizes
const WAIT_TIME = 210;
let timeout = 0;

export const updateScreenWidth = () => (dispatch: Dispatch<Action>) => {
  if (timeout) {
    clearTimeout(timeout);
  }
  timeout = window.setTimeout(() => {
    dispatch(screenUpdate(getScreenWidth(), getScreenHeight()));
  }, WAIT_TIME);
};

const isScreenAction = (action: Action): action is ScreenUpdate => action.type === SCREEN_UPDATE;

export function screen(
  state = {
    width: getScreenWidth(),
    height: getScreenHeight(),
  },
  action: Action
): IScreenState {
  if (isScreenAction(action)) {
    return {
      width: action.width,
      height: action.height,
    };
  } else {
    return state;
  }
}

export const sizes = {
  SMALL: 576,
  MEDIUM: 768,
  LARGE: 992,
  XLARGE: 1200,
  XXLARGE: 1600
};
