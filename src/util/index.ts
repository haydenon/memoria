import moment, { Moment } from 'moment';

import { Option } from '../prelude';
import { ISettings } from '../settings/settings';

export { formSubmit } from './form';

export const dateToString = (date: Moment) => date.format('YYYY-MM-DD');
export const dateFromString = (date: string) => moment(date, 'YYYY-MM-DD');

export const friendlyDate = (date: string, settings: ISettings) =>
  getMoment(date, settings).format(settings.date.friendlyDateFormat);

export const shortDate = (date: string, settings: ISettings) =>
  getMoment(date, settings).format(settings.date.dateFormat);

export const getMoment = (date: string, settings: ISettings) =>
  moment(date, settings.date.dataDateFormat);

export const getDate = (date: Moment, settings: ISettings) =>
  date.format(settings.date.dataDateFormat);

const handleResponse = <TResp>(response: Response): Promise<Option<TResp>> => {
  if (response.status >= 400 && response.status < 600) {
    return response.json().then((err) => { throw new Error(err.errors[0]); });
  } else if (response.status === 204) {
    return Promise.resolve(Option.none());
  }

  return response.json().then(Option.some) as Promise<Option<TResp>>;
};

const postFetch = <TResp>(url: string, body: any) =>
  fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    credentials: 'same-origin',
    body: body ? JSON.stringify(body) : null
  })
    .then((resp) => handleResponse<TResp>(resp))
    .catch(() => Option.none<TResp>());

const putFetch = <TResp>(url: string, body: any) =>
  fetch(url, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json, text/plain, */*',
      'Content-Type': 'application/json'
    },
    credentials: 'same-origin',
    body: body ? JSON.stringify(body) : null
  })
    .then((resp) => handleResponse<TResp>(resp))
    .catch(() => Option.none<TResp>());

const getFetch = <TResp>(url: string) => fetch(url, {
  headers: {
    ['Accept']: 'application/json, text/plain, */*',
  },
  credentials: 'same-origin',
})
  .then((resp) => handleResponse<TResp>(resp))
  .catch(() => Option.none<TResp>());

const deleteFetch = <TResp>(url: string) => fetch(url, {
  method: 'DELETE',
  headers: {
    ['Accept']: 'application/json, text/plain, */*',
  },
  credentials: 'same-origin',
})
  .then((resp) => handleResponse<TResp>(resp))
  .catch(() => Option.none<TResp>());

export const post = postFetch;
export const get = getFetch;
export const del = deleteFetch;
export const put = putFetch;
