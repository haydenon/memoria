import { WrappedFormUtils } from 'antd/lib/form/Form';
import { FormEvent } from 'react';

const errors = (form: WrappedFormUtils) => {
  const fieldErrors = form.getFieldsError() as any;
  return Object.keys(fieldErrors).reduce((errs, key) =>
    errs.concat(fieldErrors[key] || []), []);
};

export function formSubmit<T>(form: WrappedFormUtils, onSuccess: (item: T) => void) {
  return (e: FormEvent<any>) => {
    e.preventDefault();
    form.validateFields();
    if (errors(form).length) {
      return;
    }
    if (onSuccess)
      onSuccess(form.getFieldsValue() as T);
    else if (process.env.NODE_ENV !== 'production')
      // tslint:disable-next-line:no-console
      console.warn('');
  };
}
