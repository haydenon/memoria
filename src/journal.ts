import { Moment } from 'moment';
import { Delta } from 'quill';
import { ShortDateFormatId } from './settings/date-formats';

type ViewId = string;

export type Journal = [IEntry, ISection[]];

export interface IEntry {
  readonly id: ViewId;
  readonly date: string;
  readonly dateFormat: ShortDateFormatId;
  readonly journal: Delta;
}

export interface ISectionQuality {
  label: string;
  score: number;
  max: number;
}

export interface ISection {
  readonly id: ViewId;
  readonly date: string;
  readonly name: string;
  readonly value: string;
  readonly quality?: ISectionQuality;
}

export const Entry = (
  id: ViewId,
  date: string,
  dateFormat: ShortDateFormatId,
  journal: Delta,
): IEntry => ({
  id,
  date,
  dateFormat,
  journal,
});

export const Section = (
  id: ViewId,
  date: string,
  name: string,
  value: string,
  quality?: {
    label: string;
    score: number;
    max: number;
  }
): ISection => ({
  id,
  date,
  name,
  value,
  quality
});
