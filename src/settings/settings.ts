import uuid from 'uuid/v4';

import { createSingletonModelDefinition, IModel } from '../data/model';
import {
  DataDateFormat,
  LongDateFormat,
  LongDateFormats,
  ShortDateFormat,
  ShortDateFormats
} from './date-formats';

const EvaluationDefinition = createSingletonModelDefinition<IEvaluationSettings>('evaluation');
const DateDefinition = createSingletonModelDefinition<IDateSettings>('dateFormat');

export const EVALUATION_DEF = new EvaluationDefinition();
export const DATE_DEF = new DateDefinition();

export interface IEvaluationSettings extends IModel {
  readonly maxScore: number;

  readonly attributes: IAttribute[];

  readonly scoreToColour: { [key: number]: string };
}

export interface IAttribute {
  id: string;
  phrase: string;
  value: number;
}

export const EVALUATION_DEFAULT: IEvaluationSettings = {
  _id: '',
  maxScore: 1,
  scoreToColour: {
    [-3]: '#E84118',
    [-2]: '#EB551C',
    [-1]: '#EF6F21',
    [0]: '#FBC531',
    [1]: '#8DB233',
    [2]: '#72AD34',
    [3]: '#44A535',
  },
  attributes: [
    { phrase: 'Average', value: 0, id: uuid() },
    { phrase: 'Bad', value: -1, id: uuid() },
    { phrase: 'Good', value: 1, id: uuid() },
  ],
};

export interface IDateSettings extends IModel {
  readonly dateFormat: ShortDateFormat;
  readonly dataDateFormat: DataDateFormat;
  readonly friendlyDateFormat: LongDateFormat;
}

export const DATE_DEFAULT: IDateSettings = {
  _id: '',
  dateFormat: ShortDateFormats[0],
  dataDateFormat: 'YYYY-MM-DD',
  friendlyDateFormat: LongDateFormats[0],
};

export interface ISettings {
  readonly evaluation: IEvaluationSettings;
  readonly date: IDateSettings;
}
