import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';

import { Button, Card, Form, Icon, Input, message, Modal, Spin, Switch } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import QRCode from 'react-qr-code';
import { AutoSizer } from 'react-virtualized';

import { Option, present, Result, tee } from '../prelude';

import { getPassphrase } from '../account';
import { enableSync } from '../actions/account.actions';
import { ISyncModel } from '../data/account.model';
import { IState } from '../reducer';
import { IAccountState } from '../reducer/account.reducer';
import { IScreenState } from '../screen';
import { account } from '../state-helpers';
import { getSyncCode } from './sync-code';

import './syncSettings.less';

interface IProps {
  dispatch: Dispatch<IState>;
  account: IAccountState;
  getKey: (passphrase: string) => Promise<Result<JsonWebKey, string>>;
  getSyncDetails: () => Promise<Option<ISyncModel>>;
  testPassphrase: (passphrase: string) => Promise<boolean>;
  screen: IScreenState;
}

interface ISyncState {
  key?: JsonWebKey;
  syncDetails?: ISyncModel;
  connectDevice: boolean;
  passphrase?: string;
  unlockPassphrase: string;
  testing: boolean;
}

class SyncSettings extends React.Component<IProps, ISyncState> {
  private codeInp: Input | null = null;

  constructor(props: IProps) {
    super(props);
    this.state = {
      connectDevice: false,
      unlockPassphrase: '',
      testing: false,
    };
  }

  public componentDidMount() {
    document.title = 'Sync | Memoria';

    this.getKeyDetails();
  }

  public componentDidUpdate() {
    this.getKeyDetails();
  }

  public render() {
    if (!account(this.props).syncEnabled) {
      return this.notEnabled();
    }

    if (account(this.props).passphraseEnabled && this.state.passphrase === undefined) {
      return this.enterPassphrase();
    }

    const HEADER_FOOTER_PADDING = 55 + 53;
    const BODY_PADDING = 24;
    const MODAL_OFFSET = 160;

    const modalSize = Math.min(
      Math.min(
        this.props.screen.width - (BODY_PADDING * 2),
        this.props.screen.height - HEADER_FOOTER_PADDING - (MODAL_OFFSET * 1) - (BODY_PADDING * 2)
      ),
      800
    );

    const MODAL_COPY_HEIGHT = 55;
    const COPY_INPUT_HEIGHT = 32;

    const MARGIN = 20;
    const containerStyle = (size: number) => ({
      padding: MARGIN / 2,
      width: size,
      height: size,
    });
    return (
      <div className="sync-settings" style={{ height: '100%', width: '100%' }}>
        <AutoSizer disableWidth={true}>
          {({ height }) => {
            const HEADER_HEIGHT = 56;
            return (<div style={{ width: '100%' }}>
              <h1>Sync settings</h1>
              <Card style={{ width: '100%' }}>
                <div className="sync-description">
                  <strong>
                    You are currently syncing your data
                  </strong>
                </div>
                <Button size="large" icon="link" type="primary" onClick={this.showConnect}>
                  Connect other device
                </Button>
              </Card>
            </div>);
          }}
        </AutoSizer>
        <Modal
          title="Connect device"
          visible={this.state.connectDevice}
          onCancel={this.closeConnect}
          width={modalSize + (BODY_PADDING * 2)}

          footer={[
            <Button key="close" onClick={this.closeConnect}>Done</Button>,
          ]}

        >
          <div style={{ width: modalSize, height: modalSize }}>
            {!this.state.syncDetails || !this.state.key || !this.state.syncDetails.enabled
              ? <Spin
                spinning={true}
                style={{
                  position: 'absolute',
                  top: (modalSize / 2) - 16 + BODY_PADDING,
                  left: (modalSize / 2) - 16 + BODY_PADDING,
                }}
                size="large"
              />
              : (
                <div>
                  <div
                    style={{
                      height: modalSize - MODAL_COPY_HEIGHT,
                      width: modalSize,
                      marginLeft: MODAL_COPY_HEIGHT / 2,
                      marginRight: MODAL_COPY_HEIGHT / 2,
                    }}
                  >
                    <QRCode value={this.getBaseCode()} level="L" size={modalSize - MODAL_COPY_HEIGHT} />
                  </div>
                  <div
                    style={{
                      height: MODAL_COPY_HEIGHT,
                      width: modalSize,
                      paddingRight: MODAL_COPY_HEIGHT / 2,
                      paddingLeft: MODAL_COPY_HEIGHT / 2,
                      paddingTop: MODAL_COPY_HEIGHT - COPY_INPUT_HEIGHT - 2,
                    }}
                  >
                    <Input
                      value={this.getFriendlyCode()}
                      readOnly={true}
                      ref={(inp) => this.codeInp = inp}
                      style={{ width: modalSize - MODAL_COPY_HEIGHT - 100, marginRight: 10 }}
                    />
                    <CopyToClipboard text={this.getFriendlyCode()} onCopy={this.copyCode}>
                      <Button icon="copy">Copy</Button>
                    </CopyToClipboard>
                  </div>
                </div>
              )
            }
          </div>
        </Modal>

      </div>
    );
  }

  private getKeyDetails = () => {
    if (account(this.props).syncEnabled
      && (this.state.passphrase !== undefined || !this.props.account.passphraseEnabled)
      && !this.state.key) {
      this.props.getKey(getPassphrase(this.state.passphrase))
        .then((res) => Result.match(
          (key) => this.setState({ key }),
          () => tee(message.error('Failed to retrieve key', 3000)),
          res
        ));
    }
    if (account(this.props).syncEnabled && !this.state.syncDetails) {
      this.props.getSyncDetails()
        .then((opt) => Option.match(
          (syncDetails) => this.setState({ syncDetails }),
          () => tee(message.error('Failed to retrive sync details')),
          opt
        ));
    }
  }

  private copyCode = (_text: string, result: boolean) => {
    if (!result)
      message.warning('Unable to copy text');
  }

  private closeConnect = () => {
    this.setState({ connectDevice: false });
  }

  private showConnect = () => {
    this.setState({ connectDevice: true });
  }

  private getFriendlyCode = () => btoa(this.getBaseCode());

  private getBaseCode = () => {
    const key = present(this.state.key);
    const sync = present(this.state.syncDetails);
    return getSyncCode(key, sync);
  }

  private setUnlockPass = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    this.setState({ unlockPassphrase: value });
  }

  private unlock = () => {
    if (this.state.unlockPassphrase) {
      this.setState({ testing: true });
      this.props.testPassphrase(this.state.unlockPassphrase).then((valid) => {
        this.setState({ testing: false });
        if (valid) {
          this.setState({ passphrase: this.state.unlockPassphrase });
        } else {
          message.error('Invalid passphrase');
        }
      });
    }
  }

  private enterPassphrase = () => {
    const width = this.props.screen.width > 420
      ? 400
      : 300;
    return (
      <Card
        style={{
          width,
          marginRight: 'auto',
          marginLeft: 'auto',
          marginTop: 80,
        }}
      >
        <Form.Item>
          <Input placeholder="passphrase" type="password" onChange={this.setUnlockPass} />
        </Form.Item>
        <Button type="primary" icon="key" onClick={this.unlock} disabled={this.state.testing}>
          Unlock sync details
        </Button>
      </Card>
    );
  }

  private notEnabled = () => (
    <div className="exception-page">
      <h1>Not syncing <Icon type="warning" /></h1>
      <p>You are currently not syncing.<br />If you clear your browser data you will lose all your data</p>
      <Button size="large" icon="sync" onClick={this.enable} disabled={account(this.props).enablingSync}>
        Enable syncing
      </Button>
    </div>
  )

  private enable = () => {
    this.props.dispatch(enableSync());
  }
}

const mapStateToProps = (state: IState) => ({
  screen: state.screen,
  account: state.account,
});

export default connect(mapStateToProps)(SyncSettings);
