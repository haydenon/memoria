import { ISyncModel } from '../data/account.model';
import { present } from '../prelude';

type SyncCode = string;

export const getSyncCode = (key: JsonWebKey, sync: ISyncModel): SyncCode => {
  if (!sync.enabled)
    return '';
  const user = present(sync.user);
  const password = present(sync.password);

  return JSON.stringify({
    key,
    user,
    password,
  });
};

export const getFriendlyCode = (code: SyncCode): string => btoa(code);
