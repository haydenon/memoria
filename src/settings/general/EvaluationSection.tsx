import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';

import { Button, Card, Col, Form, Input, Row, Slider } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { SliderValue } from 'antd/lib/slider';
import uuid from 'uuid/v4';

import { updateEvaluation } from '../../actions/settings.actions';
import { delta } from '../../data/data-provider';
import { deleteFromArray, eql, filterMap, updateArray } from '../../prelude';
import { IState } from '../../reducer';
import { ISettingsState } from '../../reducer/settings.reducer';
import { settings } from '../../state-helpers';
import { IAttribute, IEvaluationSettings } from '../settings';

interface IProps {
  settings: ISettingsState;
  dispatch: Dispatch<IState>;
}

type Props = IProps & FormComponentProps;

interface IEvaluationState {
  attributeNames: { [id: string]: string | undefined };
  nameChangeDebounce: { [id: string]: number | undefined };
}

class EvaluationSection extends React.Component<Props, IEvaluationState> {
  constructor(props: Props) {
    super(props);
    this.state = { attributeNames: {}, nameChangeDebounce: {} };
  }

  public componentDidMount() {
    const setting = settings(this.props);
    if (!setting.loading && setting.evaluation.attributes.length) {
      this.updateAttributeNameState(setting.evaluation.attributes);
    }
  }

  public componentDidUpdate(prevProps: Props) {
    if (settings(this.props).loading)
      return;

    const current = settings(this.props).evaluation.attributes;
    if (!settings(this.props).loading
      && (!Object.keys(this.state.attributeNames).length && settings(this.props).evaluation.attributes.length)
      || !eql(settings(prevProps).evaluation.attributes, current)) {
      this.updateAttributeNameState(current);
    }
  }

  public render() {
    const maxScore = settings(this.props).evaluation.maxScore;
    return (
      <Form layout="vertical">
        <div>
          <Form.Item label="Maximum score">
            <Row gutter={24}>
              <Col xs={24} md={16}>
                <Slider
                  value={maxScore}
                  onChange={this.onMaxScoreChange}
                  min={1}
                  max={3}
                  marks={{ 1: '1', 3: '3' }}
                  dots={true}
                />
              </Col>
            </Row>
          </Form.Item>
          <div className="ant-form-item-label">
            <label>Phrases</label>
          </div>
          <div>
            {settings(this.props).evaluation.attributes.map(this.attribute(maxScore))}
          </div>
          <Button icon="plus" type="primary" className="add-evaluation-phrase" onClick={this.addAttribute}>
            Add evaluation phrase
        </Button>
        </div>
      </Form>
    );
  }

  private updateAttributeNameState = (attributes: IAttribute[]) => {
    const attrNames: { [id: string]: string | undefined } = attributes
      .filter((attr) => attr)
      .reduce((acc: { [id: string]: string | undefined }, attr) => {
        acc[attr.id] = attr.phrase;
        return acc;
      }, {});
    this.setState({ attributeNames: attrNames });
  }

  private onMaxScoreChange = (score: SliderValue) => {
    if (score instanceof Array)
      return;

    const updateAttribute = (attr: IAttribute) =>
      Math.abs(attr.value) > score
        ? { ...attr, value: attr.value < 0 ? -score : score }
        : attr;

    const updateMaxScore = (ev: IEvaluationSettings) => ({
      ...ev,
      maxScore: score,
      attributes: ev.attributes.map(updateAttribute)
    });

    this.props.dispatch(updateEvaluation(delta(
      settings(this.props).evaluation,
      updateMaxScore
    )));
  }

  private attribute = (maxScore: number) => (attr: IAttribute) => {
    const { getFieldDecorator } = this.props.form;
    const error = this.attrNameError(attr.id);
    return (
      <Card key={attr.id} bordered={true} className="setting-attribute">
        <Row gutter={24}>
          <Col xs={24} md={12}>
            <Button
              type="danger"
              icon="close"
              shape="circle"
              className="remove-button"
              onClick={this.removeAttribute(attr)}
            />
            <Form.Item className={`attribute-name${error ? ' has-error' : ''}`} help={error}>
              <Input value={this.state.attributeNames[attr.id]} onChange={this.onAttrNameChange(attr.id)} />
            </Form.Item>
          </Col>
          <Col xs={24} md={12}>
            <Slider
              className={this.attributeClass(attr.value)}
              included={false}
              value={attr.value}
              onChange={this.onAttrScoreChange(attr)}
              min={-maxScore}
              max={maxScore}
              marks={{
                [-maxScore]: -maxScore,
                [maxScore]: maxScore
              }}
              dots={true}
            />
          </Col>
        </Row>
      </Card>
    );
  }

  private attrNameError = (id: string) => {
    const value = this.state.attributeNames[id];
    if (value === undefined)
      return undefined;

    return this.valueError(id, value);
  }

  private valueError = (id: string, value: string) => {
    if (!value || !value.trim()) {
      return 'Must provide a phrase';
    }

    const settingValues = settings(this.props).evaluation.attributes
      .filter((a) => a.id !== id)
      .map((a) => a.phrase);
    const inputValues = filterMap(
      (aId) => this.state.attributeNames[aId],
      Object.keys(this.state.attributeNames)
        .filter((aId) => aId !== id));
    const otherValues = [...settingValues, ...inputValues];
    const duplicate = otherValues
      .filter((str) => str)
      .some((str) => str.trim().toLowerCase() === value.trim().toLowerCase());
    return duplicate ? 'Phrase cannot be a duplicate' : undefined;
  }

  private onAttrNameChange = (id: string) => (event: ChangeEvent<HTMLInputElement>) => {
    const debounceTime = 500;
    const value = event.target.value;
    const clearTimer = this.state.nameChangeDebounce[id];
    if (clearTimer !== undefined) {
      clearTimeout(clearTimer);
    }

    const createTimer = () => {
      if (this.valueError(id, value)) {
        return undefined;
      }
      const updateAttr = (ev: IEvaluationSettings) => ({
        ...ev,
        attributes: updateArray(ev.attributes, (a) => a.id === id, { phrase: value })
      });

      return window.setTimeout(
        () => this.props.dispatch(updateEvaluation(delta(
          settings(this.props).evaluation,
          updateAttr
        ))),
        debounceTime);
    };
    const timer = createTimer();
    this.setState({
      attributeNames: {
        ...this.state.attributeNames,
        [id]: value
      },
      nameChangeDebounce: {
        ...this.state.nameChangeDebounce,
        [id]: timer,
      }
    });
  }

  private addAttribute = () => {
    const newAttr = {
      value: 0,
      phrase: 'Enter phrase here...',
      id: uuid()
    };
    const addAttr = (ev: IEvaluationSettings) => ({
      ...ev,
      attributes: [...ev.attributes, newAttr],
    });

    this.props.dispatch(updateEvaluation(delta(
      settings(this.props).evaluation,
      addAttr
    )));
  }

  private removeAttribute = (attr: IAttribute) => () => {
    const deleteAttr = (ev: IEvaluationSettings) => ({
      ...ev,
      attributes: deleteFromArray(ev.attributes, (a) => a.id === attr.id)
    });

    this.props.dispatch(updateEvaluation(delta(
      settings(this.props).evaluation,
      deleteAttr
    )));
  }

  private onAttrScoreChange = (attr: IAttribute) => (score: SliderValue) => {
    if (score instanceof Array)
      return;

    const updateAttr = (ev: IEvaluationSettings) => ({
      ...ev,
      attributes: updateArray(ev.attributes, (a) => a.id === attr.id, { value: score })
    });

    this.props.dispatch(updateEvaluation(delta(
      settings(this.props).evaluation,
      updateAttr
    )));
  }

  private attributeClass = (value: number) =>
    `s${settings(this.props).adjustedScore(value)}`
}

const mapStateToProps = (state: IState) => ({
  settings: state.settings
});

export default connect(mapStateToProps)(Form.create()(EvaluationSection));
