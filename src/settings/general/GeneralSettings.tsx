import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';

import { Collapse } from 'antd';
import { SliderValue } from 'antd/lib/slider';

import { setBreadcrumbs } from '../../actions/breadcrumbs.actions';
import { IState } from '../../reducer';
import { ISettingsState } from '../../reducer/settings.reducer';
import { settings } from '../../state-helpers';
import { IAttribute, IEvaluationSettings } from '../settings';
import AccountSection from './AccountSection';
import DataSection from './DataSection';
import DateSection from './DateSection';
import EvaluationSection from './EvaluationSection';

const Panel = Collapse.Panel;

import './generalSettings.less';

interface IProps {
  settings: ISettingsState;
  dispatch: Dispatch<IState>;
  testPassphrase: (passphrase: string) => Promise<boolean>;
}

class GeneralSettings extends React.Component<IProps> {
  public componentDidMount() {
    document.title = 'Settings | Memoria';

    this.props.dispatch(setBreadcrumbs([{
      label: 'Settings'
    }]));
  }

  public render() {
    if (settings(this.props).loading) {
      return null;
    }

    return (
      <div className="general-settings">
        <h1>General settings</h1>
        <Collapse bordered={false}>
          <Panel header="Date formatting" key="date-format">
            <DateSection />
          </Panel>

          <Panel key="evaluation" header="Evaluation">
            <EvaluationSection />
          </Panel>

          <Panel key="account" header="Account">
            <AccountSection testPassphrase={this.props.testPassphrase} />
          </Panel>

          <Panel key="data" header="Data">
            <DataSection />
          </Panel>
        </Collapse>
      </div>
    );
  }
}

const mapStateToProps = (state: IState) => ({
  settings: state.settings
});

const timers: { [key: string]: number | undefined } = {};

export default connect(mapStateToProps)(GeneralSettings);
