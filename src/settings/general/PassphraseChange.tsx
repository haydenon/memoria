import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';

import { Button, Col, Collapse, Form, Input, message, Modal, Row } from 'antd';
import { FormComponentProps } from 'antd/lib/form';

import { getPassphrase } from '../../account';
import { IState } from '../../reducer';
import { IAccountState } from '../../reducer/account.reducer';
import { account, IAccountHelper } from '../../state-helpers';
import { formSubmit } from '../../util';

interface IChangeProps {
  dispatch: Dispatch<IState>;
  account: IAccountHelper;
  testPassphrase: (passphrase: string) => Promise<boolean>;
  onChange: (oldPassphrase: string, newPassphrase: string | undefined) => Promise<void>;
}

type ChangeProps = IChangeProps & FormComponentProps;

interface IChangeState {
  oldPassphrase?: string;
  testing: boolean;
  chooseNew: boolean;
}

const mapChangeProps = (state: IState) => ({
  account: account(state)
});

interface IFormResult { 'new-passphrase': string; 'confirm-passphrase': string; }

export default connect(mapChangeProps)
  (Form.create()(class PasswordForm extends React.Component<ChangeProps, IChangeState> {
    private onSubmit = formSubmit(this.props.form, (passphrases: IFormResult) => {
      if (passphrases['confirm-passphrase'] === passphrases['new-passphrase']) {
        this.props.onChange(getPassphrase(this.state.oldPassphrase), passphrases['new-passphrase'])
          .then(() => this.onSuccessfulChange());
      }
    });

    constructor(props: ChangeProps) {
      super(props);
      this.state = {
        testing: false,
        chooseNew: false,
      };
    }

    public componentDidMount() {
      if (!this.state.chooseNew && !this.props.account.passphraseEnabled) {
        this.setState({ chooseNew: true });
      }
    }

    public componentDidUpdate() {
      if (!this.state.chooseNew && !this.props.account.passphraseEnabled) {
        this.setState({ chooseNew: true });
      }
    }

    public render() {
      const { getFieldDecorator } = this.props.form;
      return (
        <div className="change-passphrase">
          <h2>Change passphrase</h2>
          <Form onSubmit={this.onSubmit}>
            <Row>
              <Col xs={24} sm={21} md={18} lg={15} xl={12}>
                {this.state.chooseNew ? null : <Form.Item>
                  {getFieldDecorator('current-passphrase')(
                    <Input type="password" placeholder="Current passphrase" onPressEnter={this.checkPassphrase} />
                  )}
                </Form.Item>}
                <Collapse bordered={false} activeKey={this.state.chooseNew ? 'new-passphrase' : undefined}>
                  <Collapse.Panel key="new-passphrase" header="" showArrow={false}>
                    <Form.Item>
                      {getFieldDecorator('new-passphrase', {
                        validateTrigger: 'onBlur',
                        rules: [
                          { required: true, message: 'Please enter passphrase' },
                          { message: '', validator: this.checkConfirm }
                        ]
                      })(<Input type="password" placeholder="New passphrase" />)}
                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('confirm-passphrase', {
                        validateTrigger: 'onBlur',
                        rules: [
                          { required: true, message: 'Please enter passphrase' },
                          { message: 'The passwords must match', validator: this.checkPassword }
                        ]
                      })(<Input type="password" placeholder="Confirm new passphrase" />)}
                    </Form.Item>
                  </Collapse.Panel>
                </Collapse>
                <Form.Item>
                  {this.state.chooseNew
                    ? (
                      <div>
                        <Button
                          type="primary"
                          htmlType="submit"
                          style={{ marginRight: 10 }}
                          disabled={this.props.account.passwordChange.inProgress}
                        >
                          Update
                        </Button>
                        {this.props.account.passphraseEnabled
                          ? <Button
                            onClick={this.removePassphrase}
                            disabled={this.props.account.passwordChange.inProgress}
                          >
                            Remove passphrase
                          </Button>
                          : null}
                      </div>
                    )
                    : (
                      <Button type="primary" onClick={this.checkPassphrase} disabled={this.state.testing}>
                        Check
                      </Button>
                    )}
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
      );
    }

    private onSuccessfulChange = () => {
      message.success('Passphrase successfully changed');

      this.props.form.setFieldsValue({
        'new-passphrase': '',
        'confirm-passphrase': '',
      });

      this.setState({
        oldPassphrase: undefined,
        testing: false,
        chooseNew: false,
      });
    }

    private checkPassword = (rule: any, value: string, callback: (val?: any) => void) => {
      const password = this.props.form.getFieldValue('new-passphrase');
      if (password === value) {
        callback();
      } else {
        callback(true);
      }
    }

    private checkConfirm = (rule: any, value: string, callback: (val?: any) => void) => {
      const form = this.props.form;
      if (value && form.getFieldValue('confirm-passphrase')) {
        form.validateFields(['confirm-passphrase'], { force: true }, undefined as any);
      }
      callback();
    }

    private checkPassphrase = () => {
      this.setState({ testing: true });
      const passphrase = this.props.form.getFieldValue('current-passphrase');
      this.props.testPassphrase(passphrase)
        .then((valid) => {
          this.setState({ testing: false });
          if (valid) {
            this.props.form.resetFields();
            this.setState({ chooseNew: true, oldPassphrase: passphrase });
          } else {
            message.error('Incorrect passphrase');
          }
        });
    }

    private removePassphrase = () => {
      if (!this.state.oldPassphrase) {
        return;
      }

      this.props.onChange(this.state.oldPassphrase, undefined);
    }
  }));
