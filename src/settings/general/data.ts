import moment, { Moment } from 'moment';

import Delta, { DeltaOperation } from 'quill-delta';
import { IEntry, ISection, ISectionQuality, Journal } from '../../journal';
import { capitalise, groupBy, present } from '../../prelude';

export interface ISectionBackup {
  name: string;
  value: string;
  quality?: ISectionQuality;
}

export interface IEntryBackup {
  date: string;
  dateFormatId: number;
  journal: DeltaOperation[];
  sections: ISectionBackup[];
}

export interface IDataBackup {
  entries: IEntryBackup[];
}

const fromSection = (section: ISection): ISectionBackup => ({
  name: section.name,
  value: section.value,
  quality: section.quality
});

const toSection = (date: string) => (section: ISectionBackup): ISection => ({
  id: '',
  date,
  name: section.name,
  value: section.value,
  quality: section.quality,
});

export const fromEntry = ([entry, sections]: Journal): IEntryBackup => ({
  date: entry.date,
  dateFormatId: entry.dateFormat,
  journal: present(entry.journal.ops),
  sections: sections.map(fromSection)
});

const toEntry = (entry: IEntryBackup): IEntry => ({
  id: '',
  date: entry.date,
  dateFormat: entry.dateFormatId,
  journal: new Delta(entry.journal),
});

export const toJournal = (backupEntry: IEntryBackup): Journal => {
  const sections = backupEntry.sections.map(toSection(backupEntry.date));
  const entry = toEntry(backupEntry);
  return [entry, sections];
};

const validJournal = (journal: Journal) => {
  const e = journal[0];
  const validEntry = e.dateFormat > 0
    && e.id === ''
    && e.journal.ops
    && e.journal.ops.length;
  if (!validEntry)
    return false;

  const sections = journal[1];
  return sections.every((s) =>
    !!(s.id === ''
      && s.name
      && (!s.quality
        || (s.quality.label
          && s.quality.max > 0
          && Math.abs(s.quality.score) <= s.quality.max
        ))));
};

export const validate = (journals: Journal[]): [Journal[], string[]] => {
  const errors: string[] = [];

  const validJournals = journals.filter(validJournal);
  const difference = journals.length - validJournals.length;
  if (length !== 0) {
    errors.push(`${length} journal entries have invalid fields`);
  }

  const duplicateDays = groupBy(validJournals.map((j) => j[0]), (e) => e.date)
    .filter((g) => g.count > 1);

  if (duplicateDays.length) {
    const days = duplicateDays.map((g) => g.property);
    errors.push(`Import has duplicate entries for these days: ${days.join(', ')}`);
  }

  const nonDuplicates = duplicateDays.length
    ? validJournals.filter((j) => duplicateDays.find((d) => d.property === j[0].date) === undefined)
    : validJournals;

  const toImport = nonDuplicates.map((j) => {
    const norm = (name: string) => name ? name.trim().toLowerCase() : name;
    const duplicateSections = groupBy(j[1], (s) => norm(s.name))
      .filter((g) => g.count > 1);

    if (duplicateSections.length) {
      const dupSections = duplicateSections.map((ds) => `'${capitalise(ds.property)}'`).join(', ');
      errors.push(`Entry for day ${j[0].date} has these duplicate sections: ${dupSections}`);
    }

    const nonDuplicateSect = duplicateSections.length
      ? j[1].filter((s) => duplicateSections.find((ds) => ds.property === norm(s.name)) === undefined)
      : j[1];

    const journal: Journal = [j[0], nonDuplicateSect];
    return journal;
  });

  return [toImport, errors];
};
