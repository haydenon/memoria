import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';

import { Button, message, Modal, Progress, Upload } from 'antd';

import { UploadFile } from 'antd/lib/upload/interface';
import { importJournals } from '../../actions/entry.actions';
import { IEntry, Journal } from '../../journal';
import { IState } from '../../reducer';
import { ISettingsState } from '../../reducer/settings.reducer';
import { entries, IEntryHelper, ISectionHelper, sections } from '../../state-helpers';
import { fromEntry, IDataBackup, IEntryBackup, toJournal, validate } from './data';

interface IProps {
  dispatch: Dispatch<IState>;
  entries: IEntryHelper;
  sections: ISectionHelper;
  settings: ISettingsState;
}

interface IUploadState {
  importing: boolean;
  toUpload?: Journal[];
  validationErrors?: string[];
  progress?: { done: number; total: number; };
}

class DataSection extends React.Component<IProps, IUploadState> {
  private uploadCancelCallback?: () => void;

  constructor(props: IProps) {
    super(props);
    this.state = {
      importing: false,
    };
  }

  public render() {
    return (
      <div>
        <h2>Import data</h2>
        <Upload accept=".json" beforeUpload={this.uploadData}>
          <Button icon="upload" style={{ marginBottom: 15 }}>Import your data</Button>
        </Upload>
        <h2>Export data</h2>
        <Button icon="download" onClick={this.downloadData}>Export your data</Button>
        <Modal
          title="Import data"
          visible={this.state.toUpload !== undefined}
          onCancel={this.cancelUpload}
          footer={[
            <Button key="cancel" onClick={this.cancelUpload}>
              {this.state.progress && this.state.progress.done === this.state.progress.total
                ? 'Close'
                : 'Cancel'
              }
            </Button>]}
        >
          {this.modalContent()}
        </Modal>
      </div>
    );
  }

  private modalContent = () => {
    return this.state.importing
      ? this.uploadingModal()
      : this.uploadModal();
  }

  private uploadingModal = () => {
    const progress = (done: number, total: number) => {
      const percent = Math.round((done / total) * 100);
      return (
        <div style={{ width: 120, height: 120, marginRight: 'auto', marginLeft: 'auto' }}>
          <Progress percent={percent} type="circle" />
        </div>
      );
    };
    return (
      <div>
        <h2>Importing and encrypting your data!</h2>
        {this.state.progress !== undefined
          ? progress(this.state.progress.done, this.state.progress.total)
          : null
        }
      </div>
    );
  }

  private uploadModal = () => {
    return (
      <div>
        <h2>Data validation</h2>
        <div>
          {this.state.validationErrors !== undefined && this.state.validationErrors.length
            ? <ul>
              {
                this.state.validationErrors.map((err, ind) => (
                  <li key={ind}>
                    {err}
                  </li>
                ))
              }
            </ul>
            : <p>
              Data passed all validation. Entries with dates that conflict with existing data will be excluded.
              </p>}
        </div>
        {
          this.state.toUpload && this.state.toUpload.length
            ? <div>
              {this.state.validationErrors && this.state.validationErrors.length
                ? <p>
                  You can upload the valid parts of your data, but the invalid parts will be excluded.
                  Entries with dates that conflict with existing data will also be excluded.
                  </p>
                : null
              }
              <Button type="primary" onClick={this.startUpload}>
                {this.state.validationErrors && this.state.validationErrors.length
                  ? 'Import valid data anyway'
                  : 'Import'
                }
              </Button>
            </div>
            : null
        }</div>
    );
  }

  private startUpload = () => {
    if (!this.state.toUpload)
      return;

    this.setState({ importing: true });

    const progressListener = (done: number, total: number) => {
      this.setState({ progress: { done, total } });
      if (done === total) {
        this.uploadCancelCallback = undefined;
      }
    };
    const importOptions = {
      journals: this.state.toUpload,
      progressListener,
      getCancelCallback: (cancel: () => void) => this.uploadCancelCallback = cancel
    };
    this.props.dispatch(importJournals(importOptions, (prom) => prom
      .catch(() => message.error('Error occured while uploading data'))));
  }

  private cancelUpload = () => {
    if (this.uploadCancelCallback) {
      message.warning('Cancelled import midway');
      this.uploadCancelCallback();
    }

    this.setState({
      importing: false,
      toUpload: undefined,
      validationErrors: undefined,
      progress: undefined,
    });
  }

  private uploadData = (file: UploadFile) => {
    const fr = new FileReader();

    fr.onload = (e) => {
      const result = JSON.parse((e.target as any).result) as IDataBackup;
      const journals = result.entries.map(toJournal);
      const validated = validate(journals);
      this.setState({ toUpload: validated[0], validationErrors: validated[1] });
    };

    fr.readAsText(file as any as File);

    return false;
  }

  private downloadData = () => {
    const blob = new Blob([JSON.stringify(this.getBackup())], { type: 'text/json' });
    const downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute('href', window.URL.createObjectURL(blob));
    downloadAnchorNode.setAttribute('download', 'memoria_backup.json');
    document.body.appendChild(downloadAnchorNode);
    downloadAnchorNode.click();
    document.body.removeChild(downloadAnchorNode);
    downloadAnchorNode.remove();
  }

  private getBackup = (): IDataBackup => ({
    entries: this.props.entries.nonDuplicates
      .map(this.mapEntryToBackup)
  })

  private mapEntryToBackup = (entry: IEntry) => {
    const entrySections = this.props.sections.byDate(entry.date);
    return fromEntry([entry, entrySections]);
  }
}

const mapStateToProps = (state: IState) => ({
  entries: entries(state),
  sections: sections(state),
  settings: state.settings,
});

export default connect(mapStateToProps)(DataSection);
