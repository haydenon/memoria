import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';

import { Button, Col, Collapse, Form, Input, message, Modal, Row } from 'antd';
import { push } from 'react-router-redux';
import { getPassphrase } from '../../account';
import { changePassphrase, deleteAccount } from '../../actions/account.actions';
import { IState } from '../../reducer';
import { IAccountState } from '../../reducer/account.reducer';
import { account, IAccountHelper } from '../../state-helpers';
import ChangePassphrase from './PassphraseChange';

interface IProps {
  dispatch: Dispatch<IState>;
  account: IAccountState;
  testPassphrase: (passphrase: string) => Promise<boolean>;
}

interface IAccountSectionState {
  showDelete: boolean;
  deleteInput: string;
}

class AccountSection extends React.Component<IProps, IAccountSectionState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      showDelete: false,
      deleteInput: '',
    };
  }

  public render() {
    return (
      <div>
        <ChangePassphrase onChange={this.onPassphraseChange} testPassphrase={this.props.testPassphrase} />

        <h2>Delete account</h2>
        <Button type="danger" icon="delete" onClick={this.showDelete(true)}>Delete account</Button>
        <Modal
          title="Delete account"
          visible={this.state.showDelete}
          onCancel={this.showDelete(false)}
          onOk={this.deleteAccount}
          footer={[
            <Button key="cancel" icon="close" type="primary" onClick={this.showDelete(false)}>Cancel</Button>,
            <Button key="delete" type="danger" onClick={this.deleteAccount} icon="delete" disabled={!this.canDelete()}>
              Delete
            </Button>
          ]}
        >
          <p>
            Please note that deleting here will not remove your data from other devices.
            <br /><br />
            To confirm that you want to delete your account from this device,
            please type <strong><i>delete</i></strong> into the box below
          </p>
          <Input onChange={this.onDeleteChange} value={this.state.deleteInput} placeholder="delete" />
        </Modal>
      </div>
    );
  }

  public onPassphraseChange = (oldPassphrase: string, newPassphrase: string | undefined) =>
    new Promise<void>((resolve, reject) => this.props.dispatch(
      changePassphrase(
        oldPassphrase,
        newPassphrase,
        (prom) => prom
          .then(() => resolve())
          .catch(() => {
            message.error('Failed to change password');
            reject();
          })
      )
    ))

  public onDeleteChange = (event: ChangeEvent<HTMLInputElement>) =>
    this.setState({ deleteInput: event.target.value })

  public canDelete = () =>
    this.state.deleteInput.trim().toLowerCase() === 'delete'

  public showDelete = (show: boolean) => () => {
    if (!show) {
      this.setState({ showDelete: false, deleteInput: '' });
    } else {
      this.setState({ showDelete: true });
    }
  }

  public deleteAccount = () => {
    this.props.dispatch(deleteAccount(() =>
      window.location.href = '/start'));
  }
}

const mapStateToProps = (state: IState) => ({
  account: state.account,
});

export default connect(mapStateToProps)(AccountSection);
