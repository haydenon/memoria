import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Col, Form, Row, Select } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { SelectValue } from 'antd/lib/select';
import moment from 'moment';

import { updateDate } from '../../actions/settings.actions';
import { delta } from '../../data/data-provider';
import { IState } from '../../reducer';
import { ISettingsState } from '../../reducer/settings.reducer';
import { settings } from '../../state-helpers';
import { isLongDateFormat, isShortDateFormat, LongDateFormats, ShortDateFormats } from '../date-formats';
import { IDateSettings } from '../settings';

const Option = Select.Option;

interface IProps {
  settings: ISettingsState;
  dispatch: Dispatch<IState>;
}

type Props = IProps & FormComponentProps;

class DateSection extends React.Component<Props> {
  public render() {
    const date = moment();
    return (
      <Form layout="vertical">
        <Row gutter={24}>
          <Col md={12} xs={24}>
            <Form.Item label="Short format">
              <Select defaultValue={settings(this.props).date.dateFormat} onSelect={this.onShortChange}>
                {ShortDateFormats.map((f) => (
                  <Option key={f} value={f}>
                    {date.format(f)}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col md={12} xs={24}>
            <Form.Item label="Long format">
              <Select defaultValue={settings(this.props).date.friendlyDateFormat} onSelect={this.onLongChange}>
                {LongDateFormats.map((f) => (
                  <Option key={f} value={f}>
                    {date.format(f)}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }

  private onShortChange = (selectValue: SelectValue) => {
    const value = selectValue.valueOf();
    if (typeof value !== 'string' || !isShortDateFormat(value))
      return;

    const updateShort = (date: IDateSettings) => ({
      ...date,
      dateFormat: value
    });

    this.props.dispatch(updateDate(delta(
      settings(this.props).date,
      updateShort,
    )));
  }

  private onLongChange = (selectValue: SelectValue) => {
    const value = selectValue.valueOf();
    if (typeof value !== 'string' || !isLongDateFormat(value))
      return;

    const updateLong = (date: IDateSettings) => ({
      ...date,
      friendlyDateFormat: value
    });

    this.props.dispatch(updateDate(delta(
      settings(this.props).date,
      updateLong,
    )));
  }
}

const mapStateToProps = (state: IState) => ({
  settings: state.settings
});

export default connect(mapStateToProps)(Form.create()(DateSection));
