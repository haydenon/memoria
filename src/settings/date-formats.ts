import { Option } from '../prelude';

export type DataDateFormat = 'YYYY-MM-DD';

export type ShortDateFormat =
  'YYYY/MM/DD' |
  'YYYY-MM-DD' |
  'YYYY.MM.DD' |
  'DD/MM/YYYY' |
  'DD-MM-YYYY' |
  'DD.MM.YYYY' |
  'MM/DD/YYYY' |
  'MM-DD-YYYY' |
  'MM.DD.YYYY';

export const ShortDateFormats: ShortDateFormat[] = [
  'YYYY/MM/DD',
  'YYYY-MM-DD',
  'YYYY.MM.DD',
  'DD/MM/YYYY',
  'DD-MM-YYYY',
  'DD.MM.YYYY',
  'MM/DD/YYYY',
  'MM-DD-YYYY',
  'MM.DD.YYYY',
];

export type ShortDateFormatId = number;

type ShortFormatIds = {[K in ShortDateFormat]: ShortDateFormatId };

export const ShortDateFormatIds: ShortFormatIds = {
  'YYYY/MM/DD': 1,
  'YYYY-MM-DD': 2,
  'YYYY.MM.DD': 3,
  'DD/MM/YYYY': 4,
  'DD-MM-YYYY': 5,
  'DD.MM.YYYY': 6,
  'MM/DD/YYYY': 7,
  'MM-DD-YYYY': 8,
  'MM.DD.YYYY': 9,
};

export const shortDateFromId = (id: number): Option<ShortDateFormat> =>
  Option.fromUndefined(
    (Object.keys(ShortDateFormatIds) as ShortDateFormat[])
      .find((key) => ShortDateFormatIds[key] === id)
  );

export const isShortDateFormat = (str: string): str is ShortDateFormat =>
  ShortDateFormats.indexOf(str as ShortDateFormat) >= 0;

export type LongDateFormatId = number;

type LongFormatIds = {[K in LongDateFormat]: LongDateFormatId };

export const LongDateFormatIds: LongFormatIds = {
  'dddd, MMMM Do, YYYY': 1,
  'ddd, MMM Do, YY': 2,
};

export const longDateFromId = (id: number): Option<LongDateFormat> =>
  Option.fromUndefined(
    (Object.keys(LongDateFormatIds) as LongDateFormat[])
      .find((key) => LongDateFormatIds[key] === id)
  );

export type LongDateFormat =
  'dddd, MMMM Do, YYYY' |
  'ddd, MMM Do, YY';

export const LongDateFormats: LongDateFormat[] = [
  'dddd, MMMM Do, YYYY',
  'ddd, MMM Do, YY',
];

export const isLongDateFormat = (str: string): str is LongDateFormat =>
  LongDateFormats.indexOf(str as LongDateFormat) >= 0;
