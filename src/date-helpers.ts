import moment, { Moment } from 'moment';

import { capitalise } from './prelude';

type Period = 'day' | 'week' | 'month' | 'year' | 'future';

const getSmallestPeriodType = (startDate: Moment, endDate: Moment): Period => {
  if (endDate.diff(startDate, 'seconds') < 0) {
    return 'future';
  } else if (endDate.diff(startDate, 'week') < 1) {
    return 'day';
  } else if (endDate.diff(startDate, 'month') < 1) {
    return 'week';
  } else if (endDate.diff(startDate, 'year') < 1) {
    return 'month';
  } else {
    return 'year';
  }
};

const getYearDescription = (date: Moment, endDate: Moment): string => {
  const years = Math.floor(endDate.diff(date, 'year', true));
  if (endDate.startOf('year').diff(date.startOf('year'), 'year', true) < 2) {
    return 'Last year';
  }

  return `${years} year${years > 1 ? 's' : ''} ago`;
};

const getMonthDescription = (date: Moment, endDate: Moment): string => {
  const months = Math.floor(endDate.diff(date, 'month', true));
  if (endDate.startOf('month').diff(date.startOf('month'), 'month', true) < 2) {
    return 'Last month';
  }

  return `${months} month${months > 1 ? 's' : ''} ago`;
};

const getWeekDescription = (date: Moment, endDate: Moment): string => {
  const weeks = Math.floor(endDate.diff(date, 'week', true));
  if (endDate.startOf('week').diff(date.startOf('week'), 'week', true) < 2) {
    return 'Last week';
  }

  return `${weeks} week${weeks > 1 ? 's' : ''} ago`;
};

const getDayDescription = (date: Moment, endDate: Moment): string => {
  const hours = endDate.diff(date, 'hour');
  if (hours < 24) {
    return 'Today';
  } else if (hours < 48) {
    return 'Yesterday';
  }

  return capitalise(moment(date).from(endDate.startOf('day')));
};

export const readableTimeSince = (startDate: Moment, endDate = moment()): string => {
  const d1 = moment(startDate);
  const d2 = moment(endDate);
  const type = getSmallestPeriodType(d1, d2);
  let description: string;
  switch (type) {
    case 'future':
      return capitalise(moment(d1).from(d2.startOf('day')));
    case 'year':
      description = getYearDescription(d1, d2);
      break;
    case 'month':
      description = getMonthDescription(d1, d2);
      break;
    case 'week':
      description = getWeekDescription(d1, d2);
      break;
    case 'day':
    default:
      description = getDayDescription(d1, d2);
      break;
  }

  return description;
};
