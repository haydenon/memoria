const routes = new Map([
  ['ImportScreen', '/start/import'],
  ['UnlockScreen', '/start/passphrase'],
  ['BackupScreen', '/start/backup'],
  ['StartScreen', '/start'],
  ['JournalCreate', '/journal/new'],
  ['JournalDuplicates', '/journal/duplicates'],
  ['JournalEdit', '/journal/:date'],
  ['JournalList', '/journal'],
  ['GeneralSettings', '/settings/general'],
  ['SyncSettings', '/settings/sync'],
  ['Home', '/'],
]);

export default routes;
