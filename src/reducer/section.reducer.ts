import { Action } from 'redux';

import { AccountDeleteActions } from '../actions/account.actions';
import { isOfDatalessType, isOfType } from '../actions/actions';
import { SectionLoad } from '../actions/section.actions';
import { ISection, Section } from '../journal';

export interface ISectionState {
  sections: ISection[];
  loading: boolean;
}

const originalState: ISectionState = {
  sections: [],
  loading: true,
};

export const section = (state = originalState, action: Action): ISectionState => {
  if (isOfType(action, SectionLoad)) {
    return {
      ...state,
      sections: action.data,
      loading: false,
    };
  } else if (isOfType(action, AccountDeleteActions.Success)) {
    return originalState;
  } else {
    return state;
  }
};
