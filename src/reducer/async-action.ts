export interface IAsyncAction {
  readonly inProgress: boolean;
  readonly error: string | null;
}

export const asyncInitState: IAsyncAction = {
  inProgress: false,
  error: null
};

export const asyncInProgress = (state: IAsyncAction): IAsyncAction => ({
  inProgress: true,
  error: state.error,
});

export const asyncError = (error: string): IAsyncAction => ({
  inProgress: false,
  error,
});
