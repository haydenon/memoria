import { Action } from 'redux';

import { sort } from '../prelude';

import { AccountDeleteActions } from '../actions/account.actions';
import { isOfDatalessType, isOfType } from '../actions/actions';
import {
  EntryLoad,
  JournalCreate,
  JournalCreated,
  JournalCreateFailed,
  JournalDelete,
  JournalDeleted,
  JournalDeleteFailed,
  JournalImportActions,
  JournalUpdate,
  JournalUpdated,
  JournalUpdateFailed
} from '../actions/entry.actions';
import { IEntry } from '../journal';
import { asyncError, asyncInitState, asyncInProgress, IAsyncAction } from './async-action';

export interface IEntryState {
  entries: IEntry[];
  loading: boolean;
  create: IAsyncAction;
  update: IAsyncAction;
  delete: IAsyncAction;
}

const originalState: IEntryState = {
  entries: [],
  loading: true,
  create: asyncInitState,
  update: asyncInitState,
  delete: asyncInitState,
};

export const entry = (state = originalState, action: Action): IEntryState => {
  if (isOfType(action, EntryLoad)) {
    return {
      ...state,
      entries: sort(action.data, (e1, e2) => e2.date.localeCompare(e1.date)),
      loading: false
    };
  } else if (isOfType(action, JournalCreate)) {
    return {
      ...state,
      create: {
        inProgress: true,
        error: null
      }
    };
  } else if (isOfType(action, JournalCreated)) {
    return {
      ...state,
      create: {
        ...state.create,
        inProgress: false
      }
    };
  } else if (isOfType(action, JournalCreateFailed)) {
    return {
      ...state,
      create: {
        ...state.create,
        error: action.data
      }
    };
  } else if (isOfType(action, JournalUpdate)) {
    return {
      ...state,
      update: {
        inProgress: true,
        error: null
      }
    };
  } else if (isOfType(action, JournalUpdated)) {
    return {
      ...state,
      update: {
        ...state.update,
        inProgress: false
      }
    };
  } else if (isOfType(action, JournalUpdateFailed)) {
    return {
      ...state,
      update: {
        ...state.update,
        error: action.data
      }
    };
  } else if (isOfType(action, JournalDelete)) {
    return {
      ...state,
      delete: {
        inProgress: true,
        error: null
      }
    };
  } else if (isOfType(action, JournalDeleted)) {
    return {
      ...state,
      delete: {
        ...state.delete,
        inProgress: false
      }
    };
  } else if (isOfType(action, JournalDeleteFailed)) {
    return {
      ...state,
      delete: {
        ...state.delete,
        error: action.data
      }
    };
  } else if (isOfType(action, JournalImportActions.Start)) {
    return {
      ...state,
      create: asyncInProgress(state.create),
    };
  } else if (isOfType(action, JournalImportActions.Success)) {
    return {
      ...state,
      create: asyncInitState,
    };
  } else if (isOfType(action, JournalImportActions.Error)) {
    return {
      ...state,
      create: asyncError(action.data),
    };
  } else if (isOfType(action, AccountDeleteActions.Success)) {
    return originalState;
  } else {
    return state;
  }
};
