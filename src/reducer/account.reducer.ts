import { Action } from 'redux';

import { IAccount } from '../account';
import {
  AccountCheck,
  AccountChecked,
  AccountCreateActions,
  AccountDeleteActions,
  AccountImportActions,
  AccountImportDataActions,
  PassphraseChangeActions,
  PassphraseRequired,
  SyncEnableActions,
} from '../actions/account.actions';
import { isOfDatalessType, isOfType } from '../actions/actions';
import { asyncError, asyncInitState, asyncInProgress, IAsyncAction } from './async-action';

export interface IAccountState {
  account?: IAccount;
  checking: boolean;
  waitingForPassphrase: boolean;
  passphraseEnabled: boolean;
  initialised: boolean;
  initialising: boolean;
  enableSync: IAsyncAction;
  delete: IAsyncAction;
  changePass: IAsyncAction;
  dataImport: IAsyncAction;
}

const initialState = {
  checking: true,
  waitingForPassphrase: true,
  passphraseEnabled: false,
  initialised: false,
  initialising: false,
  enableSync: asyncInitState,
  delete: asyncInitState,
  changePass: asyncInitState,
  dataImport: asyncInitState,
};

export const account = (state = initialState, action: Action): IAccountState => {
  if (isOfDatalessType(action, AccountCheck)) {
    return {
      ...state,
      checking: true,
    };
  } else if (isOfDatalessType(action, PassphraseRequired)) {
    return {
      ...state,
      checking: false,
      waitingForPassphrase: true,
      passphraseEnabled: true,
    };
  } else if (isOfType(action, AccountChecked)) {
    return {
      ...state,
      checking: false,
      account: action.data && action.data.account,
      initialised: action.data !== undefined && action.data.account !== undefined,
      waitingForPassphrase: false,
      passphraseEnabled: action.data && action.data.passphraseEnabled || false,
    };
  } else if (isOfType(action, AccountCreateActions.Start)) {
    return {
      ...state,
      initialising: true
    };
  } else if (isOfType(action, AccountCreateActions.Success)) {
    return {
      ...state,
      account: action.data.account,
      passphraseEnabled: action.data.passphraseEnabled,
      initialising: false,
      initialised: true,
    };
  } else if (isOfType(action, AccountCreateActions.Error)) {
    return {
      ...state,
      initialising: false,
    };
  } else if (isOfType(action, SyncEnableActions.Start)) {
    return {
      ...state,
      enableSync: asyncInProgress(state.enableSync),
    };
  } else if (isOfType(action, SyncEnableActions.Success)) {
    return {
      ...state,
      enableSync: {
        ...state.enableSync,
        inProgress: false,
      },
      account: {
        ...account,
        sync: action.data
      }
    };
  } else if (isOfType(action, SyncEnableActions.Error)) {
    return {
      ...state,
      enableSync: asyncError(action.data)
    };
  } else if (isOfType(action, PassphraseChangeActions.Start)) {
    return {
      ...state,
      changePass: asyncInProgress(state.changePass),
    };
  } else if (isOfType(action, PassphraseChangeActions.Success)) {
    return {
      ...state,
      changePass: asyncInitState,
      passphraseEnabled: action.data
    };
  } else if (isOfType(action, SyncEnableActions.Error)) {
    return {
      ...state,
      enableSync: asyncError(action.data)
    };
  } else if (isOfType(action, AccountImportActions.Start)) {
    return {
      ...state,
      enableSync: asyncInProgress(state.enableSync),
      initialised: false,
      initialising: true,
    };
  } else if (isOfType(action, AccountImportActions.Success)) {
    return {
      ...state,
      enableSync: asyncInitState,
      account: action.data.account,
      initialising: false,
      initialised: true,
      passphraseEnabled: action.data.passphraseEnabled
    };
  } else if (isOfType(action, AccountImportActions.Error)) {
    return {
      ...state,
      initialising: false,
      enableSync: asyncError(action.data),
    };
  } else if (isOfType(action, AccountImportDataActions.Start)) {
    return {
      ...state,
      dataImport: asyncInProgress(state.dataImport),
    };
  } else if (isOfType(action, AccountImportDataActions.Success)) {
    return {
      ...state,
      dataImport: asyncInitState
    };
  } else if (isOfType(action, AccountImportDataActions.Error)) {
    return {
      ...state,
      dataImport: asyncError(action.data)
    };
  } else if (isOfType(action, AccountDeleteActions.Start)) {
    return {
      ...state,
      delete: asyncInProgress(state.delete)
    };
  } else if (isOfType(action, AccountDeleteActions.Success)) {
    return initialState;
  } else if (isOfType(action, AccountDeleteActions.Error)) {
    return {
      ...state,
      delete: asyncError(action.data),
    };
  } else {
    return state;
  }
};
