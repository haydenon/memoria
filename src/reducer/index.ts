import { routerReducer, RouterState } from 'react-router-redux';
import { combineReducers } from 'redux';

import { IScreenState, screen } from '../screen';
import { account, IAccountState } from './account.reducer';
import { breadcrumbs, IBreadcrumbState } from './breadcrumbs.reducer';
import { entry, IEntryState } from './entry.reducer';
import { ISectionState, section } from './section.reducer';
import { ISettingsState, settings } from './settings.reducer';

export interface IState {
  routing: RouterState;
  entry: IEntryState;
  section: ISectionState;
  breadcrumbs: IBreadcrumbState;
  settings: ISettingsState;
  screen: IScreenState;
  account: IAccountState;
}

export const reducer = combineReducers({
  routing: routerReducer,
  entry,
  section,
  breadcrumbs,
  settings,
  screen,
  account,
});
