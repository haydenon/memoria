import { Action } from 'redux';

import { isOfType } from '../actions/actions';
import { BreadcrumbsSet } from '../actions/breadcrumbs.actions';
import { IBreadcrumb } from '../LayoutRoute';

export interface IBreadcrumbState {
  breadcrumbs: IBreadcrumb[];
}

const originalState: IBreadcrumbState = {
  breadcrumbs: []
};

export const breadcrumbs = (state = originalState, action: Action): IBreadcrumbState => {
  if (isOfType(action, BreadcrumbsSet)) {
    return {
      ...state,
      breadcrumbs: action.data
    };
  } else {
    return state;
  }
};
