import { Action } from 'redux';

import { sort } from '../prelude';

import { AccountDeleteActions } from '../actions/account.actions';
import { isOfDatalessType, isOfType } from '../actions/actions';
import {
  DateLoad,
  DateUpdateActions,
  EvaluationLoad,
  EvaluationUpdateActions,
} from '../actions/settings.actions';
import { DATE_DEFAULT, EVALUATION_DEFAULT, IEvaluationSettings, ISettings } from '../settings/settings';

interface ISubSetting {
  update: { inProgress: boolean; error: string | null; };
  loading: boolean;
}

type SubSettingState = {
  [K in keyof ISettings]: ISubSetting;
};

export interface ISettingsState {
  settings?: ISettings;
  state: SubSettingState;
}

const origState = {
  loading: true,
  update: {
    inProgress: false,
    error: null
  }
};

const originalState: ISettingsState = {
  state: {
    evaluation: origState,
    date: origState,
  }
};

const currentSettings = (current: ISettings | undefined) =>
  current === undefined ? {} as any as ISettings : current;

const handleLoad = <K extends keyof ISettings>(type: K, state: ISettingsState, data: ISettings[K] | undefined) => {
  const currentState: ISubSetting = state.state[type];
  return {
    ...state,
    state: {
      ...state.state,
      [type]: {
        ...currentState,
        loading: false
      }
    },
    settings: {
      ...currentSettings(state.settings),
      [type]: data
    }
  };
};

const handleUpdate = <K extends keyof ISettings>(type: K, state: ISettingsState) => {
  const currentState: ISubSetting = state.state[type];
  return {
    ...state,
    state: {
      ...state.state,
      [type]: {
        ...currentState,
        update: {
          inProgress: true,
          error: null
        }
      }
    }
  };
};

const handleUpdated = <K extends keyof ISettings>(type: K, state: ISettingsState) => {
  const currentState: ISubSetting = state.state[type];
  return {
    ...state,
    state: {
      ...state.state,
      [type]: {
        ...currentState,
        update: {
          ...currentState.update,
          inProgress: false,
        }
      }
    }
  };
};

const handleUpdateFailed = <K extends keyof ISettings>(type: K, state: ISettingsState, error: string | undefined) => {
  const currentState: ISubSetting = state.state[type];
  return {
    ...state,
    state: {
      ...state.state,
      [type]: {
        ...currentState,
        update: {
          ...currentState.update,
          inProgress: false,
          error,
        }
      }
    }
  };
};

const sortAttributes = (evalSettings: IEvaluationSettings | undefined) => {
  if (evalSettings === undefined)
    return undefined;

  return {
    ...evalSettings,
    attributes: sort(evalSettings.attributes, (a1, a2) => a2.value - a1.value)
  };
};

export const settings = (state = originalState, action: Action): ISettingsState => {
  // EVALUATION
  if (isOfType(action, EvaluationLoad)) {
    return handleLoad('evaluation', state, sortAttributes(action.data));
  } else if (isOfType(action, EvaluationUpdateActions.Start)) {
    return handleUpdate('evaluation', state);
  } else if (isOfType(action, EvaluationUpdateActions.Success)) {
    return handleUpdated('evaluation', state);
  } else if (isOfType(action, EvaluationUpdateActions.Error)) {
    return handleUpdateFailed('evaluation', state, action.data);
    // DATE
  } else if (isOfType(action, DateLoad)) {
    return handleLoad('date', state, action.data);
  } else if (isOfType(action, DateUpdateActions.Start)) {
    return handleUpdate('date', state);
  } else if (isOfType(action, DateUpdateActions.Success)) {
    return handleUpdated('date', state);
  } else if (isOfType(action, DateUpdateActions.Error)) {
    return handleUpdateFailed('date', state, action.data);
  } else if (isOfType(action, AccountDeleteActions.Success)) {
    return originalState;
  } else {
    return state;
  }
};
