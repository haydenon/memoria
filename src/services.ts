import { AccountService } from './data/account-service';
import { KeyStorageManager } from './encryption/key-storage';
import PassphraseManager from './encryption/passphrase-manager';

export interface IServices {
  passphraseManager: PassphraseManager;
  keyStorageManager: KeyStorageManager;
  accountService: AccountService;
}
