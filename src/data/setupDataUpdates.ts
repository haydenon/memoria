import { Action, Dispatch } from 'redux';

import { identity, Option } from '../prelude';

import { EntryLoad } from '../actions/entry.actions';
import { SectionLoad } from '../actions/section.actions';

import { DateLoad, EvaluationLoad } from '../actions/settings.actions';
import { IState } from '../reducer';
import { DATE_DEF, DATE_DEFAULT, EVALUATION_DEF, EVALUATION_DEFAULT } from '../settings/settings';
import { DataProvider } from './data-provider';
import { ENTRY_DEF, IEntryModel, mapEntryModel, mapSectionModel, SECTION_DEF } from './journal.model';
import { IModel, IModelDefinition } from './model';

const add = <T, R>(
  dispatch: Dispatch<IState>,
  action: new (data: R[]) => Action,
  map: (t: T) => R) => (items: T[]) => {
    dispatch(new action(items.map(map)));
  };

const addSingleton = <T>(
  dispatch: Dispatch<IState>,
  action: new (data: T) => Action
) => (items: T[]) => {
  if (items.length > 1)
    throw new Error('Should only have one of singleton doc');
  else if (items.length > 0)
    dispatch(new action(items[0]));
};

export default async (dispatch: Dispatch<IState>, dataProvider: DataProvider) => {
  dataProvider.addDataListener(addSingleton(dispatch, EvaluationLoad), EVALUATION_DEF);
  dataProvider.addDataListener(addSingleton(dispatch, DateLoad), DATE_DEF);

  dataProvider.addDataListener(add(dispatch, EntryLoad, mapEntryModel), ENTRY_DEF);
  dataProvider.addDataListener(add(dispatch, SectionLoad, mapSectionModel), SECTION_DEF);
};
