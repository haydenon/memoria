import { eql, flatMap, groupBy, identity, Option, toReadableString, truthy, update } from '../prelude';

import { DataProvider, IUpdate } from './data-provider';
import { IModel, IModelDefinition } from './model';

import { IAccount, ISync } from '../account';
import { IEntry, Journal } from '../journal';
import {
  DATE_DEF,
  DATE_DEFAULT,
  EVALUATION_DEF,
  EVALUATION_DEFAULT,
  IEvaluationSettings,
  ISettings
} from '../settings/settings';
import { ACCOUNT_DEF, IAccountModel, mapAccountModel, mapAccountView } from './account.model';
import { ENTRY_DEF, IEntryModel, ISectionModel, mapEntryView, mapSectionViews, SECTION_DEF } from './journal.model';

const eqlModel = <T extends IModel>(item1: T, item2: T) =>
  eql(update(item1 as IModel, { _rev: undefined }), update(item2 as IModel, { _rev: undefined }));

export class DataService {
  private initialised = false;
  constructor(private readonly provider: DataProvider) { }

  public initialise = () => {
    if (!this.initialised)
      this.provider.init();

    this.initialised = false;
  }

  public async createJournal(journal: Journal) {
    const [entry, sections] = journal;
    const sectionIds = (await this.provider.createRange(mapSectionViews(sections), SECTION_DEF))
      .map((s) => s._id);

    await this.provider.create(mapEntryView(entry), ENTRY_DEF);
  }

  public async importJournals(
    journals: Journal[],
    progressListener?: (item: number, total: number) => void,
    hasCancelled?: () => boolean,
  ) {
    const CHUNK_SIZE = 25;
    const chunked = groupBy(journals, (_, ind) => Math.floor(ind / CHUNK_SIZE));

    if (progressListener)
      progressListener(0, chunked.length);

    await chunked.reduce((prom, chunk, index) => {
      return prom.then(async () => {
        if (hasCancelled && hasCancelled()) {
          return prom.then<void>(() => (undefined));
        }

        const sections = flatMap((j) => j[1], chunk.items);
        const entries = chunk.items.map((j) => j[0]);
        await this.provider.runImport((createRange) => createRange(mapSectionViews(sections), SECTION_DEF));
        await this.provider.runImport((createRange) => createRange(entries.map(mapEntryView), ENTRY_DEF));
        if (progressListener)
          progressListener(index + 1, chunked.length);
      });
    }, Promise.resolve<void>(undefined));
  }

  public importFromRemote = (progressListener?: (item: number, total: number) => void) =>
    this.provider.importFromRemote(progressListener)

  public async updateJournal(journal: Journal) {
    const [ent, sect] = journal;
    const entry = mapEntryView(ent);
    const sections = mapSectionViews(sect);
    const current = Option.except(
      new Error('Error occurred while updating journal entry'),
      await this.provider.getById<IEntryModel>(ent.id)
    );

    // TODO: Change dates
    if (current.date !== entry.date) {
      throw new Error('You cannot currently change journal entry dates');
    }

    const oldSections = await this.provider.list(SECTION_DEF, current.date);
    const changedSections = sections.filter((s) => {
      const old = oldSections.find((os) => os._id === s._id);
      return old !== undefined && !eqlModel(old, s);
    });
    const addedSections = sections.filter((s) => !s._id);
    const removedSections = oldSections.filter((os) => sections.every((s) => os._id !== s._id));

    const added = !addedSections.length || await this.provider.createRange(addedSections, SECTION_DEF);
    const updated = !changedSections.length || await this.provider.updateRange(changedSections);
    const removed = !removedSections.length || await this.provider.removeRange(removedSections);

    const updateEntry = await this.provider.update(entry);

    if (!(updated && removed && updateEntry)) {
      throw new Error('Error occurred while updating journal entry');
    }
  }

  public async deleteJournal(entry: IEntry) {
    const entryModel = mapEntryView(entry);
    const sections = await this.provider.list(SECTION_DEF, entryModel.date);
    const sectionsRemoved = await this.provider.removeRange(sections);
    const entryRemoved = await this.provider.remove(entryModel);

    if (!(sectionsRemoved && entryRemoved)) {
      throw new Error('Error occurred while removing journal entry');
    }
  }

  public async updateSettings<T extends keyof ISettings>(name: T, propUpdate: IUpdate<ISettings[T]>) {
    try {
      await this.provider.deltaUpdate(propUpdate);
    } catch {
      throw new Error(`Failed to update ${toReadableString(name)} settings`);
    }
  }

  public async createSettings(): Promise<void> {
    await this.provider.create(DATE_DEFAULT, DATE_DEF);
    await this.provider.create(EVALUATION_DEFAULT, EVALUATION_DEF);
  }

  public deleteDatabases = (): Promise<boolean> =>
    this.provider.deleteDatabases()
      .then(() => true)
      .catch(() => false)
}
