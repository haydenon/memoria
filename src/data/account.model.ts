import { IAccount } from '../account';
import { createSingletonModelDefinition, IModel } from './model';

export const mapAccountView = (account: IAccount, user: string, password: string): IAccountModel => ({
  _id: '',
  sync: {
    enabled: account.sync.enabled,
    user,
    password,
  }
});

export const mapAccountModel = (account: IAccountModel): IAccount => ({
  sync: {
    enabled: account.sync.enabled,
  }
});

export interface ISyncModel {
  enabled: boolean;
  user?: string;
  password?: string;
}

export interface IAccountModel extends IModel {
  sync: ISyncModel;
}

const AccountDefinition = createSingletonModelDefinition('account');

export const ACCOUNT_DEF = new AccountDefinition();

export interface IPassphraseInformation extends IModel {
  passphraseEnabled: boolean;
}

const PassphraseInformationDefinition = createSingletonModelDefinition('passphraseInformation');

export const PASSPHRASE_INFORMATION_DEF = new PassphraseInformationDefinition();
