import PouchDB from 'pouchdb';
import PouchDBUpsert from 'pouchdb-upsert';

import { IModel, IModelDefinition } from './model';

PouchDB.plugin(PouchDBUpsert);

import { error, filterType, identity, noop, Option, throwIf, truthy, update } from '../prelude';

import { IEncryptedData, isEncryptedData } from '../encryption/common';
import MemoriaCrypto from '../encryption/crypto';
import { AccountService } from './account-service';

type ImportAction<T extends IModel, TData> =
  (createRange: (range: T[], def: IModelDefinition<T, TData>) => Promise<void>) => Promise<void>;

export interface IUpdate<T> {
  id: string;
  update: (t: T) => T;
}

export const delta = <T extends IModel>(item: T, itemUpdate: (t: T) => T): IUpdate<T> => ({
  id: item._id,
  update: itemUpdate,
});

const recreateArray = (arr: any) => new Uint8Array(Object.keys(arr).map((k) => arr[k]));

/*
Changes synced from a remote db turn Uint8Array into a map, i.e. [10, 11] -> { '0': 10, '1': 11 }
This transforms them back into a Uint8Array
*/
const transformEncryptedData = <T extends IEncryptedData>(data: T): T => {
  data.data = recreateArray(data.data);
  data.iv = recreateArray(data.iv);

  return data;
};

interface IBulkDoc {
  doc?: any;
  id: string;
  key: string;
  value: {
    rev: string;
    deleted?: boolean;
  };
}

const withNotFound = <T>(db: PouchDB.Database<{}>, docId: string, ifNotFound: () => T | Promise<T>): Promise<T> =>
  db.get<T>(docId).catch(async (err) => {
    if (err.name === 'not_found') {
      return ifNotFound();
    } else {
      throw err;
    }
  });

const withDefault = <T extends IModel>(db: PouchDB.Database<{}>, docId: string, defaultValue: T, putDefault = false) =>
  withNotFound(db, docId, async () => {
    if (putDefault) {
      await db.put(defaultValue as any);
    }
    return defaultValue;
  });

const asyncMap = <T, R>(array: T[], mapper: (t: T) => Promise<R | undefined>) => {
  return Promise.all(array.map(mapper))
    .then((arr) => arr.filter(truthy)) as Promise<R[]>;
};

const id = <T extends IModel, TData>(def: IModelDefinition<T, TData>, item: T) =>
  def.generateId(item);

interface ISyncSettings extends IModel {
  lastSeq: string | number;
}

const SYNC_ID = 'sync_settings';

const isOfType = <T extends IModel, TData>(type: IModelDefinition<T, TData>) => (item: IModel): item is T =>
  !!(item && item._id && item._id.startsWith(type.name()));

interface IListener {
  id: number;
  callback: (models: IModel[]) => void;
  type: string;
}

let listenerId = 1;

export const deltaUpdate = (database: PouchDB.Database<{}>, crypto: MemoriaCrypto) =>
  async<T extends IModel>(propUpdate: IUpdate<T>): Promise<void> => {
    let succeeded = false;
    while (!succeeded) {
      const doc = transformEncryptedData(await database.get<IEncryptedData>(propUpdate.id));
      const decrypted = await crypto.decryptData<T>(doc);
      const updated = propUpdate.update(decrypted);
      const encrypted = await crypto.encryptData(updated) as IModel;
      encrypted._rev = doc._rev;
      encrypted._id = propUpdate.id;

      succeeded = true;
      try {
        await database.put(encrypted);
      } catch {
        succeeded = false;
      }
    }
  };

export class DataProvider {
  public deltaUpdate = deltaUpdate(this.remoteDb, this.crypto);

  private remoteDetails?: string;
  private sync?: PouchDB.Replication.Sync<{}>;
  private changes?: PouchDB.Core.Changes<{}>;

  private changeListeners: IListener[] = [];

  private seqUpdate?: number;

  private localUpdates: { [id: string]: string[] } = {};

  constructor(
    private readonly crypto: MemoriaCrypto,
    private readonly accountService: AccountService,
    private readonly localDb: PouchDB.Database<{}>,
    private readonly remoteDb: PouchDB.Database<{}>,
  ) { }

  public create = async <T extends IModel, TData>(
    item: T,
    type: IModelDefinition<T, TData>,
  ): Promise<T> => {
    if (item._id) {
      throw Error(`Item with id '${item._id}' already exists`);
    }

    item._id = id(type, item);

    const encrypted = await this.crypto.encryptData(item);
    encrypted._id = item._id;

    this.addLocalUpdate(await this.remoteDb.put(encrypted));
    await this.localDb.put(item);
    this.fireDataChange(type.name());

    return item;
  }

  public createRange = async <T extends IModel, TData>(
    items: T[],
    type: IModelDefinition<T, TData>
  ): Promise<T[]> => {
    const exist = items.filter((item) => item._id);
    if (exist.length) {
      throw Error(`Items with ids [${exist.map((i) => i._id)}] already exists`);
    }

    const encrypted = await asyncMap(items, async (item) => {
      item._id = id(type, item);
      const encrypt = await this.crypto.encryptData(item);
      encrypt._id = item._id;

      return encrypt;
    });

    (await this.remoteDb.bulkDocs(encrypted)).forEach(this.addLocalUpdate);
    await this.localDb.bulkDocs(items);
    this.fireDataChange(type.name());
    return items;
  }

  public addDataListener = async <T extends IModel, TData>(
    onChange: (items: T[]) => void,
    type: IModelDefinition<T, TData>
  ) => {
    const isType = isOfType<T, TData>(type);
    const docs = await this.localDb.allDocs(
      {
        include_docs: true,
        startkey: `${type.name()}`,
        endkey: `${type.name()}\uffff`
      });
    onChange(filterType(docs.rows.map((r) => r.doc), isType));

    const lId = listenerId++;

    this.changeListeners.push({
      id: lId,
      callback: (models) => onChange(filterType(models, isType)),
      type: type.name()
    });

    return lId;
  }

  public removeDataListener = (lId: number): boolean => {
    const index = this.changeListeners.findIndex((cl) => cl.id === lId);
    if (index >= 0) {
      this.changeListeners.splice(index, 0);
    }

    return index >= 0;
  }

  public getById = <T extends IModel>(modelId: string): Promise<Option<T>> =>
    this.localDb.get<T>(modelId)
      .then(Option.some)
      .catch(() => Option.none())

  public list = async <T extends IModel, TData>(def: IModelDefinition<T, TData>, data?: TData): Promise<T[]> => {
    const prefix = data !== undefined ? def.getIdPrefix(data) : def.name();
    return (await this.localDb.allDocs({
      include_docs: true,
      startkey: `${prefix}_`,
      endkey: `${prefix}_\uffff`
    })).rows
      .map((d) => d.doc)
      .filter((doc) => doc && !(doc as IModel)._deleted) as T[];
  }

  public update = async<T extends IModel>(item: T): Promise<boolean> => {
    try {
      const updateItem = update(item as IModel, { _rev: undefined });
      const remoteProm = this.remoteDb.get(item._id);
      const localProm = this.localDb.get(item._id);
      const encrypted = await this.crypto.encryptData(updateItem) as IModel;
      encrypted._id = item._id;

      const withRev = (i: any, retrieved: PouchDB.Core.GetMeta) => {
        i._rev = retrieved._rev;
        return i;
      };

      this.addLocalUpdate(await this.remoteDb.put(withRev(encrypted, await remoteProm)));
      await this.localDb.put(withRev(item, await localProm));
      this.fireDataChange(this.getTypeFromId(item._id));

      return true;
    } catch {
      return false;
    }
  }

  public updateRange = async<T extends IModel>(items: T[]) => {
    try {
      const keys = items.map((i) => i._id);
      const remoteItems = items.map((item) => update(item as IModel, { _rev: undefined }));
      const encryptedProm = asyncMap(remoteItems, this.crypto.encryptData);
      const remoteProm = this.remoteDb.allDocs({ keys });
      const localProm = this.localDb.allDocs({ keys });

      const toUpdate = (bulkItems: IBulkDoc[]) => (item: any) => {
        const bulk = bulkItems.find((b) => b.id === item._id);
        if (bulk) {
          item._rev = bulk.value.rev;
        }

        return item;
      };

      (await this.remoteDb.bulkDocs((await encryptedProm).map(toUpdate((await remoteProm).rows))))
        .forEach(this.addLocalUpdate);
      this.localDb.bulkDocs(items.map(toUpdate((await localProm).rows)));
      this.fireDataChange(this.getTypeFromId(items[0]._id));

      return true;
    } catch {
      return false;
    }
  }

  public remove = async <T extends IModel>(item: T): Promise<boolean> => {
    try {
      const remoteProm = this.remoteDb.get(item._id);
      const localProm = this.localDb.get(item._id);
      this.addLocalUpdate(await this.remoteDb.remove(await remoteProm));
      await this.localDb.remove(await localProm);
      this.fireDataChange(this.getTypeFromId(item._id));

      return true;
    } catch {
      return false;
    }
  }

  public removeRange = async <T extends IModel>(items: T[]): Promise<boolean> => {
    try {
      const keys = items.map((i) => i._id);
      const remoteProm = this.remoteDb.allDocs({ keys });
      const localProm = this.localDb.allDocs({ keys });

      const toDelete = (row: IBulkDoc) => ({
        _id: row.id,
        _rev: row.value.rev,
        _deleted: true,
      });

      (await this.remoteDb.bulkDocs((await remoteProm).rows.map(toDelete)))
        .forEach(this.addLocalUpdate);
      await this.localDb.bulkDocs((await localProm).rows.map(toDelete));
      this.fireDataChange(this.getTypeFromId(items[0]._id));

      return true;
    } catch {
      return false;
    }
  }

  public deleteDatabases = async () => {
    if (this.sync) {
      this.sync.cancel();
    }

    await this.remoteDb.destroy();
    await this.localDb.destroy();
  }

  public importFromRemote = async (progressListener?: (item: number, total: number) => void) => {
    const progress = (item: number, total: number) => {
      if (progressListener)
        progressListener(item, total);
    };

    progress(0, 100);
    const remote = Option.except('Must have remote details to import data', this.accountService.getRemoteDatabase());

    const sync = this.remoteDb.sync(remote);

    return new Promise<void>((resolve, reject) => {
      sync.on('complete', async () => {
        const lastSeq = (await this.remoteDb.info()).update_seq;
        const data = (await this.remoteDb.allDocs({
          include_docs: true,
        }))
          .rows
          .map((d) => d.doc)
          .filter((doc) => doc && !(doc as IEncryptedData)._deleted) as IEncryptedData[];

        const created: IModel[] = [];

        progress(0, data.length);
        data.reduce((prom, item, index) => {
          return prom.then(() => {
            return this.crypto.decryptData<IModel>(transformEncryptedData(item))
              .then(async (model) => {
                delete model._rev;
                await this.localDb.put(model);
                created.push(model);
                progress(index + 1, data.length);
              });
          });
        }, Promise.resolve<void>(undefined)).then(() =>
          this.localDb.upsert<ISyncSettings>(SYNC_ID, (syncSetting: any) => {
            if (truthy(syncSetting.lastSeq)) {
              syncSetting.lastSeq = lastSeq;
            } else {
              return {
                _id: SYNC_ID,
                lastSeq,
              };
            }
            return syncSetting;
          })
        ).then(() => {
          this.changeListeners.forEach((listener) => {
            const type = listener.type;
            listener.callback(created.filter((c) => this.getTypeFromId(c._id) === type));
          });
          resolve(undefined);
        });
      });
    });
  }

  public init = async () => {
    const sync = await withDefault<ISyncSettings>(this.localDb, SYNC_ID, { _id: SYNC_ID, lastSeq: 0 }, true);

    this.accountService.addRemoteDatabaseSyncListener((remoteDetails) =>
      Option.match(
        (details) => this.setSyncDetails(details),
        () => this.sync ? this.sync.cancel() : noop(),
        remoteDetails
      )
    );

    this.watchChanges(sync.lastSeq);
  }

  public setSyncDetails = (details: string) => {
    this.remoteDetails = details;
    this.startSync();
  }

  public runImport = async <T extends IModel, TData>(action: ImportAction<T, TData>): Promise<void> => {
    this.endSync();
    this.endWatch();

    await action((range, def) => this.createRange(range, def).then(() => undefined));
    const info = await this.remoteDb.info();
    await this.updateLastSeq(info.update_seq);

    this.watchChanges(info.update_seq);
    await this.startSync();
  }

  private watchChanges = (since: string | number) => {
    this.changes = this.remoteDb.changes({ live: true, since, include_docs: true });
    this.changes.on('change', this.handleRemoteChange);
  }

  private endWatch = () => {
    if (this.changes) {
      this.changes.cancel();
      this.changes = undefined;
    }
  }

  private endSync = () => {
    if (this.sync) {
      this.sync.cancel();
      this.sync = undefined;
    }
  }

  private startSync = async () => {
    const remoteDetails = this.remoteDetails;
    if (remoteDetails === undefined) {
      return;
    }

    this.endSync();

    await new Promise((resolve) => {
      this.remoteDb.sync(remoteDetails, { live: false }).on('complete', () => {
        resolve();
      });
    });

    const syncOptions = {
      live: true,
      retry: true,
      continuous: true
    };

    this.sync = this.remoteDb.sync(remoteDetails, syncOptions);
  }

  private handleRemoteChange = async (change: PouchDB.Core.ChangesResponseChange<{}>) => {
    if (!change.doc) return;
    const seq = change.seq;

    const type = this.getTypeFromId(change.doc._id);

    const localDoc = await withNotFound<IModel | null>(this.localDb, change.id, () => null);
    let modelId: string | undefined;

    if (change.changes.length) {
      const rev = change.changes[0].rev;
      if (this.popLocalUpdate(change.id, rev)) {
        if (this.seqUpdate !== undefined) {
          window.clearTimeout(this.seqUpdate);
        }
        this.seqUpdate = window.setTimeout(() => {
          this.updateLastSeq(seq);
        }, 200);

        return;
      }
    }

    let changed = true;
    if (change.deleted && localDoc) {
      await this.localDb.remove(localDoc as any);
    } else if (!change.deleted) {
      const changeDoc = change.doc;
      if (!isEncryptedData(changeDoc))
        return;

      const model = await this.crypto.decryptData<IModel>(
        transformEncryptedData(changeDoc)
      );

      modelId = model._id;

      if (localDoc) {
        model._rev = localDoc._rev;
      } else {
        delete model._rev;
      }

      await this.localDb.put(model);
    } else {
      // Nothing changed
      changed = false;
    }

    await this.updateLastSeq(seq);

    if (changed)
      this.fireDataChange(type);
  }

  private fireDataChange = (type: string) => {
    this.changeListeners.forEach(async (listener) => {
      if (listener.type === type)
        listener.callback(await this.getForType(listener.type));
    });
  }

  private updateLastSeq = async (seq: number | string) => {
    await this.localDb.upsert<ISyncSettings>(SYNC_ID, (sync: any) => {
      const intSeq = typeof (seq) === 'string' ? parseInt(seq, 10) : seq;
      if (truthy(sync.lastSeq)) {
        sync.lastSeq = Math.max(intSeq, sync.lastSeq);
      }

      return { ...sync, lastSeq: intSeq };
    });
  }

  private getTypeFromId = (modelId: string): string => {
    throwIf('Must provide a valid id', () => !modelId);

    const splitId = modelId.split('_');
    return splitId.length === 1
      ? modelId
      : splitId[0];
  }

  private popLocalUpdate = (modelId: string, rev: string): boolean => {
    if (!this.localUpdates[modelId]) {
      return false;
    }

    const index = this.localUpdates[modelId].indexOf(rev);
    if (index >= 0) {
      this.localUpdates[modelId].splice(index, 1);
    }

    return index >= 0;
  }

  private addLocalUpdate = (response: PouchDB.Core.Response | PouchDB.Core.Error) => {
    const isError = (resp: any): resp is PouchDB.Core.Error => {
      return resp && resp.error || resp.status;
    };

    if (isError(response)) {
      return;
    }

    if (!this.localUpdates[response.id]) {
      this.localUpdates[response.id] = [];
    }

    this.localUpdates[response.id].push(response.rev);
  }

  private getForType = async (type: string): Promise<IModel[]> => {
    return (await this.localDb.allDocs({
      include_docs: true,
      startkey: `${type}`,
      endkey: `${type}\uffff`
    })).rows
      .map((d) => d.doc)
      .filter((doc) => doc && !(doc as IModel)._deleted) as IModel[];
  }
}
