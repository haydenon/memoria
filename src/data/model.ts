import uuid from 'uuid/v4';

export interface IModel {
  _id: string;
  _rev?: string;
  _deleted?: boolean;
}

// tslint:disable:interface-name

interface ModelDefinitionConstructor<T extends IModel, TData> {
  new(): IModelDefinition<T, TData>;
}

export interface IModelDefinition<T extends IModel, TData> {
  name(): string;
  generateId(item: T): string;
  getPrefixData(item: T): TData;
  getIdPrefix(prefixData: TData): string;
}

export const createMultiModelDefinition = <T extends IModel, TData>(
  model: string,
  getPrefixData: (item: T) => TData,
  dataToString: (data: TData) => string,
): ModelDefinitionConstructor<T, TData> => {
  return class MultiModelDefinition implements IModelDefinition<T, TData> {
    public getPrefixData = getPrefixData;

    public getIdPrefix = (data: TData) => `${model}_${dataToString(data)}`;

    public name = () => model;

    public generateId = (item: T): string =>
      `${this.getIdPrefix(this.getPrefixData(item))}_${uuid()}`
  };
};

export const createSingletonModelDefinition = <T extends IModel>(
  documentName: string
): ModelDefinitionConstructor<T, undefined> => {
  return class SingletonModelDefinition implements IModelDefinition<T, undefined> {
    public name = () => documentName;

    public generateId = (_item: T) => this.getIdPrefix(undefined);
    public getPrefixData = (_item: T) => undefined;
    public getIdPrefix = (_data: undefined) => this.name();
  };
};
