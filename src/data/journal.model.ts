import moment from 'moment';
import { Delta } from 'quill';

import { identity, truthy } from '../prelude';

import { Entry, IEntry, ISection, ISectionQuality, Section } from '../journal';
import { ShortDateFormatId } from '../settings/date-formats';
import { createMultiModelDefinition, IModel } from './model';

type ModelId = string;

const DATE_FORMAT = 'YYYY-MM-DD';

export const mapEntryView = (entry: IEntry): IEntryModel => ({
  _id: entry.id,
  date: entry.date,
  dateFormat: entry.dateFormat,
  journal: entry.journal,
});

export const mapSectionViews = (sections: ISection[]): ISectionModel[] =>
  sections.map((section) => ({
    _id: section.id,
    date: section.date,
    name: section.name,
    value: section.value,
    quality: section.quality,
  }));

export const mapEntryModel = (entry: IEntryModel) => Entry(
  entry._id,
  entry.date,
  entry.dateFormat,
  entry.journal,
);

export const mapSectionModel = (section: ISectionModel) => Section(
  section._id,
  section.date,
  section.name,
  section.value,
  section.quality
);

export interface IEntryModel extends IModel {
  readonly date: string;
  readonly dateFormat: ShortDateFormatId;
  readonly journal: Delta;
}

export interface ISectionModel extends IModel {
  readonly date: string;
  readonly name: string;
  readonly value: string;
  readonly quality?: ISectionQuality;
}

const EntryDefinition = createMultiModelDefinition<IEntryModel, string>('entry', (e) => e.date, identity);
const SectionDefinition = createMultiModelDefinition<ISectionModel, string>('section', (s) => s.date, identity);

export const ENTRY_DEF = new EntryDefinition();
export const SECTION_DEF = new SectionDefinition();
