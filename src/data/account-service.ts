import uuid from 'uuid/v4';

import config from '../config';
import { error, Option, present, update } from '../prelude';
import { post } from '../util';

import { DEFAULT_PASSPHRASE, getPassphrase, IAccount, IAccountCode, ISync } from '../account';
import { encrypt, generateRandomPassword, IEncryptedData, isEncryptedData } from '../encryption/common';
import MemoriaCrypto from '../encryption/crypto';
import {
  ACCOUNT_DEF,
  IAccountModel,
  IPassphraseInformation,
  ISyncModel,
  mapAccountModel,
  mapAccountView,
  PASSPHRASE_INFORMATION_DEF
} from './account.model';
import { DataProvider, deltaUpdate } from './data-provider';

interface IRegisterBody {
  email: string;
  username: string;
  password: string;
  confirmPassword: string;
}

interface ILoginBody {
  username: string;
  password: string;
}

interface ILoginResponse {
  issued: number;
  expires: number;
  provider: string;
  token: string;
  password: string;
  user_id: string;
  roles: string[];
  userDBs: {
    memoria: string;
  };
}

const ACCOUNT_ID = ACCOUNT_DEF.getIdPrefix(undefined);

type RemoteDetailCallback = (details: Option<string>) => void;

export class AccountService {
  private syncDetails?: ISyncModel;
  private remoteDbDetails?: ILoginResponse;
  private remoteListeners: RemoteDetailCallback[] = [];

  constructor(
    private crypto: MemoriaCrypto,
    private localDb: PouchDB.Database<{}>,
    private importKey: (key: JsonWebKey, passphrase: string) => Promise<void>,
  ) { }

  public getAccount = async (): Promise<Option<IAccount>> =>
    Option.map(mapAccountModel, await this.getAccountModel())

  public async createAccount(): Promise<IAccount> {
    const id = ACCOUNT_DEF.getIdPrefix(undefined);
    const newAcc: IAccountModel = {
      _id: id,
      sync: {
        enabled: false,
        user: undefined,
        password: undefined
      }
    };

    const encrypted = await this.crypto.encryptData(newAcc);
    encrypted._id = id;

    await this.localDb.put(encrypted).catch(error('Failed to create new account'));
    return mapAccountModel(newAcc);
  }

  public async enableSync(): Promise<ISync> {
    const getSyncData = async () => {
      const currentAccount = Option.except(new Error('Account doesn\'t exist'), await this.getAccountModel());
      if (!currentAccount.sync.enabled
        || !currentAccount.sync.user
        || !currentAccount.sync.password) {
        // 16 characters is the limit for superlogin usernames
        const newSync = {
          enabled: true,
          user: uuid().replace(/-/g, '').substring(0, 16),
          password: generateRandomPassword(32),
        };

        await deltaUpdate(this.localDb, this.crypto)<IAccountModel>({
          id: ACCOUNT_ID,
          update: (account) => update(account, { sync: newSync })
        });

        return newSync;
      }

      return currentAccount.sync;
    };

    const sync = await getSyncData();
    this.syncDetails = sync;

    const user = present(sync.user);
    const pass = present(sync.password);

    const body: IRegisterBody = {
      email: `${user}@${config.domain}`,
      username: user,
      password: pass,
      confirmPassword: pass,
    };

    const resp = await post<ILoginResponse>('/api/auth/register', body);
    return Option.match(
      (r) => {
        this.remoteDbDetails = r;
        this.remoteListeners.forEach((callback) => callback(Option.some(r.userDBs.memoria)));
        return { enabled: sync.enabled };
      },
      error('Failed to setup sync'),
      resp
    );
  }

  public importAccount = async (code: IAccountCode): Promise<IAccount> => {
    const phrase = getPassphrase(code.passphrase);
    await this.importKey(code.key, phrase);
    const acc = await this.createAccount();
    const sync: ISyncModel = {
      enabled: true,
      user: code.user,
      password: code.password,
    };
    await deltaUpdate(this.localDb, this.crypto)<IAccountModel>({
      id: ACCOUNT_ID,
      update: (account) => update(account, { sync })
    });

    Option.execute(
      (details) => {
        this.remoteListeners.forEach((callback) => callback(Option.some(details.userDBs.memoria)));
      },
      await this.getRemoteDbDetails()
    );

    return { ...acc, sync: { enabled: true } };
  }

  public getSyncDetails = async (): Promise<Option<ISyncModel>> => {
    if (this.syncDetails)
      return Option.some(this.syncDetails);

    return Option.map((acc) => acc.sync, await this.getAccountModel());
  }

  public getRemoteDatabase = () =>
    Option.map(
      (details) => details.userDBs.memoria,
      Option.fromUndefined(this.remoteDbDetails)
    )

  public addRemoteDatabaseSyncListener = (callback: RemoteDetailCallback): void => {
    this.remoteListeners.push(callback);

    if (this.remoteDbDetails) {
      callback(Option.some(this.remoteDbDetails.userDBs.memoria));
      return;
    }

    this.getRemoteDbDetails().then((details) => {
      callback(Option.map(
        (resp) => resp.userDBs.memoria,
        details
      ));
    });
  }

  private getRemoteDbDetails = async (): Promise<Option<ILoginResponse>> => {
    const syncDetails = Option.bind<IAccountModel, { user: string, password: string }>(
      (acc) => acc.sync && acc.sync.enabled
        ? Option.some({
          user: present(acc.sync.user),
          password: present(acc.sync.password)
        })
        : Option.none(),
      await this.getAccountModel()
    );
    const details: Option<ILoginBody> = Option.map((sync) => ({
      username: sync.user,
      password: sync.password,
    }), syncDetails);
    if (!details.hasValue)
      return Option.none();

    const resp = await post<ILoginResponse>('/api/auth/login', details.value);
    return Option.match(
      (r) => {
        this.remoteDbDetails = r;
        return Option.some(r);
      },
      error('Failed to get login details from server'),
      resp
    );
  }

  private getAccountModel = async (): Promise<Option<IAccountModel>> => {
    const encrypted = await this.localDb.get<IEncryptedData>(ACCOUNT_ID).catch(() => undefined);
    if (encrypted === undefined)
      return Option.none();

    return this.crypto.decryptData<IAccountModel>(encrypted)
      .then((account) => {
        if (this.syncDetails === undefined)
          this.syncDetails = account.sync;
        return Option.some(account);
      })
      .catch((err) => Option.none<IAccountModel>());
  }
}
