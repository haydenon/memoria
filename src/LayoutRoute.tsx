import {
  Avatar,
  Breadcrumb,
  Col,
  Dropdown,
  Icon,
  Layout,
  Menu,
  Row
} from 'antd';
import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect, Dispatch } from 'react-redux';
import { Route, RouterProps } from 'react-router';
import { Link, Redirect } from 'react-router-dom';
import { push } from 'react-router-redux';
import { AutoSizer } from 'react-virtualized';

import LoadingScreen from './components/LoadingScreen';
import { IState } from './reducer';
import { IAccountState } from './reducer/account.reducer';

const { Header, Sider, Content } = Layout;

import './layout.less';

export interface IBreadcrumb {
  link?: string;
  label: string;
}

interface IProps {
  dispatch: Dispatch<IState>;
  account: IAccountState;
  path: string | undefined;
  component: any;
  pathname: string;
  breadcrumbs: IBreadcrumb[];
}

class LayoutRoute extends React.Component<IProps> {
  private settingsMenu = (
    <Menu className="header-settings-menu">
      <Menu.Item>
        <Link to={'/settings/general'}><Icon type="setting" /> Settings</Link>
      </Menu.Item>
      <Menu.Item>
        <Link to={'/settings/sync'}><Icon type="sync" /> Sync</Link>
      </Menu.Item>
    </Menu>
  );

  public componentDidMount() {
    this.checkInitialised();
  }

  public componentDidUpdate() {
    this.checkInitialised();
  }

  public render() {
    if (this.props.account.checking)
      return <LoadingScreen />;

    if (!this.props.account.initialised)
      return null;

    const {
      component,
      breadcrumbs,
      ...rest
    } = this.props;

    const RouteComponent = component;

    const renderRoute = (matchProps: any) => {
      const BREADCRUMBS_HEIGHT = 53;
      const FOOTER_MARGIN = 20;

      return (
        <Layout className="layout">
          <Header>
            <div className="header-content">
              <Row type="flex" justify="space-between" align="middle">
                <Col span={20}>
                  <Link to={'/'}>
                    <div className="header-logo" />
                  </Link>
                  <Menu
                    style={{ lineHeight: '64px' }}
                    theme="dark"
                    mode="horizontal"
                    selectedKeys={this.selectedKeys()}
                  >
                    <Menu.Item key="journal">
                      <Link to={'/journal'}>
                        <Icon type="book" />Journals
                      </Link>
                    </Menu.Item>
                    <Menu.Item key="progress">
                      <Link to={'/progress'}>
                        <Icon type="line-chart" />Progress
                    </Link>
                    </Menu.Item>
                  </Menu>
                </Col>
                <Col>
                  <Dropdown overlay={this.settingsMenu} trigger={['click']} placement="bottomRight">
                    <Avatar className="settings" size="large" icon="user" />
                  </Dropdown>
                </Col>
              </Row>
            </div>
          </Header>
          <Content className="page-content">
            <AutoSizer disableWidth={true}>
              {({ height }) => [
                <Breadcrumb key="breadcrumbs" className="breadcrumbs">
                  {
                    breadcrumbs.map((bc, index) => (
                      <Breadcrumb.Item key={index}>
                        {bc.link !== undefined ? (
                          <Link to={bc.link}>{bc.label}</Link>
                        ) : bc.label}
                      </Breadcrumb.Item>
                    ))
                  }
                </Breadcrumb>,
                <div
                  style={{ height: height - BREADCRUMBS_HEIGHT - FOOTER_MARGIN, overflowY: 'auto' }}
                  key="route-content"
                  className="route-content"
                >
                  <RouteComponent {...matchProps} />
                </div>]}
            </AutoSizer>
          </Content>
        </Layout>
      );
    };

    return <Route {...rest} render={renderRoute} />;
  }

  private checkInitialised() {
    if (!this.props.account.checking && this.props.account.waitingForPassphrase) {
      this.props.dispatch(push('/start/passphrase'));
    } else if (!this.props.account.checking && !this.props.account.initialised) {
      this.props.dispatch(push('/start'));
    }
  }

  private selectedKeys = (): string[] => {
    const path = (this.props.pathname || '/').split('/')[1];
    return [path ? path : 'journal'];
  }
}

const mapStateToProps = (state: IState) => ({
  breadcrumbs: state.breadcrumbs.breadcrumbs,
  pathname: state.routing.location && state.routing.location.pathname,
  account: state.account,
});

export default connect(mapStateToProps)(LayoutRoute);
