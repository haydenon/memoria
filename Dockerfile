FROM amd64/node:carbon
ENTRYPOINT ["node", "./server.js"]
ARG source=./dist
WORKDIR /usr/src/app
COPY $source/package*.json ./
RUN npm install
COPY $source/ .
EXPOSE 8080