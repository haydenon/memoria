export declare interface IChartDataset {
  label?: string;
  values: number[]
}

export declare interface IChartData {
  labels: string[];
  datasets: IChartDataset[];
}

export declare interface IChartOptions {
  parent: string,
  title?: string;
  data: IChartData,
  type: string,
  height: number,
  colors?: string[],
}

declare class Charts {
  constructor(options: IChartOptions)
  unbind_window_events(): void;
}

export default Charts;
