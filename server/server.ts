import bodyParser from 'body-parser';
import cors from 'cors';
import express, { RequestHandler } from 'express';
import http from 'http';
import https from 'https';
import logger from 'morgan';
import path from 'path';
import serveStatic from 'serve-static';
import SuperLogin from 'superlogin';
import routes from '../src/route-list';

const MAX_AGE = 7889400000;

process.on('unhandledRejection', (error) => {
  // tslint:disable-next-line:no-console
  console.error(error);
  process.exit(1);
});

const protocol = process.env.MEMORIA_PROTOCOL;
const host = process.env.MEMORIA_HOST;

const startApp = () => {
  const app = express();
  app.set('port', process.env.PORT || 3000);
  app.use(logger('common') as RequestHandler);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cors());

  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  });

  const config = {
    testMode: {
      noEmail: true,
    },
    local: {
      sendConfirmEmail: false,
      requireEmailConfirm: false,
    },
    dbServer: {
      protocol: process.env.MEMORIA_PROTOCOL,
      host: process.env.MEMORIA_HOST,
      user: process.env.MEMORIA_DB_ADMIN,
      password: process.env.MEMORIA_DB_PASS,
      userDB: 'sl-users',
      couchAuthDB: '_users'
    },
    security: {
      maxFailedLogins: 3,
      lockoutTime: 600,
      tokenLife: 86400,
      loginOnRegistration: true,
    },
    userDBs: {
      defaultDBs: {
        private: ['memoria']
      }
    },
    providers: {
      local: true
    }
  };

  const superlogin = new SuperLogin(config);

  app.use('/api/auth', superlogin.router);

  const staticFiles = express.static(path.join(__dirname, 'public'), { maxAge: MAX_AGE });
  app.use(staticFiles);

  routes.forEach((route) => {
    app.use(route, staticFiles);
  });

  http.createServer(app).listen(app.get('port'));
};

const checkDatabase = () => {
  const get = protocol === 'https://' ? https.get : http.get;
  return new Promise((resolve, reject) => {
    const req = get(`${protocol}${host}`, resolve);
    req.on('error', reject);
  });
};

let timer: NodeJS.Timer | undefined;

timer = setInterval(() => {
  checkDatabase()
    .then(() => {
      if (timer !== undefined) clearInterval(timer);
      startApp();
    })
    .catch(() => {
      // tslint:disable-next-line:no-console
      console.warn('Failed to connect to database, retrying...');
    });
}, 1000);
